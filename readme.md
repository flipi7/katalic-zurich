## Katalic project

Welcome. Please follow below instructions in order to setup your development environment.

## Get Started

1. Install Vagrant and Virtual Box (also make sure to have installed composer)
2. Clone this repo
3. Run **composer install** command in your terminal on root project
4. Run **composer install** command in your terminal on laravel folder
5. Rename .env.example to .env on laravel folder
6. Run **vagrant up** command in your terminal on root project
7. Add in your hosts file this entry: 192.168.11.10 katalic.app
8. Access the project at http://katalic.app

## Setup Database
1. Run in terminal: **php artisan migrate** command
2. Run in terminal: **php artisan db:seed** command
3. Run in terminal: **php artisan db:seed --class="Dev"** command

Also there is a command to auto reset & populate the database: **composer reload-demo**
The above command will auto execute the 3 previous commands.