var opts = {
    "closeButton": true,
    "debug": false,
    "positionClass": "toast-top-right",
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
};

jQuery(document).ready(function ($) {

    if (typeof $app !== 'undefined' && $app.messages) {
        for (messages in $app.messages.errors) {
            toastr.error($app.messages.errors[messages][0], "Error", opts);
        }

        for (messages in $app.messages.success) {
            toastr.success($app.messages.success[0], "OK", opts);
        }
    }

    // Javascript to enable link to tab
    var hash = document.location.hash;
    var prefix = "";
    if (hash) {
        $('.tabs a[href=' + hash.replace(prefix, "") + ']').tab('show');
    }

    $('a[href="#tab-claim"]').tab('show').addClass('btn-zurich');

    $('.btn-tab').click(function (e) {
        var btnList = $(e.currentTarget).parent().children('a');
        var btn = $(e.currentTarget);
        $(btnList).removeClass('btn-zurich');
        $(btnList).addClass('btn-primary');
        btn.removeClass('btn-primary');
        btn.addClass('btn-zurich');
    });

// Change hash for page-reload
    $('.tabs a').on('shown', function (e) {
        window.location.hash = e.target.hash.replace("#", "#" + prefix);
    });

    // Show modal when validation fails
    $('.js-modal-error').modal();
    $('.modal_old').modal();

    $('.remove-attachment').on('click', function(e) {
        var current = $(e.currentTarget);
        current.parent().parent().parent().addClass('hidden');
        current.parent().parent().parent().next().removeClass('hidden');
        current.next().removeAttr('value');
    });

});
