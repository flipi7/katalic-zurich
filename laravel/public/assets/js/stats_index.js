/* Initial data */

var TODAY = moment();
var TEN_DAYS_AGO = moment().subtract(10, 'days');

function initCharts(start, end) {
    newClaimsByDay(start, end);
    claimsByCover(start, end);
    affectedPolicies();
}

function updateCounters(data) {
    var totalClaims = data['totalClaims'];
    var averageClaims = Math.round(data['averageClaims'] * 100) / 100; // Math.floor(data['averageClaims']);
    $('#number-of-claims').attr('data-to', totalClaims);
    $('#average-claims').attr('data-to', averageClaims);
    $('#no-coverage-claims').attr('data-to', Math.round((Math.random() * totalClaims / 2)));
    $('#average-cost').attr('data-to', Math.round((totalClaims * (Math.random() * (100 - 10) + 10) ) * 100 ) / 100 );
}

function runCounters() {
    $("[data-from][data-to]").each(function(i, el)
    {
        var $el = $(el),
            sm = scrollMonitor.create(el);

        sm.fullyEnterViewport(function()
        {
            var opts = {
                    useEasing: 		attrDefault($el, 'easing', true),
                    useGrouping:	attrDefault($el, 'grouping', true),
                    separator: 		attrDefault($el, 'separator', ','),
                    decimal: 		attrDefault($el, 'decimal', '.'),
                    prefix: 		attrDefault($el, 'prefix', ''),
                    suffix:			attrDefault($el, 'suffix', ''),
                },
                $count		= attrDefault($el, 'count', 'this') == 'this' ? $el : $el.find($el.data('count')),
                from        = attrDefault($el, 'from', 0),
                to          = $el.attr('data-to'), //attrDefault($el, 'to', 100),
                duration    = attrDefault($el, 'duration', 2.5),
                delay       = attrDefault($el, 'delay', 0),
                decimals	= new String(to).match(/\.([0-9]+)/) ? new String(to).match(/\.([0-9]+)$/)[1].length : 0,
                counter 	= new countUp($count.get(0), from, to, decimals, duration, opts);

            setTimeout(function(){ counter.start(); }, delay * 1000);

            sm.destroy();
        });
    });
}

function newClaimsByDay(data) {

    var chartOptions = {
        dataSource: data,
        commonSeriesSettings: {
            argumentField: "date",
            type: "line",
            color: '#08407A',
            width: 1,
            point : {
                size: 8,
            }
        },
        argumentAxis: {
            valueMarginsEnabled: false,
            grid: {
                visible: true,
                opacity: 0.25
            },
            visible: false,
            label : { visible: false }
        },
        valueAxis: {
            argumentMarginsEnabled: false,
            grid: {
                visible: true,
                opacity: 0.25
            },
            visible: false,
            label : { visible: false },
            max: Math.max.apply(Math,data.map(function(o){return o.claims;})) + 2,
            min: Math.min.apply(Math,data.map(function(o){return o.claims;})) - 2,
        },
        series: [
            {valueField: "claims", name: "{!! trans('global.Claims') !!}"},
        ],
        legend: {
            visible: true,
            position: 'inside'
        },
        tooltip: {
            enabled: true,
            zIndex: 2000,
            customizeTooltip: function (arg) {
                return {
                    text: arg.argumentText + ' (' + arg.valueText + ')'
                };
            }
        }
    };

    $("#new-claims-day").dxChart(chartOptions).dxChart("instance");
}

function claimsByCover(data) {

    var chartOptions = {
        dataSource: data,
        series: {
            argumentField: "cover",
            valueField: "claims",
            name: "{!! trans('global.Claims by cover') !!}",
            type: "bar",
            label: {
                visible: true,
                connector: {visible: true},
                format: 'Number',
                customizeText: function (e) {
                    return e.valueText;
                }
            },
        },
        customizePoint: function (bar) {
            var colors = ['#08407A','#1D558F','#326AA4','#477FB9','#5C94CE'];
            return {color: colors[bar.index]};
        },
        tooltip: {
            enabled: true,
            zIndex: 2000,
            customizeTooltip: function (arg) {
                return {
                    text: arg.argumentText
                };
            }
        },
        margin: {
            top: 90,
            bottom: 10,
            left: 10,
            right: 30
        },
        argumentAxis: {
            valueMarginsEnabled: false,
            visible: true,
            label: {visible: true}
        },
        valueAxis: {
            argumentMarginsEnabled: false,
            visible: true,
            grid: {
                visible: true,
            },
            label: {visible: true},
            //max: Math.max.apply(Math,data.map(function(o){return o.claims;})) + 2,
            //min: Math.min.apply(Math,data.map(function(o){return o.claims;})) - 2,
        },
        legend: {
            visible: false
        }
    };

    $("#claims-by-cover").dxChart(chartOptions);
}

function affectedPolicies(dataSource) {

    $("#affected-policies-category").dxPieChart({
        dataSource: dataSource,
        series: [
            {
                argumentField: "cover",
                valueField: "claims",
                label:{
                    visible: true,
                    customizeText: function(e) {
                        return e.argumentText;
                    },
                    connector:{
                        visible:true,
                        width: 1
                    }
                },
                border: {
                    visible: true,
                    color: '#00306A',
                    width: 0.5
                }
            }
        ],
        customizePoint: function (bar) {
            var colors = ['#08407A','#164E86','#245C93','#336BA0','#4179AD','#5088BA','#5E96C6','#6CA4D3','#7BB3E0','#89C1ED'];
            return {color: colors[bar.index]};
        },
        legend: {
            visible: false
        },
        margin: {
            top: 90,
            bottom: 20,
        },
        tooltip: {
            enabled: true,
            zIndex: 2000,
            customizeTooltip: function (arg) {
                return {
                    text: arg.valueText
                };
            }
        }
    });
}

function updateMap(data) {

    // States in Mexico by id: 2427 - 2459 (33 entities / 32 states)
    // Note: 'Estado de Mexico' and 'Mexico' are the same. It is duplicated
    idMap = {
        '2427' : '26',
        '2428' : '14',
        '2429' : '13',
        '2430' : '1',
        '2431' : '2',
        '2432' : '16',
        '2433' : '17',
        '2434' : '30',
        '2435' : '10',
        '2436' : '21',
        '2437' : '12',
        '2438' : '27',
        '2439' : '5',
        '2440' : '11',
        '2441' : '28',
        '2442' : '12',
        '2443' : '29',
        '2444' : '9',
        '2445' : '22',
        '2446' : '19',
        '2447' : '4',
        '2448' : '7',
        '2449' : '24',
        '2450' : '31',
        '2451' : '23',
        '2452' : '20',
        '2453' : '15',
        '2454' : '3',
        '2455' : '18',
        '2456' : '8',
        '2457' : '6',
        '2458' : '32',
        '2459' : '25'
    };

    validData = [];
    validData['data'] = [];

    for (var i = 0; i <= 32; i++) {
        validData[i.toString()] = '0-5';
        validData['data'][i.toString()] = 0;
    }

    for(var i = 0; i < data.length; i++) {
        var value = idMap[data[i]['state_id']];
        var claims = data[i]['claims'];
        var range = '';
        if(claims == 0) {
            range= '0';
        }
        else if(claims >= 1 && claims <=5) {
            range = '1-5';
        }
        else if(claims >= 6 && claims <= 10) {
            range = '6-10';
        }
        else if(claims >= 11 && claims <= 15) {
            range = '11-15';
        }
        else if(claims >= 16 && claims <= 20) {
            range = '16-20';
        }
        else {
            range = '21+';
        }

        validData[value] = range;
        validData['data'][value] = claims;
    }

    window.MexicoMap.series.regions[0].setValues(validData);
}

function generateMap() {

    validData = [];
    for (var i = 0; i <= 32; i++) {
        validData[i.toString()] = '0-5';
    }


    var MexicoMap = new jvm.Map({
        container: $('#mexico'),
        map: 'mx_en',
        enableZoom: false,
        showTooltip: true,
        backgroundColor: '#FFFFFF',
        borderColor: '#DDD',
        borderOpacity: 1,
        borderWidth: 1,
        color: '#CCCCCC',
        hoverColor: '#CCC',
        hoverOpacity: null,
        normalizeFunction: 'linear',
        scaleColors: ['#98D0FA', '#08407A'],
        regionsSelectable: false,
        regionStyle: {
            initial: {
                fill: '#FFF',
                "fill-opacity": 1,
                stroke: '#333',
                "stroke-width": 2,
                "stroke-opacity": 1
            },
            hover: {
                fill: '#CCC',
                "fill-opacity": 0.3,
                cursor: 'pointer'
            },
        },
        regionLabelStyle: {
            initial: {
                'font-family': 'Verdana',
                'font-size': '12',
                'font-weight': 'bold',
                cursor: 'default',
                fill: 'black'
            },
            hover: {
                cursor: 'pointer'
            }
        },
        onRegionTipShow: function(e, el, code){
            el.html(el.html() + ' (' + validData["data"][code] + ')' );
        },
        series: {
            regions: [{
                scale: {
                    "0": '#FFFFFF',
                    "1-5": '#C5E1F7',
                    "6-10": '#9FC0DE',
                    "11-15": '#6690B8',
                    "16-20": '#2D6093',
                    "21+": '#08407A',
                },
                attribute: 'fill',
                values: validData,
                legend: {
                    vertical: true,
                    title: "{!! trans('global.Claims') !!}"
                }
            }]
        }
    });

    return MexicoMap;
}

function generateCitiesTable() {
    window.citiesTable = $('#cities-table').DataTable({
        paging: false,
        info: false,
        searching: false,
        ordering: false,
        columns: [
            {"data": "city_name"},
            {"data": "state_name"},
            {"data": "claims"},
        ]
    });
}

$('#stats-range').daterangepicker({
        format: "DD/MM/YYYY",
        startDate: TEN_DAYS_AGO,
        endDate: TODAY,
        ranges: {
            'Hoy': [moment(), moment()],
            'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Últimos 7 días': [moment().subtract(6, 'days'), moment()],
            'Últimos 30 días': [moment().subtract(29, 'days'), moment()],
            'Últimos 90 días': [moment().subtract(3, 'month'), moment()],
            'Mes actual': [moment().startOf('month'), moment().endOf('month')],
            'Último mes': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }
);

var updateStats = function () {
    var range = $('#stats-range').val();
    $.ajax({
            method: "GET",
            url: "stats/claims-stats",
            data: {
                dates: range,
            }
        })
        .done(function (data) {
            newClaimsByDay(data['claimsByDay']);
            claimsByCover(data['claimsByCover']);
            affectedPolicies(data['claimsByCover']);
            updateCounters(data['counters']);
            updateMap(data['claimsByStates']);
            window.citiesTable.ajax.url('/stats/claims-cities?dates=' + $('#stats-range').val());
            window.citiesTable.ajax.reload();
            runCounters();
        });
}

$('#stats-range').on('apply.daterangepicker', function(ev, picker) {
    updateStats();
});

$(document).ready(function()
{
    window.MexicoMap = generateMap();
    generateCitiesTable();
    $('#stats-range').val(TEN_DAYS_AGO.format('DD/MM/YYYY') + ' - ' + TODAY.format('DD/MM/YYYY'));
    updateStats();
    affectedPolicies();
});
