jQuery(document).ready(function ($) {

    // Get selected state
    var current_country = $('form #country').val();
    var current_state = $('form #state').val();

    // Event for country
    $('form #country').change(function () {
        var country_id = this.value;
        if (country_id) {
            if (current_country !== country_id) {
                current_country = country_id;
                loadStates(current_country);
            }
        }
        else {
            current_state = 0;
            $('form #state option').remove();
            var selectBox = $("form #state").data('selectBox-selectBoxIt');
            selectBox.refresh();
        }
    });

    // Event for state
    $('form #state').change(function () {
        var state_id = this.value;
        if (state_id) {
            if (current_state !== state_id) {
                current_state = state_id;
                loadCities(current_state);
            }
        }
        else {
            current_state = 0;
            $('form #city option').remove();
            var selectBox = $("form #city").data('selectBox-selectBoxIt');
            selectBox.refresh();
        }
    });

    function loadStates(country_id) {
        var url = '/api/geo-location/states/' + country_id;

        if ($('form #city').data('selected-id')) {
            url = url + '/' + $('form #city').data('selected-id');
        }

        $.get(url, function (states) {
            $('form #state option').remove();
            $('form #state').html(states);
            var selectBox = $("form #state").data('selectBox-selectBoxIt');
            selectBox.refresh();

            // Clear Cities
            clearCities();
        });
    }

    function loadCities(category_id) {
        var url = '/api/geo-location/cities/' + category_id;

        if ($('form #city').data('selected-id')) {
            url = url + '/' + $('form #city').data('selected-id');
        }

        $.get(url, function (cities) {
            $('form #city option').remove();
            $('form #city').html(cities);
            var selectBox = $("form #city").data('selectBox-selectBoxIt');
            selectBox.refresh();
        });
    }

    function clearCities()
    {
        $('form #city option').remove();
        var selectBox = $("form #city").data('selectBox-selectBoxIt');
        selectBox.refresh();
    }
});