<?php

namespace App\Repositories\Models;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserRepository
 * @package namespace App\Repositories\Models;
 */
interface UserRepository extends RepositoryInterface
{
    //
}
