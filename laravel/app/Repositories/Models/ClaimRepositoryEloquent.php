<?php

namespace App\Repositories\Models;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Entities\Models\Claim;
use Carbon\Carbon;

/**
 * Class ClaimRepositoryEloquent
 * @package namespace App\Repositories\Models;
 */
class ClaimRepositoryEloquent extends BaseRepository implements ClaimRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Claim::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function findByDate($date, $columns = array('*'))
    {
        $this->applyCriteria();
        $this->applyScope();
        $model = $this->model
            ->where('occurred_at', '>=', $date->copy()->startOfDay())
            ->where('occurred_at', '<=', $date->copy()->endOfDay())
            ->get($columns);
        $this->resetModel();
        return $this->parserResult($model);
    }

    public function findByDateRange($start, $end, $columns = array('*'))
    {
        $this->applyCriteria();
        $this->applyScope();
        $model = $this->model
            ->where('occurred_at', '>=', $start->copy()->startOfDay())
            ->where('occurred_at', '<=', $end->copy()->endOfDay())
            ->get($columns);
        $this->resetModel();
        return $this->parserResult($model);
    }
}
