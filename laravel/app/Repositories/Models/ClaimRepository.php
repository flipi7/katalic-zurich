<?php

namespace App\Repositories\Models;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ClaimRepository
 * @package namespace App\Repositories\Models;
 */
interface ClaimRepository extends RepositoryInterface
{
    public function findByDate($date);
    public function findByDateRange($start, $end);
}
