<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\Email\EmailMapper;
use App\Http\Controllers\Email\EmailExtractor;

class checkEmails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check new claims in the email';

    /**
     * The EmailMapper Service
     *
     * @var emailMapper
     */
    protected $emailMapper;

    /**
     * The EmailExtractor Service
     *
     * @var emailExtractor
     */
    protected $emailExtractor;

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();

        $this->emailExtractor = new EmailExtractor();
        $this->emailMapper = new EmailMapper($this->emailExtractor);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Checking for new emails...');
        $emails = $this->emailMapper->map();
        if (count($emails) > 0) {
            $this->info(count($emails) . ' new emails have been found and generated claims.');
            foreach($emails as $email) {
                $this->info(implode(' - ', $email));
                // $this->info($email);
            }
        }
        else {
            $this->info('No new emails were found.');
        }
    }
}
