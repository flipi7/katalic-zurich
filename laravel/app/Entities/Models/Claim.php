<?php

namespace App\Entities\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Claim extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = [];

}
