<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;


abstract class Request extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    protected function failedValidation(Validator $validator)
    {
        // Save into session form details
        $this->session()->flash('formResponse', [
            'route' => $this->fullUrl(),
            'method' => $this->getMethod()
        ]);
        parent::failedValidation($validator);
    }
}
