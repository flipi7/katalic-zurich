<?php

namespace App\Http\Requests\Claim;

use App\Models\Survey;
use App\Http\Requests\Request;


class SurveysStorePostRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
//        $rules = [
//            'claim.survey_template_id' => ['required', 'numeric', 'exists:surveys,id'],
//        ];
//
//        // Survey Rules
//        if ($this->input('claim.survey_template_id')) {
//            $templateId = $this->input('claim.survey_template_id');
//            $template = Survey::findOrFail($templateId);
//
//            $questions = $template->questions;
//
//            foreach ($questions as $question) {
//                $rules['claim.answer.' . $question->id] = ['required', 'in:yes,no'];
//            }
//        }

        return [];
    }

    protected function getRedirectUrl()
    {
        return parent::getRedirectUrl() . '#survey';
    }
}
