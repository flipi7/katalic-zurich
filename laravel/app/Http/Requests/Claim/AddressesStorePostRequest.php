<?php

namespace App\Http\Requests\Claim;

use App\Http\Requests\Request;

class AddressesStorePostRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'claim.address.address' => ['required'],
            'claim.address.country_id' => ['required', 'numeric', 'exists:countries,id'],
            'claim.address.state_id' => ['required', 'numeric', 'exists:states,id'],
            'claim.address.city_id' => ['required', 'numeric', 'exists:cities,id'],
        ];

        return $rules;
    }
}
