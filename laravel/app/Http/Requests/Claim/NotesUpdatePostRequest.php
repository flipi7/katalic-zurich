<?php

namespace App\Http\Requests\Claim;

use App\Http\Requests\Request;

class NotesUpdatePostRequest extends Request
{
    protected $errorBag = 'claim_notes';

    public function rules()
    {
        return [
            'description' => ['required']
        ];
    }

    protected function getRedirectUrl()
    {
        return parent::getRedirectUrl() . '#notes';
    }
}
