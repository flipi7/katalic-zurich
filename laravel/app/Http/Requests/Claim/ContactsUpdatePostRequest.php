<?php

namespace App\Http\Requests\Claim;

use App\Http\Requests\Request;

class ContactsUpdatePostRequest extends Request
{
    protected $errorBag = 'claim_contacts';

    public function rules()
    {
        return [
            'name' => ['required'],
            'phone' => ['required', 'numeric'],
            'email' => ['required', 'email'],
            'type' => ['required']
        ];
    }

    protected function getRedirectUrl()
    {
        return parent::getRedirectUrl() . '#contacts';
    }
}
