<?php

namespace App\Http\Requests;

use App\Models\Role;
use App\Models\SurveyTemplate;

class ClaimsStorePostRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'claim.policy_number' => ['required'],
            'claim.policy_holder' => ['required'],
            'claim.description' => ['required'],
            'claim.amount_loss' => ['required', 'numeric'],
            'claim.currency' => ['required', 'in:mxn,usd'],
            'claim.occurred' => ['required', 'date_format:d/m/Y H:i'],
        ];

        return $rules;
    }
}
