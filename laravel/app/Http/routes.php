<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/* Api */
Route::group(['prefix' => 'api', 'namespace' => 'Api'], function () {
    // Users
    Route::controller('geo-location', 'GeoLocationController');
});

/* Resources */
Route::group(['namespace' => 'Resources'], function () {
    // Users
    Route::resource('users', 'UsersController');

    // Claims

    Route::post('claims/{id}/update-info', ['as' => 'claims.update-info', 'uses' => 'ClaimsController@updateInfo']);
    Route::post('claims/{id}/update-description', ['as' => 'claims.update-description', 'uses' => 'ClaimsController@updateDescription']);
    Route::post('claims/{id}/update-ike', ['as' => 'claims.update-ike', 'uses' => 'ClaimsController@updateIke']);

    Route::get('claims/{id}/{status}', ['as' => 'claims.status', 'uses' => 'ClaimsController@show']);

    Route::post('claims/{id}/verification/approve', ['as' => 'claims.dataVerification.approve', 'uses' => 'Claim\DataVerificationController@approve']);
    Route::post('claims/{id}/verification/cancel', ['as' => 'claims.dataVerification.cancel', 'uses' => 'Claim\DataVerificationController@cancel']);

    Route::post('claims/{id}/report/confirm', ['as' => 'claims.dataReport.confirm', 'uses' => 'Claim\DataReportController@confirm']);

    Route::post('claims/{id}/processing/confirm', ['as' => 'claims.processing', 'uses' => 'ClaimsController@processing']);
    Route::post('claims/{id}/finished/confirm', ['as' => 'claims.finished', 'uses' => 'ClaimsController@finished']);

    // Insured claim view
    Route::get('/insured', ['as' => 'claims.public.login', 'uses' => 'ClaimsController@publicLogin']);
    Route::post('/insured', ['as' => 'claims.public.login', 'uses' => 'ClaimsController@postPublicLogin']);

    Route::resource('claims', 'ClaimsController');
    Route::resource('claims.dataIke', 'Claim\DataIkeController');
    Route::resource('claims.dataContact', 'Claim\DataContactController');
    Route::resource('claims.dataVerification', 'Claim\DataVerificationController');
    Route::resource('claims.dataVisits', 'Claim\DataVisitsController');
    Route::resource('claims.dataPreliminary', 'Claim\DataPreliminaryController');
    Route::resource('claims.dataReport', 'Claim\DataReportController');
    Route::resource('claims.damageAssessment', 'Claim\DamageAssessmentController');
    Route::resource('claims.contacts', 'Claim\ContactsController');
    Route::resource('claims.notes', 'Claim\NotesController');
    Route::resource('claims.documents', 'Claim\DocumentsController');
    Route::resource('claims.addresses', 'Claim\AddressesController');
    Route::resource('claims.surveys', 'Claim\SurveysController');
    Route::resource('claims.checkList', 'Claim\CheckListAnswersController');
    Route::resource('claims.statuses', 'Claim\StatusesController', ['only' => 'store']);

    // Stats
    Route::get('stats/users', ['as' => 'stats.users', 'uses' => 'StatsController@users' ]);
    Route::get('stats/external', ['as' => 'stats.external', 'uses' => 'StatsController@external' ]);
    Route::get('stats/get-claims', ['as' => 'stats.get-claims', 'uses' => 'StatsController@getClaims' ]);
    Route::get('stats/claims-cities', ['as' => 'stats.claims-cities', 'uses' => 'StatsController@claimsByCities' ]);
    Route::get('stats/claims-stats', ['as' => 'stats.claims-stats', 'uses' => 'StatsController@claimsStats' ]);
    Route::resource('stats', 'StatsController');
});

// Dashboard
Route::get('dashboard', ['as' => 'dashboard.index', 'uses' => 'DashboardController@getIndex']);

// Authentication routes...
Route::get('auth/logout', ['as' => 'auth.logout', 'uses' => 'Auth\AuthController@getLogout']);
Route::get('/', ['as' => 'auth.login', 'uses' => 'Auth\AuthController@getLogin']);
Route::post('/', ['as' => 'auth.login', 'uses' => 'Auth\AuthController@postLogin']);

// Insured claim view
Route::get('/insured', ['as' => 'public.login', 'uses' => 'Resources\PublicClaimsController@publicLogin']);
Route::post('/insured', ['as' => 'public.login', 'uses' => 'Resources\PublicClaimsController@postPublicLogin']);
Route::get('/insured/{hash}', ['as' => 'public.index', 'uses' => 'Resources\PublicClaimsController@publicClaim']);