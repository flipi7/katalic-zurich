<?php
// Dashboard
Breadcrumbs::register('dashboard', function ($breadcrumbs) {
    $breadcrumbs->push('Dashboard', route('dashboard.index'));
});

/* USERS */

// Dashboard > Users
Breadcrumbs::register('users', function ($breadcrumbs) {
    //$breadcrumbs->parent('dashboard');
    $breadcrumbs->push(trans('global.Users'), route('users.index'));
});

// Dashboard > Users > [User]
Breadcrumbs::register('user', function ($breadcrumbs, $user) {
    $breadcrumbs->parent('users');
    $breadcrumbs->push($user->name, route('users.edit', $user));
});

// Dashboard > Users > [User]
Breadcrumbs::register('user.edit', function ($breadcrumbs, $user) {
    $breadcrumbs->parent('user', $user);
    $breadcrumbs->push(trans('global.Edit'), route('users.edit', $user));
});

// Dashboard > Users > Create]
Breadcrumbs::register('users.create', function ($breadcrumbs) {
    $breadcrumbs->parent('users');
    $breadcrumbs->push(trans('global.Add New User'), route('users.create'));
});

/* CLAIMS */

// Dashboard > Claims
Breadcrumbs::register('claims', function ($breadcrumbs) {
    //$breadcrumbs->parent('dashboard');
    $breadcrumbs->push(trans('global.Claims'), route('claims.index'));
});

// Dashboard > Claims > Create
Breadcrumbs::register('claims.create', function ($breadcrumbs) {
    $breadcrumbs->parent('claims');
    $breadcrumbs->push(trans('global.Add New Claim'), route('claims.create'));
});

// Dashboard > Claims > [Claim]
Breadcrumbs::register('claim', function ($breadcrumbs, $claim) {
    $breadcrumbs->parent('claims');
    $breadcrumbs->push($claim->id, route('claims.show', $claim));
});

// Dashboard > Claims > [Claim] > Edit
Breadcrumbs::register('claim.edit', function ($breadcrumbs, $claim) {
    $breadcrumbs->parent('claim', $claim);
    $breadcrumbs->push(trans('global.Edit'), route('claims.edit', $claim));
});

/* STATS */

Breadcrumbs::register('stats', function ($breadcrumbs) {
    $breadcrumbs->push(trans('global.Statistics'), route('claims.index'));
});