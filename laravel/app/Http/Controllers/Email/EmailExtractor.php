<?php

namespace App\Http\Controllers\Email;

use App\Models\EmailUpdate;
use Carbon\Carbon;

/**
 * EmailExtractor
 *
 * @author Jonathan O. Nieto Lajo <johan.nieto.lajo@gmail.com>
 */

class EmailExtractor
{
    /**
     * Constructor.
     *
     */

    public function __construct()
    {
        // Construct
    }

    public function extractEmailData($mailbox)
    {
        $data = array();

        $emailUpdates = EmailUpdate::all()->last();

        // If it is the first time we are checking the emails,
        // we may not want to check all previous emails
        if ($emailUpdates == null) {
            if ($mailbox) {
                $lastId = $mailbox->countMessages();
                if ($lastId) {
                    $email = $mailbox->formatMessage($lastId);
                    EmailUpdate::insert([
                        'last_email_id' => $email['uid'],
                        'number_of_emails' => 0,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                    ]);
                }
            }
            return $data;
        }

        if ($mailbox) {
            $data = $this->extractMailboxData($mailbox, $emailUpdates);

            $lastMail = null;
            if (count($data)>0) {
                $lastMail = $data[0];
            }

            if ($lastMail) {
                try {
                    EmailUpdate::insert([
                        'last_email_id' => $lastMail['uid'],
                        'number_of_emails' => count($data),
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                    ]);
                } catch(\Exception $e) {
                    //
                }
            }
        }

        return $data;
    }

    protected function extractMailboxData($mailbox, $imapUpdated)
    {
        $data = array();

        if ($mailbox) {
            $total = $mailbox->countMessages();
            for ($i = $total; $i > 0; $i--) {
                $email = $mailbox->formatMessage($i);
                try {
                    $uid  = $email['uid'];
                } catch (\Exception $e) {
                    continue;
                }
                if ($uid <= $imapUpdated['last_email_id']) {
                    return $data;
                }
                $data[] = $email;
            }
        }

        return $data;
    }
}