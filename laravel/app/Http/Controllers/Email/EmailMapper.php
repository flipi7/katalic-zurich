<?php

namespace App\Http\Controllers\Email;

use App\Http\Controllers\Email\Lib\Imap;
use App\Models\Claim;
use App\Models\User;
use App\Models\Activity;
use App\Events\ClaimWasCreated;
use \Carbon\Carbon;

/**
 * EmailMapper
 *
 * @author Jonathan O. Nieto Lajo <johan.nieto.lajo@gmail.com>
 */
class EmailMapper
{
    const CONNECTION_TIMEOUT = 30;
    protected $emailExtractor = null;
    protected $server         = 'imap.1and1.es';
    protected $port           = 143;
    protected $username       = 'testing@theshopexpert.com';
    protected $password       = 'g1zm0g1zm0';
    protected $encryption     = 'tls';
    protected $imap           = null;

    /**
     * Constructor.
     *
     * @param EmailExtractor      $emailExtractor
     */
    public function __construct(EmailExtractor $emailExtractor)
    {
        $this->emailExtractor = $emailExtractor;
        $this->startImap();
    }

    public function startImap()
    {
        try {
            imap_timeout(IMAP_READTIMEOUT, self::CONNECTION_TIMEOUT);
            imap_timeout(IMAP_WRITETIMEOUT, self::CONNECTION_TIMEOUT);
            imap_timeout(IMAP_OPENTIMEOUT, self::CONNECTION_TIMEOUT);
            $this->imap = new Imap($this->server, $this->port, $this->username, $this->password, $this->encryption);

            if ($this->imap->isConnected() === false) {
                return;
            }

            $result = $this->imap->selectFolder('Inbox');
            if (!$result) {
                $result = $this->imap->selectFolder('INBOX');
            }
        } catch (\Exception $e) {
            return;
        }
    }

    public function map()
    {
        $emails = $this->emailExtractor->extractEmailData($this->imap);
        $data = $this->useData($emails);
        return $data;
    }

    public function useData($emailData)
    {
        $data = array();
        if (count($emailData) > 0) {
            foreach ($emailData as $email) {
                $data[] = $this->generateClaimFromEmail($email);
            }
        }
        return $data;
    }

    protected function generateClaimFromEmail($emailData)
    {
        // Altern.: (NOMBRE DEL ASEGURADO:.?([a-zA-Z0-9\sá-úÁ-Ú]*))
        preg_match('/NÚMERO DE PÓLIZA:\s+(\d*)/', $emailData['body'], $policyNumber);
        preg_match('/<div[^<>].*?[^<>]>NOMBRE DEL ASEGURADO:\s?(.*?)<\/div>/', $emailData['body'], $policyName);
        preg_match('/<div[^<>].*?[^<>]>NOMBRE DEL OPERADOR QUE CAPTURA:\s?(.*?)<\/div>/', $emailData['body'], $agent);
        preg_match('/<div[^<>].*?[^<>]>NOMBRE DE QUIEN SERÁ EL CONTACTO:\s?(.*?)<\/div>/', $emailData['body'], $contactName);
        preg_match('/<div[^<>].*?[^<>]>TELEFONOS DE CONTACTO:\s?(.*?)<\/div>/', $emailData['body'], $contactPhone);
        preg_match('/([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9._-]+)/', $emailData['body'], $contactEmail);
        preg_match('/<div[^<>].*?[^<>]>CALLE Y NÚMERO:\s?(.*?)<\/div>/', $emailData['body'], $address);
        preg_match('/<div[^<>].*?[^<>]>COLONIA:\s?(.*?)<\/div>/', $emailData['body'], $neighborhood);
        preg_match('/<div[^<>].*?[^<>]>ENTIDAD:\s?(.*?)<\/div>/', $emailData['body'], $state);
        preg_match('/<div[^<>].*?[^<>]>MUNICIPIO:\s?(.*?)<\/div>/', $emailData['body'], $district);
        preg_match('/<div[^<>].*?[^<>]>PAÍS:\s?(.*?)<\/div>/', $emailData['body'], $country);
        preg_match('/<div[^<>].*?[^<>]>DESCRIPCION DEL SINIESTRO:\s?(.*?)<\/div>/', $emailData['body'], $description);
        preg_match('/<div[^<>].*?[^<>]>MONTO ESTIMADO:\s?(.*?)<\/div>/', $emailData['body'], $amountLoss);
        preg_match('/<div[^<>].*?[^<>]>FECHA Y HORA DEL SINIESTRO:\s?(.*?)<\/div>/', $emailData['body'], $date);
        preg_match('/<div[^<>].*?[^<>]>FOLIO ASIGNADO:\s?(.*?)<\/div>/', $emailData['body'], $folio);
        preg_match('/<div[^<>].*?[^<>]>AJUSTADOR ASIGNADO:\s?(.*?)<\/div>/', $emailData['body'], $adjuster);

        $emailInfo = [
            'policyNumber' => $policyNumber[1],
            'policyName'   => $policyName[1],
            'agent'        => $agent[1],
            'contactName'  => $contactName[1],
            'contactPhone' => $contactPhone[1],
            'contactEmail' => $contactEmail[1],
            'address'      => $address[1],
            'neighborhood' => $neighborhood[1],
            'state'        => $state[1],
            'district'     => $district[1],
            'country'      => $country[1],
            'description'  => $description[1],
            'amountLoss'   => $amountLoss[1],
            'date'         => $date[1],
            'folio'        => $folio[1],
            'adjuster'     => $adjuster[1],
        ];

        // Create claim from email info
        $claimInfo = [
            'policy_number' => $policyNumber[1],
            'policy_holder' => $policyName[1],
            'description'   => $description[1],
            'amount_loss'   => $amountLoss[1],
            'created_at'    => \Carbon\Carbon::now(),
            'updated_at'    => \Carbon\Carbon::now(),
            'occurred_at'   => \Carbon\Carbon::createFromFormat('Y-m-d H:i', $date[1]),
        ];
        $this->createClaim($claimInfo);

        return $emailInfo;
    }

    protected function createClaim($claimInfo)
    {
        $user = User::findOrFail(1);
        $claim = new Claim($claimInfo);
        $user->claims()->save($claim);

        // Create Activity
        $activity = new Activity(['activity' => 'created', 'date' => Carbon::now(), 'claim_id' => $claim->id, 'user_id' => $user->id]);
        $claim->activities()->save($activity);

        // Fire event
        event(new ClaimWasCreated($claim));
    }
}