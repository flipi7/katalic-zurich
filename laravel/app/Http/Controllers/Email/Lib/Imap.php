<?php

/**
 * Helper class for imap access
 *
 * @package    protocols
 * @copyright  Copyright (c) Tobias Zeising (http://www.aditu.de)
 * @license    GPLv3 (http://www.gnu.org/licenses/gpl-3.0.html)
 * @author     Tobias Zeising <tobias.zeising@aditu.de>
 */
namespace App\Http\Controllers\Email\Lib;

class Imap {

    /**
     * imap connection
     */
    private $imap = false;

    /**
     * mailbox url string
     */
    private $mailbox = "";

    /**
     * currentfolder
     */
    private $folder = "Inbox";

    private $serverEncoding = "UTF-8";




    /**
     * initialize imap helper
     *
     * @return void
     * @param $mailbox imap_open string
     * @param $username
     * @param $password
     * @param $encryption ssl or tls
     */
    public function __construct($server, $port, $username, $password, $encryption = false, $logger = null) {

        set_error_handler(array($this, 'handleError'));

        $enc = '';
        $mailbox = $server.":".$port;
        $protocol = ($port == '110' || $port == '995') ? '/pop3' : '/imap';

        $this->username = $username;
        $this->password = $password;

        if ($encryption!=null && isset($encryption) && $encryption=='ssl')
            $enc = '/ssl/novalidate-cert';
        else if($encryption!=null && isset($encryption) && $encryption=='tls')
            $enc = '/tls/novalidate-cert';

        $this->mailbox = "{" . $mailbox . $protocol . $enc . "}";
        $this->imap = @imap_open($this->mailbox, $this->username, $this->password);



    }



    public function handleError($errno, $errstr, $errfile, $errline) {

        /*if (strpos($errstr,'IMAP connection broken') !== false) {
          throw new ConnectionBroken();
        }*/

        return false;

    }
    /**
     * close connection
     */
    function __destruct() {
        if($this->imap!==false)
            imap_close($this->imap);
    }


    /**
     * returns true after successfull connection
     *
     * @return bool true on success
     */
    public function isConnected() {
        return $this->imap !== false;
    }


    /**
     * returns last imap error
     *
     * @return string error message
     */
    public function getError() {
        return imap_last_error();
    }


    /**
     * select given folder
     *
     * @return bool successfull opened folder
     * @param $folder name
     */
    public function selectFolder($folder) {
        $result = imap_reopen($this->imap, $this->mailbox . $folder);
        if($result === true)
            $this->folder = $folder;
        return $result;
    }


    /**
     * returns all available folders
     *
     * @return array with foldernames
     */
    public function getFolders() {
        $folders = imap_list($this->imap, $this->mailbox, "*");
        return str_replace($this->mailbox, "", $folders);
    }


    /**
     * returns the number of messages in the current folder
     *
     * @return int message count
     */
    public function countMessages() {
        return imap_num_msg($this->imap);
    }


    /**
     * returns the number of unread messages in the current folder
     *
     * @return int message count
     */
    public function countUnreadMessages() {
        $result = imap_search($this->imap, 'UNSEEN');
        if($result===false)
            return false;
        return count($result);
    }

    /**
     * returns unseen emails in the current folder
     *
     * @return array messages
     * @param $withbody without body
     */
    public function getUnreadMessages($withbody=true){

        $result = imap_search($this->imap, 'UNSEEN');
        foreach($result as $k=>$i){

            $email = $this->formatMessage($i, $withbody);

            if ($email)
                $emails[] = $email;
        }
        return $emails;
    }

    public function getUnformattedMessages($withbody=true) {


    }



    /**
     * returns all emails in the current folder
     *
     * @return array messages
     * @param $withbody without body
     */
    public function getMessages($withbody = true) {
        $count = $this->countMessages();
        $emails = array();
        for($i=1;$i<=$count;$i++) {
            $email = $this->formatMessage($i, $withbody);
            if ($email)
                $emails[] = $email;
        }

        //sort emails descending by date
        usort($emails, function($a, $b) {
            try {
                $datea = new \DateTime($a['date']);
                $dateb = new \DateTime($b['date']);
            } catch(\Exception $e) {
                return 0;
            }
            if ($datea == $dateb)
                return 0;
            return $datea < $dateb ? 1 : -1;
        });

        return $emails;
    }

    /**
     * @param $id
     * @param bool $withbody
     * @return array
     */
    public function formatMessage($id, $withbody=true){

        try {

            $header = imap_headerinfo($this->imap, $id);
            // fetch unique uid
            if (empty($header))
                return;

            $uid = imap_uid($this->imap, $id);

            if (!property_exists($header, 'from'))
                return;

            // get email data
            if ( isset($header->subject) && strlen($header->subject) > 0 ) {
                $subjectParts = imap_mime_header_decode($header->subject);

                $subject = "";
                foreach ($subjectParts as $subjectPart) {

                    $subject .= $subjectPart->text;
                }

            } else {
                $subject = '';
            }
            $subject = $this->convertToUtf8($subject);
            $reply_to = isset($header->reply_to) ? $this->toAddress($header->reply_to[0]) : '';
            $from = $this->toAddress($header->from[0]);

            $email = array(
                'to'       => isset($header->to) ? $this->arrayToAddress($header->to) : '',
                'from'     => $from,
                'name'     => $this->getName($header->from[0]),
                'from_normalized' => ($reply_to!='' && $from!=$reply_to) ? $reply_to : $from,
                'reply_to' => $reply_to,
                'date'     => $header->date,
                'subject'  => $subject,
                'uid'       => $uid,
                'unread'   => strlen(trim($header->Unseen))>0,
                'answered' => strlen(trim($header->Answered))>0,
            );
            if(isset($header->cc)) {
                $email['cc_name'] = $this->arrayToName($header->cc);
                $email['cc'] = $this->arrayToAddress($header->cc);
            }
            // get email body
            if($withbody===true) {
                $body = $this->getBody($uid);
                $email['body'] = $body['body'];
                $email['html'] = $body['html'];
            }

            // get attachments
            $mailStruct = imap_fetchstructure($this->imap, $id);
            //$attachments = $this->attachments2name($this->getAttachmentsV2($this->imap, $id, $mailStruct, ""));

            //NOTE we are using uid instead id otherwise some attachments were missing
            $attachments = $this->getAttachmentsV2($this->imap, $uid, $mailStruct, "");
            if(count($attachments)>0)
                $email['attachments'] = $attachments;

            return $email;

        } catch (\Exception $e) {

            echo "Here!";
            if (strpos($e->getMessage(),'IMAP connection broken') !== false) {
                $this->imap = @imap_open($this->mailbox, $this->username, $this->password);
                $this->formatMessage($id, $withbody);
            }

        }
    }

    /**
     * delete given message
     *
     * @return bool success or not
     * @param $id of the message
     */
    public function deleteMessage($id) {
        return $this->deleteMessages(array($id));
    }


    /**
     * delete messages
     *
     * @return bool success or not
     * @param $ids array of ids
     */
    public function deleteMessages($ids) {
        if( imap_mail_move($this->imap, implode(",", $ids), $this->getTrash(), CP_UID) == false)
            return false;
        return imap_expunge($this->imap);
    }


    /**
     * move given message in new folder
     *
     * @return bool success or not
     * @param $id of the message
     * @param $target new folder
     */
    public function moveMessage($id, $target) {
        return $this->moveMessages(array($id), $target);
    }


    /**
     * move given message in new folder
     *
     * @return bool success or not
     * @param $ids array of message ids
     * @param $target new folder
     */
    public function moveMessages($ids, $target) {
        if(imap_mail_move($this->imap, implode(",", $ids), $target, CP_UID)===false)
            return false;
        return imap_expunge($this->imap);
    }


    /**
     * mark message as read
     *
     * @return bool success or not
     * @param $id of the message
     * @param $seen true = message is read, false = message is unread
     */
    public function setUnseenMessage($id, $seen = true) {
        $header = $this->getMessageHeader($id);
        if($header==false)
            return false;

        $flags = "";
        $flags .= (strlen(trim($header->Answered))>0 ? "\\Answered " : '');
        $flags .= (strlen(trim($header->Flagged))>0 ? "\\Flagged " : '');
        $flags .= (strlen(trim($header->Deleted))>0 ? "\\Deleted " : '');
        $flags .= (strlen(trim($header->Draft))>0 ? "\\Draft " : '');

        $flags .= (($seen == true) ? '\\Seen ' : ' ');
        echo "\n<br />".$id.": ".$flags;
        imap_clearflag_full($this->imap, $id, '\\Seen', ST_UID);
        return imap_setflag_full($this->imap, $id, trim($flags), ST_UID);
    }


    /**
     * return content of messages attachment
     *
     * @return binary attachment
     * @param $id of the message
     * @param $index of the attachment (default: first attachment)
     */
    public function getAttachment($id, $index = 0) {
        // find message
        $attachments = false;
        $messageIndex = imap_msgno($this->imap, $id);
        $header = imap_headerinfo($this->imap, $messageIndex);
        $mailStruct = imap_fetchstructure($this->imap, $messageIndex);
        $attachments = $this->getAttachments($this->imap, $messageIndex, $mailStruct, "");

        if($attachments==false)
            return false;

        // find attachment
        if($index > count($attachments))
            return false;
        $attachment = $attachments[$index];

        // get attachment body
        $partStruct = imap_bodystruct($this->imap, imap_msgno($this->imap, $id), $attachment['partNum']);
        if ($partStruct) {
            $filename = $partStruct->ifdparameters ? (is_object($partStruct->dparameters[0]) ? $partStruct->dparameters[0]->value : 'unknown') : ($partStruct->ifparameters ? $partStruct->parameters[0]->value : 'unknown');
        }
        $message = imap_fetchbody($this->imap, $id, $attachment['partNum'], FT_UID);

        switch ($attachment['enc']) {
            case 0:
            case 1:
                $message = imap_8bit($message);
                break;
            case 2:
                $message = imap_binary($message);
                break;
            case 3:
                $message = imap_base64($message);
                break;
            case 4:
                $message = quoted_printable_decode($message);
                break;
        }

        return array(
            "name" => $attachment['name'],
            "cid" => $attachment['cid'],
            "size" => $attachment['size'],
            "content" => $message);
    }


    /**
     * add new folder
     *
     * @return bool success or not
     * @param $name of the folder
     */
    public function addFolder($name) {
        return imap_createmailbox($this->imap , $this->mailbox . $name);
    }


    /**
     * remove folder
     *
     * @return bool success or not
     * @param $name of the folder
     */
    public function removeFolder($name) {
        return imap_deletemailbox($this->imap, $this->mailbox . $name);
    }


    /**
     * rename folder
     *
     * @return bool success or not
     * @param $name of the folder
     * @param $newname of the folder
     */
    public function renameFolder($name, $newname) {
        return imap_renamemailbox($this->imap, $this->mailbox . $name, $this->mailbox . $newname);
    }


    /**
     * clean folder content of selected folder
     *
     * @return bool success or not
     */
    public function purge() {
        // delete trash and spam
        if($this->folder==$this->getTrash() || strtolower($this->folder)=="spam") {
            if(imap_delete($this->imap,'1:*')===false) {
                return false;
            }
            return imap_expunge($this->imap);

            // move others to trash
        } else {
            if( imap_mail_move($this->imap,'1:*', $this->getTrash()) == false)
                return false;
            return imap_expunge($this->imap);
        }
    }


    /**
     * returns all email addresses
     *
     * @return array with all email addresses or false on error
     */
    public function getAllEmailAddresses() {
        $saveCurrentFolder = $this->folder;
        $emails = array();
        foreach($this->getFolders() as $folder) {
            $this->selectFolder($folder);
            foreach($this->getMessages(false) as $message) {
                $emails[] = $message['from'];
                $emails = array_merge($emails, $message['to']);
                if(isset($message['cc']))
                    $emails = array_merge($emails, $message['cc']);
            }
        }
        $this->selectFolder($saveCurrentFolder);
        return array_unique($emails);
    }


    /**
     * save email in sent
     *
     * @return void
     * @param $header
     * @param $body
     */
    public function saveMessageInSent($header, $body) {
        return imap_append($this->imap, $this->mailbox . $this->getSent(), $header . "\r\n" . $body . "\r\n", "\\Seen");
    }



    // private helpers


    /**
     * get trash folder name or create new trash folder
     *
     * @return trash folder name
     */
    private function getTrash() {
        foreach($this->getFolders() as $folder) {
            if(strtolower($folder)==="trash" || strtolower($folder)==="papierkorb")
                return $folder;
        }

        // no trash folder found? create one
        $this->addFolder('Trash');

        return 'Trash';
    }


    /**
     * get sent folder name or create new sent folder
     *
     * @return sent folder name
     */
    private function getSent() {
        foreach($this->getFolders() as $folder) {
            if(strtolower($folder)==="sent" || strtolower($folder)==="gesendet")
                return $folder;
        }

        // no sent folder found? create one
        $this->addFolder('Sent');

        return 'Sent';
    }


    /**
     * fetch message by id
     *
     * @return header
     * @param $id of the message
     */
    private function getMessageHeader($id) {
        $count = $this->countMessages();
        for($i=1;$i<=$count;$i++) {
            $uid = imap_uid($this->imap, $i);
            if($uid==$id) {
                $header = imap_headerinfo($this->imap, $i);
                return $header;
            }
        }
        return false;
    }


    /**
     * convert attachment in array(name => ..., size => ...).
     *
     * @return array
     * @param $attachments with name and size
     */
    private function attachments2name($attachments) {
        $names = array();

        foreach($attachments as $attachment) {
            $names[] = array(
                'cid'  => $attachment['cid'],
                'name' => $attachment['name'],
                'size' => $attachment['size']
            );
        }
        return $names;
    }


    /**
     * convert imap given address in string
     *
     * @return string in format "email@bla.de"
     * @param $headerinfos the infos given by imap
     */
    private function toAddress($headerinfos) {
        $email = "";
        if(isset($headerinfos->mailbox) && isset($headerinfos->host)) {
            $email = $headerinfos->mailbox . "@" . $headerinfos->host;
        }
        return $email;
    }

    private function getName($headerinfos) {

        $emailUser = "";
        $name = "";
        if(isset($headerinfos->mailbox)) {
            $emailUser = $headerinfos->mailbox;
        }

        if(!empty($headerinfos->personal)) {
            $name = imap_mime_header_decode($headerinfos->personal);
            if (is_array($name)) {
                $name = !empty($name[0]->text) ? $name[0]->text : $emailUser;
            } else {
                $name = !empty($name->text) ? $name->text : $emailUser;
            }
        } else {
            $name = $emailUser;
        }
        $name = $this->convertToUtf8($name);

        return $name;

    }

    /**
     * converts imap given array of addresses in strings
     *
     * @return array with strings (e.g. ["email@bla.de", "email2@bla.de"]
     * @param $addresses imap given addresses as array
     */
    private function arrayToAddress($addresses) {
        $addressesAsString = array();
        foreach($addresses as $address) {
            $addressesAsString[] = $this->toAddress($address);
        }
        return $addressesAsString;
    }

    /**
     * converts imap given array of Names in strings
     *
     * @return array with strings (e.g. ["John Smith", "Steve Miller"]
     * @param $names imap given names as array
     */
    public function arrayToName($names) {
        $namesAsString = array();
        foreach($names as $name) {
            $namesAsString[] = $this->getName($name);
        }
        return $namesAsString;
    }

    /**
     * returns body of the email. First search for html version of the email, then the plain part.
     *
     * @return string email body
     * @param $uid message id
     */
    private function getBody($uid) {
        $body = $this->get_part($this->imap, $uid, "TEXT/HTML");
        $html = true;
        // if HTML body is empty, try getting text body
        if ($body == "") {
            $body = $this->get_part($this->imap, $uid, "TEXT/PLAIN");
            $html = false;
        }
        $body = $this->convertToUtf8($body);
        return array( 'body' => $body, 'html' => $html);
    }


    /**
     * convert to utf8 if necessary.
     *
     * @return true or false
     * @param $string utf8 encoded string
     */
    function convertToUtf8($str) {
        try {

            if(mb_detect_encoding($str, "UTF-8, ISO-8859-1, GBK")!="UTF-8")
                $str = utf8_encode($str);

            $str = iconv('UTF-8', 'UTF-8//IGNORE', $str);

        } catch (ContextErrorException $e) {
            //error converting string
            echo $str;
            echo "Detected an incomplete multibyte character in input string";
        }

        return $str;
    }


    /**
     * returns a part with a given mimetype
     * taken from http://www.sitepoint.com/exploring-phps-imap-library-2/
     *
     * @return string email body
     * @param $imap imap stream
     * @param $uid message id
     * @param $mimetype
     */
    private function get_part($imap, $uid, $mimetype, $structure = false, $partNumber = false) {
        if (!$structure) {
            $structure = imap_fetchstructure($imap, $uid, FT_UID);
        }
        if ($structure) {
            if ($mimetype == $this->get_mime_type($structure)) {
                if (!$partNumber) {
                    $partNumber = 1;
                }
                $text = imap_fetchbody($imap, $uid, $partNumber, FT_UID | FT_PEEK);
                switch ($structure->encoding) {
                    case 3: return imap_base64($text);
                    case 4: return imap_qprint($text);
                    default: return $text;
                }
            }

            // multipart
            if ($structure->type == 1) {
                foreach ($structure->parts as $index => $subStruct) {
                    $prefix = "";
                    if ($partNumber) {
                        $prefix = $partNumber . ".";
                    }
                    $data = $this->get_part($imap, $uid, $mimetype, $subStruct, $prefix . ($index + 1));
                    if ($data) {
                        return $data;
                    }
                }
            }
        }
        return false;
    }


    /**
     * extract mimetype
     * taken from http://www.sitepoint.com/exploring-phps-imap-library-2/
     *
     * @return string mimetype
     * @param $structure
     */
    private function get_mime_type($structure) {

        $primaryMimetype = array("TEXT", "MULTIPART", "MESSAGE", "APPLICATION", "AUDIO", "IMAGE", "VIDEO", "OTHER");

        if ($structure->subtype) {
            return $primaryMimetype[(int)$structure->type] . "/" . $structure->subtype;
        }
        return "TEXT/PLAIN";
    }


    /*
    get attachments of given email
    based on https://github.com/barbushin/php-imap/
    */
    private function getAttachmentsV2($imap, $uid, $part, $partNum) {

        if(empty($part->parts)) {
            return $this->getAttachmentV2($imap, $uid, $part, 0);
        }
        else {
            $attachments = [];
            foreach($part->parts as $partNum => $partStructure) {
                $attachments = array_merge($attachments, $this->getAttachmentV2($imap, $uid, $partStructure, $partNum + 1));
            }
        }

        return $attachments;

    }

    private function getAttachmentV2($imap, $uid, $partStructure, $partNum) {

        $options = FT_UID;
        $attachments = [];
        $data = $partNum ? imap_fetchbody($imap, $uid, $partNum, $options) : imap_body($imap, $uid, $options);
        if($partStructure->encoding == 1) {
            $data = imap_utf8($data);
        }
        elseif($partStructure->encoding == 2) {
            $data = imap_binary($data);
        }
        elseif($partStructure->encoding == 3) {
            $data = imap_base64($data);
        }
        elseif($partStructure->encoding == 4) {
            $data = quoted_printable_decode($data);
        }

        $params = array();
        if(!empty($partStructure->parameters)) {
            foreach($partStructure->parameters as $param) {
                $params[strtolower($param->attribute)] = $param->value;
            }
        }
        if(!empty($partStructure->dparameters)) {
            foreach($partStructure->dparameters as $param) {
                $paramName = strtolower(preg_match('~^(.*?)\*~', $param->attribute, $matches) ? $matches[1] : $param->attribute);
                if(isset($params[$paramName])) {
                    $params[$paramName] .= $param->value;
                }
                else {
                    $params[$paramName] = $param->value;
                }
            }
        }
        // attachments
        $attachmentId = $partStructure->ifid
            ? trim($partStructure->id, " <>")
            : (isset($params['filename']) || isset($params['name']) ? mt_rand() . mt_rand() : null);

        if($attachmentId) {
            if(empty($params['filename']) && empty($params['name'])) {
                $fileName = $attachmentId . '.' . strtolower($partStructure->subtype);
            }
            else {
                $fileName = !empty($params['filename']) ? $params['filename'] : $params['name'];
                $fileName = $this->decodeMimeStr($fileName, $this->serverEncoding);
                $fileName = $this->decodeRFC2231($fileName, $this->serverEncoding);
            }

            $attachment = array(
                "name"    => str_replace(array("/", "\\"), ":", $fileName),
                "partNum" => $partNum,
                "size"    => $partStructure->bytes,
                "cid"     => $partStructure->ifid ? $partStructure->id : Null,
                "data"    => $data
            );
            $attachments[] = $attachment;

        }

        if(!empty($partStructure->parts)) {
            foreach($partStructure->parts as $subPartNum => $subPartStructure) {
                if($partStructure->type == 2 && $partStructure->subtype == 'RFC822') {
                    $attachments = array_merge($attachments, $this->getAttachmentV2($imap, $uid, $subPartStructure, $partNum));
                }
                else {
                    $attachments = array_merge($attachments,  $this->getAttachmentV2($imap, $uid, $subPartStructure, $partNum . '.' . ($subPartNum + 1)));
                }
            }
        }

        return $attachments;
    }


    protected function decodeMimeStr($string, $charset = 'utf-8') {
        $newString = '';
        $elements = imap_mime_header_decode($string);
        for($i = 0; $i < count($elements); $i++) {
            if($elements[$i]->charset == 'default') {
                $elements[$i]->charset = 'iso-8859-1';
            }
            $newString .= $this->convertStringEncoding($elements[$i]->text, $elements[$i]->charset, $charset);
        }
        return $newString;
    }
    function isUrlEncoded($string) {
        $hasInvalidChars = preg_match( '#[^%a-zA-Z0-9\-_\.\+]#', $string );
        $hasEscapedChars = preg_match( '#%[a-zA-Z0-9]{2}#', $string );
        return !$hasInvalidChars && $hasEscapedChars;
    }
    protected function decodeRFC2231($string, $charset = 'utf-8') {
        if(preg_match("/^(.*?)'.*?'(.*?)$/", $string, $matches)) {
            $encoding = $matches[1];
            $data = $matches[2];
            if($this->isUrlEncoded($data)) {
                $string = $this->convertStringEncoding(urldecode($data), $encoding, $charset);
            }
        }
        return $string;
    }

    protected function convertStringEncoding($string, $fromEncoding, $toEncoding) {
        $convertedString = null;
        if($string && $fromEncoding != $toEncoding) {
            $convertedString = @iconv($fromEncoding, $toEncoding . '//IGNORE', $string);
            if(!$convertedString && extension_loaded('mbstring')) {
                $convertedString = @mb_convert_encoding($string, $toEncoding, $fromEncoding);
            }
        }
        return $convertedString ?: $string;
    }

    /**
     * get attachments of given email
     * taken from http://www.sitepoint.com/exploring-phps-imap-library-2/
     *
     * @return array of attachments
     * @param $imap stream
     * @param $mailNum email
     * @param $part
     * @param $partNum
     */

    private function getAttachments($imap, $mailNum, $part, $partNum) {

        $attachments = array();

        if (isset($part->parts)) {

            foreach ($part->parts as $key => $subpart) {
                if($partNum != "") {
                    $newPartNum = $partNum . "." . ($key + 1);
                }
                else {
                    $newPartNum = ($key+1);
                }
                $result = $this->getAttachments($imap, $mailNum, $subpart,
                    $newPartNum);


                if (count($result) != 0) {

                    if (!array_key_exists('name', $result)) {
                        $result = array_shift($result);
                        if (!array_key_exists('name', $result)) {
                            echo "Something went wrong";
                            continue;
                        }
                    }

                    array_push($attachments, $result);
                }
            }
        }

        else if (isset($part->disposition)) {
            if (strtolower($part->disposition) == "attachment" || strtolower($part->disposition) == "inline") {

                $partStruct = imap_bodystruct($imap, $mailNum,
                    $partNum);
                if ($part->ifdparameters) {
                    $name = is_array($part->dparameters) ? $part->dparameters[0]->value : Null;
                } else if (is_array($part->parameters)) {
                    $name =  $part->parameters[0]->value;
                }
                if (empty($name))
                    return array();


                $attachmentDetails = array(
                    "name"    => $name,
                    "partNum" => $partNum,
                    "enc"     => ($partStruct) ? $partStruct->encoding : Null,
                    "size"    => $part->bytes,
                    "cid"     => $part->ifid ? $part->id : Null
                );
                return $attachmentDetails;
            }
        }

        return $attachments;
    }

}