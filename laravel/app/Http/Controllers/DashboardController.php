<?php

namespace App\Http\Controllers;

class DashboardController extends AuthenticatedController
{
    public function getIndex()
    {
        return view('dashboard.index');
    }
}
