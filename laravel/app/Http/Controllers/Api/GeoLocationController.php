<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\AuthenticatedController;
use App\Models\City;
use App\Models\State;

class GeoLocationController extends AuthenticatedController
{
    public function getStates($country_id = null, $selected = null)
    {
        return view('api.geo-location.states')
            ->with('states', State::where('country_id', $country_id)->orderBy('name', 'ASC')->get())
            ->with('selected', $selected);
    }

    public function getCities($state_id = null, $selected = null)
    {
        return view('api.geo-location.cities')
            ->with('cities', City::where('state_id', $state_id)->orderBy('name', 'ASC')->get())
            ->with('selected', $selected);
    }
}
