<?php

namespace App\Http\Controllers\Resources;

use App\Events\ClaimWasCreated;
use App\Events\ClaimWasUpdated;
use App\Http\Controllers\AuthenticatedController;
use App\Models\Activity;
use App\Models\Address;
use App\Models\Claim;
use App\Models\Contact;
use App\Models\Country;
use App\Models\Role;
use App\Models\State;
use App\Models\Status;
use App\Models\Survey;
use App\Models\DataIke;
use App\Models\SurveyTemplate;
use App\Models\CheckListQuestion;
use App\Models\CheckListAnswer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\Http\Requests;
use App\Http\Requests\ClaimsStorePostRequest;

class ClaimsController extends AuthenticatedController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (Auth::user()->id == Role::AJUSTADOR) {
            // select * from claims where id in (select claim_id from claim_data_ike where adjuster_id = '10')
            $claims = Claim::whereIn('id', function($query) {
                $query->select('claim_id')
                    ->from('claim_data_ike')
                    ->where('adjuster_id', Auth::user()->id);
            });
        }
        else {
            $claims = Claim::select('*');
        }

        $searchInput = $request->input();
        if(isset($searchInput['q'])) {
            $q = $searchInput['q'];
            if(is_numeric($q)) {
                $claims = $claims->where('id', $q);
            }
            else {
                $claims = $claims->where('policy_holder', 'LIKE', '%' . $q . '%')
                    ->orWhere('description', 'LIKE', '%' . $q . '%')
                    ->orWhere('hash', 'LIKE', $q . '%');
            }
        }
        elseif(isset($searchInput['range']) || isset($searchInput['status'])) {

            $statusClaims = array();
            $rangeClaims = array();

            if(isset($searchInput['status'])) {
                $status = Status::getStatusId($searchInput['status']);

                $claims = $claims->where('last_status', '=', $status);

                if(isset($searchInput['range'])) {
                    $dateRange = $this->getDateRange($searchInput['range']);
                    $claims = $claims->where('occurred_at', '>', $dateRange);
                }
            }
            elseif(isset($searchInput['range'])) {
                $dateRange = $this->getDateRange($searchInput['range']);
                $claims = $claims->where('occurred_at', '>', $dateRange);
            }
        }
        else {
            $claims = $claims->recent();
        }

        return view('resources.claims.index', [
            'totalClaims' => $claims->count(),
            'claims' => $claims->paginate(10)//->recent()->paginate(10)
        ]);
    }

    private function getDateRange($literal)
    {
        if ($literal == 'hours') {
            $dateRange = Carbon::yesterday();
        } elseif ($literal == 'week') {
            $dateRange = Carbon::now()->subWeek();
        } elseif ($literal == 'week') {
            $dateRange = Carbon::now()->subMonth();
        } elseif ($literal == 'year') {
            $dateRange = Carbon::now()->subYear();
        } else {
            $dateRange = Carbon::now()->subYears(10);
        }

        return $dateRange;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        // Get Selected Country
        $selectedCountry = $request->old('claim.address.country_id') ? Country::find($request->old('claim.address.country_id')) : null;

        // Get Selected State
        $selectedState = $request->old('claim.address.state_id') ? State::find($request->old('claim.address.state_id')) : null;

        return view('resources.claims.create', [
            'surveys' => Survey::select('*'),
            'selectedCountry' => $selectedCountry,
            'selectedState' => $selectedState,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  ClaimsStorePostRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(ClaimsStorePostRequest $request)
    {
        $input = $request->input('claim');

        // Claim info
        $occurredAt = Carbon::createFromFormat('d/m/Y H:i', $input['occurred']);
        $claimInfo = array_merge($input, ['occurred_at' => $occurredAt]);
        $claimInfo = array_merge($claimInfo, [
            'hash' => md5(
                $claimInfo['policy_number'] .
                $claimInfo['policy_holder'] .
                $claimInfo['description'] .
                $claimInfo['occurred_at']->format('Y-m-d H:i:s')
            )
        ]);

        $claim = new Claim($claimInfo);
        $this->authUser->claims()->save($claim);

        // Ike info
        $ikeInfo = ['reference' => $input['ike_reference'], 'adjuster_id' => $input['adjuster_id']];
        $claim->dataIke()->save(new DataIke($ikeInfo));

        // Contact info
        $contactInfo = $input['contact'];
        if(array_filter($contactInfo)) {
            $contact = new Contact($contactInfo);
            $claim->contacts()->save($contact);
        }

        // Address info
        $addressInfo = $input['address'];
        if(array_filter($addressInfo)) {
            $address = new Address($addressInfo);
            $claim->address()->save($address);
        }

        // Survey info
        $surveyInfo = $input['survey_template_id'];
        if($surveyInfo == '') {
            $claim->surveys()->attach($input['survey_template_id']);
        }

        // Create Activity
        $activity = new Activity(['activity' => 'created', 'date' => Carbon::now(), 'claim_id' => $claim->id, 'user_id' => Auth::user()->id]);
        $claim->activities()->save($activity);

        // Fire event
        event(new ClaimWasCreated($claim));

        return redirect()->route('claims.status', ['id' => $claim->id, 'status' => 'created'])->with('messages.success', [trans('global.The claim has been created successfully')]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id, $status = 'created')
    {
        $claim = Claim::findOrFail($id);

        $selectedCountry = null;
        $selectedState = null;

        // Get Selected Country
        if($request->old('claim.address.country_id'))
        {
            $selectedCountry = Country::find($request->old('claim.address.country_id'));
        }
        elseif (!empty($claim->address->id)) {
            $selectedCountry =  $claim->address->country;
        }

        // Get Selected State
        if($request->old('claim.address.state_id'))
        {
            $selectedState = Country::find($request->old('claim.address.state_id'));
        }
        elseif (!empty($claim->address->id)) {
            $selectedState =  $claim->address->state;
        }

        $status = Status::getStatusId($status);

        $userRoleId = Auth::user()->roles->first()->id;
        $editable = Status::statusIsEditableForUserRole($status, $userRoleId);

        if (Status::statusIsAvailableForClaim($status, $claim, $userRoleId)) {
            return view('resources.claims.show', [
                'claim' => $claim,
                'selectedCountry' => $selectedCountry,
                'selectedState' => $selectedState,
                'status' => $status,
                'editable' => $editable
            ]);
        }
        else {
            return redirect()->route('claims.status', [ 'id' => $id, 'status' => strtolower($claim->status->name)]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        // Get Claim
        $claim = Claim::findOrFail($id);
        $claim->load(['address', 'surveys', 'surveyAnswers']);

        // Set values to request
        if (!$request->old()) {
            $claimInput = $claim->toArray();

            // Get Survey
            $claimInput['answer'] = $claim->surveyAnswers->lists('pivot.answer', 'pivot.question_id')->all();

            $request->merge([
                'claim' => $claimInput
            ]);
            $request->flash();
        }

        // Get Selected Country
        $selectedCountry = $request->old('claim.address.country_id') ? Country::find($request->old('claim.address.country_id')) : null;

        // Get Selected State
        $selectedState = $request->old('claim.address.state_id') ? State::find($request->old('claim.address.state_id')) : null;

        return view('resources.claims.edit-status-' . $claim->status->id, [
            'claim' => $claim,
            'selectedCountry' => $selectedCountry,
            'selectedState' => $selectedState
        ]);
    }

    public function updateInfo(Request $request, $claimId)
    {
        // Get Claim
        $claim = Claim::findOrFail($claimId);

        // Rules
        $rules = [
            'claim.policy_number' => ['required'],
            'claim.policy_holder' => ['required'],
            'claim.occurred_at' => ['required', 'date_format:d/m/Y H:i'],
        ];

        $this->validate($request, $rules);

        // Parse Occurred Date
        $inputClaim = $request->input('claim');
        $inputClaim['occurred_at'] = Carbon::createFromFormat('d/m/Y H:i', $inputClaim['occurred_at']);


        // Update survey
        \DB::table('claim_survey')->where('claim_id', $claim->id)->delete();
        $claim->surveys()->attach($request->input('claim.survey_template_id'));

        // Update Claim
        $claim->fill($inputClaim);
        $claim->save();

        // Fire event
        event(new ClaimWasUpdated($claim, ['type' => 'basic information']));

        return back()->with('messages.success', [trans('global.The data has been updated successfully')]);

    }

    public function updateDescription(Request $request, $claimId)
    {
        // Get Claim
        $claim = Claim::findOrFail($claimId);

        // Rules
        $rules = [
            'claim.description' => ['required'],
            'claim.amount_loss' => ['required', 'numeric'],
            'claim.currency' => ['required', 'in:mxn,usd'],
            'claim.surveys' => ['required', 'array'],
        ];

        if ($claim->status->id === Status::VERIFICATION) {
            //    $rules['claim.zurich_claim_number'] = ['required', 'numeric'];
        }

        $this->validate($request, $rules);

        // Update Claim
        $inputClaim = $request->input('claim');
        $claim->fill($inputClaim);
        $claim->save();

        // Update Surveys
        $claim->surveys()->sync($request->input('claim.surveys'));

        // Fire event
        event(new ClaimWasUpdated($claim, ['type' => 'description']));

        return back()->with('messages.success', [trans('global.The data has been updated successfully')]);

    }

    public function updateIke(Request $request, $claimId)
    {
        // Get Claim
        $claim = Claim::findOrFail($claimId);

        // Rules
        $rules = [
            'claim.ike_reference' => ['required'],
            'claim.adjuster_id' => ['required', 'numeric', 'exists:users,id', 'exists:role_user,user_id,role_id,' . Role::AJUSTADOR]
        ];

        $this->validate($request, $rules);

        // Update Claim
        $inputClaim = $request->input('claim');
        $claim->fill($inputClaim);
        $claim->save();

        // Fire event
        event(new ClaimWasUpdated($claim, ['type' => 'Ike']));

        return back()->with('messages.success', [trans('global.The data has been updated successfully')]);

    }

    public function processing(Request $request, $claimId)
    {
        // Get Claim
        $claim = Claim::findOrFail($claimId);

        // Check that the survey has been answered

        $status = Status::find(Status::PROCESSING);
        if ($claim->status->id == Status::REPORT && $claim->checkListAnswers()->count() == CheckListQuestion::all()->count()) {
            $claim->statuses()->save($status);

            $claim->last_status = $status->id;
            $claim->save();

            // Create Activity
            $activity = new Activity([
                'activity' => 'processing',
                'date' => Carbon::now(),
                'claim_id' => $claim->id,
                'user_id' => Auth::user()->id]);
            $claim->activities()->save($activity);

            return back()->with('messages.success', [trans('global.The claim is now being processed')]);
        }
        else {
            return back()->with('messages.success', [trans('global.There has been some error. Please, check the data and try again')]);
        }
    }

    public function finished(Request $request, $claimId)
    {
        // Get Claim
        $claim = Claim::findOrFail($claimId);

        $status = Status::find(Status::FINISHED);
        if ($claim->status->id == Status::PROCESSING) {
            $claim->statuses()->save($status);

            $claim->last_status = $status->id;
            $claim->save();

            // Create Activity
            $activity = new Activity([
                'activity' => 'finished',
                'date' => Carbon::now(),
                'claim_id' => $claim->id,
                'user_id' => Auth::user()->id]);
            $claim->activities()->save($activity);

            return back()->with('messages.success', [trans('global.The claim has been set to finished')]);
        }
        else {
            return back()->with('messages.success', [trans('global.There has been some error. Please, check the data and try again')]);
        }
    }

    public function publicLogin(Request $request)
    {
        return view('resources.claims.public.login');
    }

    public function postPublicLogin(Request $request)
    {
        $claimId = $request->input('claim_number');

        // Get Claim
        $claim = Claim::findOrFail($claimId);

        return view('resources.claims.public.index', ['claim' => $claim]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
