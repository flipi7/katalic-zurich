<?php

namespace App\Http\Controllers\Resources;

use App\Http\Controllers\Controller;
use App\Models\Claim;
use App\Models\SurveyTemplate;
use App\Models\Status;
use Illuminate\Http\Request;
use App\Http\Requests;

class PublicClaimsController extends Controller
{
    public function publicLogin(Request $request)
    {
        return view('resources.claims.public.login');
    }

    public function postPublicLogin(Request $request)
    {
        $claimHash = $request->input('claim_number');

        // Get Claim
        $claim = Claim::where('hash', 'LIKE', $claimHash . '%')->first();

        if(count($claim) == 1) {
            return redirect()->route('public.index', ['hash' => $claim['hash']]);
        }
        else {
            // 404
        }
    }

    public function publicClaim(Request $request, $claimHash)
    {
        // Get Claim
        $claim = Claim::where('hash', $claimHash)->first();

        $selectedCountry = '';
        $selectedState = '';

        // Get Selected Country
        if($request->old('claim.address.country_id'))
        {
            $selectedCountry = Country::find($request->old('claim.address.country_id'));
        }
        elseif (!empty($claim->address->id)) {
            $selectedCountry =  $claim->address->country;
        }

        // Get Selected State
        if($request->old('claim.address.state_id'))
        {
            $selectedState = Country::find($request->old('claim.address.state_id'));
        }
        elseif (!empty($claim->address->id)) {
            $selectedState =  $claim->address->state;
        }
   
        $status = $claim->last_status;

        return view('resources.claims.public.index', [
            'claim' => $claim,
            'selectedCountry' => $selectedCountry,
            'selectedState' => $selectedState,
            'status' => $status,
            'latestStatus' => $status,
        ]);
    }
}
