<?php

namespace App\Http\Controllers\Resources\Claim;

use App\Models\Claim;
use App\Models\DamageAssessment;
use App\Models\Status;
use App\Models\Activity;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Events\ClaimWasUpdated;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class DamageAssessmentController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @param $claimId
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $claimId)
    {
        // Validate
        $this->validate($request, [
            'claim.damage_assessment.amount_loss' => ['required'],
            'claim.damage_assessment.deductible' => ['required'],
            'claim.damage_assessment.adjustment_description' => ['required'],
        ]);

        // Get Claim
        $claim = Claim::findOrFail($claimId);

        // Parse Data
        $damageAssessmentData = $request->input('claim.damage_assessment');
        $amount_loss = str_replace(',','.',$damageAssessmentData['amount_loss']);
        $deductible = str_replace(',','.',$damageAssessmentData['deductible']);
        $damageAssessmentData['amount_loss'] = $amount_loss;
        $damageAssessmentData['deductible'] = $deductible;
        $claim->damageAssessments()->save(new DamageAssessment($damageAssessmentData));

        // If a damage assessment is added after the report
        // has been confirmed, notify it with an activity
        if ($claim->status->id >= Status::REPORT) {
            event(new ClaimWasUpdated($claim, ['type' => 'damage assessment', 'add' => TRUE]));
        }

        return back()->with('messages.success', [trans('global.The damage assessment has been saved successfully')]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     * @param  int $claimId
     * @param  int $damageAssessmentId
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $claimId, $damageAssessmentId)
    {
        // Validate
        $this->validate($request, [
            'claim.damage_assessment.amount_loss' => ['required'],
            'claim.damage_assessment.deductible' => ['required'],
            'claim.damage_assessment.adjustment_description' => ['required'],
        ]);

        // Get Claim
        $claim = Claim::findOrFail($claimId);
        $damageAssessment = DamageAssessment::findOrFail($damageAssessmentId);

        // Parse Data
        $damageAssessmentData = $request->input('claim.damage_assessment');
        $damageAssessment->fill($damageAssessmentData);
        $claim->damageAssessments()->save($damageAssessment);

        // If a damage assessment is updated after the report
        // has been confirmed, notify it with an activity
        if ($claim->status->id >= Status::REPORT) {
            event(new ClaimWasUpdated($claim, ['type' => 'damage assessment']));
        }

        return back()->with('messages.success', [trans('global.The damage assessment has been updated successfully')]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     * @param  int $claimId
     * @param  int $damageAssessmentId
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $claimId, $damageAssessmentId)
    {
        // Get Claim
        $claim = Claim::findOrFail($claimId);
        $damageAssessment = DamageAssessment::findOrFail($damageAssessmentId);
        $damageAssessment->delete();

        // If a damage assessment is updated after the report
        // has been confirmed, notify it with an activity
        if ($claim->status->id >= Status::REPORT) {
            event(new ClaimWasUpdated($claim, ['type' => 'damage assessment', 'delete' => TRUE]));
        }

        return back()->with('messages.success', [trans('global.The damage assessment has been deleted successfully')]);
    }
}