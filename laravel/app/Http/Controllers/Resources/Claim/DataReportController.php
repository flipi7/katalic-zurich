<?php

namespace App\Http\Controllers\Resources\Claim;

use App\Models\Claim;
use App\Models\DataReport;
use App\Models\Status;
use App\Models\Activity;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Events\ClaimWasUpdated;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class DataReportController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @param $claimId
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $claimId)
    {
        // Validate
        $this->validate($request, [
            'claim.report.salvage' => ['required'],
            'claim.report.subrogation' => ['required'],
            'claim.report.agreement' => ['required'],
            'claim.report.fraud' => ['required'],
            'claim.report.insurance' => ['required'],
            'claim.report.claim_report' => ['required'],
        ]);

        // Get Claim
        $claim = Claim::findOrFail($claimId);

        // Parse Data
        $dataReportData = $request->input('claim.report');
        $dataReport = new DataReport($dataReportData);
        $claim->dataReport()->save($dataReport);

        // Create Activity
        event(new ClaimWasUpdated($claim, ['type' => 'report', 'add' => TRUE]));

        return back()->with('messages.success', [trans('global.The report has been saved successfully')]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     * @param  int $claimId
     * @param  int $dataReportId
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $claimId, $dataReportId)
    {
        // Validate
        $this->validate($request, [
            'claim.report.salvage' => ['required'],
            'claim.report.subrogation' => ['required'],
            'claim.report.agreement' => ['required'],
            'claim.report.fraud' => ['required'],
            'claim.report.insurance' => ['required'],
            'claim.report.claim_report' => ['required'],
        ]);

        // Get Claim
        $claim = Claim::findOrFail($claimId);
        $dataReport = DataReport::findOrFail($dataReportId);

        // Parse Data
        $dataReportData = $request->input('claim.report');
        $dataReport->fill($dataReportData);
        $claim->dataReport()->save($dataReport);

        // If a damage assessment is added after the report
        // has been confirmed, notify it with an activity
        if ($claim->status->id >= Status::REPORT) {
            event(new ClaimWasUpdated($claim, ['type' => 'report']));
        }

        return back()->with('messages.success', [trans('global.The report has been updated successfully')]);
    }

    /**
     * Approve data verification
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function confirm(Request $request, $claimId)
    {
        $claim = Claim::findOrFail($claimId);

        // Change Status
        $status = Status::find(Status::REPORT);
        if (!empty($claim->dataReport()->first()) && ($claim->status->id == Status::PRELIMINARY)) {
            $claim->statuses()->save($status);

            $claim->last_status = $status->id;
            $claim->save();

            // Create Activity
            $activity = new Activity([
                'activity' => 'report',
                'date' => Carbon::now(),
                'attachment' => $claim->dataReport->id,
                'claim_id' => $claim->id,
                'user_id' => Auth::user()->id]);
            $claim->activities()->save($activity);

            return back()->with('messages.success', [trans('global.The report has been confirmed')]);
        }
        else {
            return back()->with('messages.success', [trans('global.This report has not been saved. Please, check it before confirming')]);
        }
    }
}