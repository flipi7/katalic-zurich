<?php

namespace App\Http\Controllers\Resources\Claim;

use App\Events\ClaimWasUpdated;
use App\Http\Requests\Claim\AddressesStorePostRequest;
use App\Models\Address;
use App\Models\Claim;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AddressesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AddressesStorePostRequest|Request $request
     * @param $claimId
     * @return \Illuminate\Http\Response
     */
    public function store(AddressesStorePostRequest $request, $claimId)
    {
        $claim = Claim::findOrFail($claimId);

        // Store Address
        $address = new Address($request->input('claim.address'));
        $claim->address()->save($address);

        // Fire event
        // event(new ClaimWasUpdated($claim));

        return back()->with('messages.success', [trans('global.The address has been saved successfully')]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param AddressesStorePostRequest|Request $request
     * @param $claimId
     * @param $addressId
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function update(AddressesStorePostRequest $request, $claimId, $addressId)
    {
        // Get Claim
        $claim = Claim::findOrFail($claimId);

        // Get Address
        $address = Address::findOrFail($addressId);

        // Update Address
        $address->fill($request->input('claim.address'));

        // Save Address
        $claim->address()->save($address);

        event(new ClaimWasUpdated($claim, ['type' => 'address', 'value' => $address]));

        return back()->with('messages.success', [trans('global.The address has been updated successfully')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
