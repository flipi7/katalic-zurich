<?php

namespace App\Http\Controllers\Resources\Claim;

use App\Events\ClaimWasUpdated;
use App\Http\Requests\Claim\AddressesStorePostRequest;
use App\Models\Claim;
use App\Models\DataIke;
use App\Models\Role;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class DataIkeController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param AddressesStorePostRequest|Request $request
     * @param $claimId
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $claimId)
    {
        // Validate
        $this->validate($request, [
            'claim.ike.reference' => ['required'],
            'claim.ike.adjuster_id' => ['required', 'numeric', 'exists:users,id', 'exists:role_user,user_id,role_id,' . Role::AJUSTADOR],
        ]);

        $claim = Claim::findOrFail($claimId);

        // Store Ike
        $claim->dataIke()->save(new DataIke($request->input('claim.ike')));

        // Fire event
        event(new ClaimWasUpdated($claim, ['type' => 'Ike', 'add' => TRUE]));

        return back()->with('messages.success', [trans('global.The Ike data has been saved successfully')]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AddressesStorePostRequest|Request $request
     * @param $claimId
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $claimId, $ikeId)
    {
        // Validate
        $this->validate($request, [
            'claim.ike.reference' => ['required'],
            'claim.ike.adjuster_id' => ['required', 'numeric', 'exists:users,id', 'exists:role_user,user_id,role_id,' . Role::AJUSTADOR],
        ]);

        $claim = Claim::findOrFail($claimId);

        // Store Ike
        $claim->dataIke->fill($request->input('claim.ike'));
        $claim->dataIke->save();

        // Fire event
        event(new ClaimWasUpdated($claim, ['type' => 'Ike']));

        return back()->with('messages.success', [trans('global.The Ike data has been saved successfully')]);
    }
}
