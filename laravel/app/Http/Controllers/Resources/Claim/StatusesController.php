<?php

namespace App\Http\Controllers\Resources\Claim;

use App\Events\ClaimWasCanceled;
use App\Events\ClaimWasVerified;
use App\Models\Claim;
use App\Models\Status;
use App\Models\Activity;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class StatusesController extends Controller
{
    public function store(Request $request, $claimId)
    {
        $this->validate($request, [
            'status_id' => ['required', 'exists:statuses,id']
        ]);

        $claim = Claim::findOrFail($claimId);

        // Fire event
        switch ($request->input('status_id')) {
            case Status::CONTACT:
                if($claim->isDataComplete()) {

                    if ($claim->isDataComplete()) {
                        $status = Status::find(Status::VERIFICATION);
                        $claim->statuses()->save($status);

                        $claim->last_status = $status->id;
                        $claim->save();
                    }

                    $activity = new Activity(['activity' => 'verification', 'date' => Carbon::now(), 'claim_id' => $claim->id, 'user_id' => Auth::user()->id]);
                    $claim->activities()->save($activity);

                    return back()->with('messages.success', [trans('global.The claim has been verified successfully')]);
                }
                else {
                    return back()->with('messages.success', [trans('global.This claim is not complete. Please, check the data before confirming')]);
                }
                break;
            case Status::CANCELLED:
                $activity = new Activity(['activity' => 'cancelled', 'date' => Carbon::now(), 'claim_id' => $claim->id, 'user_id' => Auth::user()->id]);
                $claim->activities()->save($activity);

                event(new ClaimWasCanceled($claim));
                return back()->with('messages.success', [trans('global.The claim has been cancelled')]);
                break;
        }
    }


}
