<?php

namespace App\Http\Controllers\Resources\Claim;

use App\Events\ClaimVisitDateWasSet;
use App\Http\Requests\Claim\AddressesStorePostRequest;
use App\Models\Claim;
use App\Models\DataContact;
use App\Models\Activity;
use App\Models\Status;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class DataContactController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param AddressesStorePostRequest|Request $request
     * @param $claimId
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $claimId)
    {
        // Validate
        $this->validate($request, [
            'claim.contact.contact' => ['required', 'in:yes,no'],
            'claim.contact.visit_date' => ['required', 'date_format:d/m/Y H:i'],
            'claim.contact.note' => ['required'],
        ]);

        // Get Claim
        $claim = Claim::findOrFail($claimId);

        // Parse Date
        $inputContact = $request->input('claim.contact');

        $date = Carbon::createFromFormat('d/m/Y H:i', $inputContact['visit_date']);
        $inputContact = array_merge($inputContact, ['visit_date' => $date]);

        // Store Ike
        $dataContact = new DataContact($inputContact);
        $claim->dataContact()->save($dataContact);

        // Create Activity
        $activity = new Activity([
            'activity' => 'contact',
            'date' => Carbon::now(),
            'attachment' => $dataContact->id,
            'claim_id' => $claim->id,
            'user_id' => Auth::user()->id]);
        $claim->activities()->save($activity);

        // Fire event
        if ($inputContact['contact'] == 'yes') {
            if ($claim->status->id == Status::VERIFICATION) {
                $status = Status::find(Status::CONTACT);
                $claim->statuses()->save($status);

                $claim->last_status = $status->id;
                $claim->save();
            }
        }

        return back()->with('messages.success', [trans('global.The contact data has been saved successfully')]);
    }
}
