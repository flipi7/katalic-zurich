<?php

namespace App\Http\Controllers\Resources\Claim;

use App\Events\ClaimWasVisited;
use App\Models\Claim;
use App\Models\DataVisit;
use App\Models\Status;
use App\Models\Activity;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Symfony\Component\VarDumper\Cloner\Data;

class DataVisitsController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @param $claimId
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $claimId)
    {
        // Validate
        $this->validate($request, [
            'claim.visit.date' => ['required', 'date_format:d/m/Y H:i'],
            'claim.visit.description' => ['required'],
        ]);

        // Get Claim
        $claim = Claim::findOrFail($claimId);

        // Parse Data
        $dataVisitData = $request->input('claim.visit');
        $date = Carbon::createFromFormat('d/m/Y H:i', $dataVisitData['date']);
        $userId = Auth::user()->id;
        $dataVisitData = array_merge($dataVisitData, ['claim_id' => $claimId, 'user_id' => $userId, 'date' => $date]);
        $dataVisit = new DataVisit($dataVisitData);
        $claim->dataVisits()->save($dataVisit);

        // Create Activity
        $activity = new Activity([
            'activity' => 'visit',
            'date' => Carbon::now(),
            'attachment' => $dataVisit->id,
            'claim_id' => $claim->id,
            'user_id' => Auth::user()->id]);
        $claim->activities()->save($activity);

        if ($claim->status->id == Status::CONTACT || $claim->status->id == Status::VERIFICATION) {
            $status = Status::find(Status::VISIT);
            $claim->statuses()->save($status);

            $claim->last_status = $status->id;
            $claim->save();
        }

        return back()->with('messages.success', [trans('global.The visit has been created successfully')]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     * @param  int $claimId
     * @param  int $dataVisitId
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $claimId, $dataVisitId)
    {
        // Validate
        $this->validate($request, [
            'claim.visit.date' => ['required', 'date_format:d/m/Y H:i'],
            'claim.description' => ['required'],
        ]);

        // Get Claim
        $claim = Claim::findOrFail($claimId);

        // Get DataVisit
        $dataVisit = DataVisit::findOrFail($dataVisitId);

        // Parse Data
        $dataVisitData = $request->input('claim.visit');
        $date = Carbon::createFromFormat('d/m/Y H:i', $dataVisitData['date']);
        $dataVisitData = array_merge($dataVisitData, ['date' => $date]);

        $dataVisit->fill($dataVisitData);

        // Store DataVisit
        $claim->dataVisits()->save($dataVisit);

        event(new ClaimWasUpdated($claim, ['type' => 'update']));

        return back()->with('messages.success', [trans('global.The visit has been updated successfully')]);
    }
}