<?php

namespace App\Http\Controllers\Resources\Claim;

use App\Models\Claim;
use App\Models\DataPreliminary;
use App\Models\Status;
use App\Models\Activity;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Events\ClaimWasUpdated;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class DataPreliminaryController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @param $claimId
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $claimId)
    {
        // Validate
        $this->validate($request, [
            'claim.preliminary.visit_date' => ['required', 'date_format:d/m/Y H:i'],
            'claim.preliminary.claim_date' => ['required', 'date_format:d/m/Y H:i'],
            'claim.preliminary.validity_start' => ['required', 'date_format:d/m/Y'],
            'claim.preliminary.validity_end' => ['required', 'date_format:d/m/Y'],
            'claim.preliminary.third_party' => ['required'],
            'claim.preliminary.cover' => ['required'],
            'claim.preliminary.description' => ['required'],
            'claim.preliminary.amount_loss' => ['required'],
        ]);

        // Get Claim
        $claim = Claim::findOrFail($claimId);

        if ($claim->status->id == Status::VISIT || $claim->status->id == Status::VERIFICATION) {
            // Parse Data
            $dataPreliminaryData = $request->input('claim.preliminary');

            // Dates
            $visit_date = Carbon::createFromFormat('d/m/Y H:i', $dataPreliminaryData['visit_date'])->format('Y-m-d H:i:s');
            $claim_date = Carbon::createFromFormat('d/m/Y H:i', $dataPreliminaryData['claim_date'])->format('Y-m-d H:i:s');
            $validity_start = Carbon::createFromFormat('d/m/Y', $dataPreliminaryData['validity_start'])->format('Y-m-d H:i:s');
            $validity_end = Carbon::createFromFormat('d/m/Y', $dataPreliminaryData['validity_end'])->format('Y-m-d H:i:s');

            $dataPreliminaryData = array_merge($dataPreliminaryData,
                ['claim_id' => $claimId,
                 'visit_date' => $visit_date,
                 'claim_date' => $claim_date,
                 'validity_start' => $validity_start,
                 'validity_end' => $validity_end]);
            $dataPreliminary = new DataPreliminary($dataPreliminaryData);
            $claim->dataPreliminary()->save($dataPreliminary);

            // Create Activity
            $activity = new Activity([
                'activity' => 'preliminary',
                'date' => Carbon::now(),
                'attachment' => $dataPreliminary->id,
                'claim_id' => $claim->id,
                'user_id' => Auth::user()->id]);
            $claim->activities()->save($activity);

            // Change status
            $status = Status::find(Status::PRELIMINARY);
            $claim->statuses()->save($status);

            $claim->last_status = $status->id;
            $claim->save();
        }

        return back()->with('messages.success', [trans('global.The preliminary report has been created successfully')]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     * @param  int $claimId
     * @param  int $dataPreliminaryId
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $claimId, $dataPreliminaryId)
    {
        // Validate
        $this->validate($request, [
            'claim.preliminary.visit_date' => ['required', 'date_format:d/m/Y H:i'],
            'claim.preliminary.claim_date' => ['required', 'date_format:d/m/Y H:i'],
            'claim.preliminary.validity_start' => ['required', 'date_format:d/m/Y'],
            'claim.preliminary.validity_end' => ['required', 'date_format:d/m/Y'],
            'claim.preliminary.third_party' => ['required'],
            'claim.preliminary.cover' => ['required'],
            'claim.preliminary.description' => ['required'],
            'claim.preliminary.amount_loss' => ['required'],
        ]);

        // Get Claim
        $claim = Claim::findOrFail($claimId);

        // Parse Data
        $dataPreliminaryData = $request->input('claim.preliminary');

        // Dates
        $visit_date = Carbon::createFromFormat('d/m/Y H:i', $dataPreliminaryData['visit_date'])->format('Y-m-d H:i:s');
        $claim_date = Carbon::createFromFormat('d/m/Y H:i', $dataPreliminaryData['claim_date'])->format('Y-m-d H:i:s');
        $validity_start = Carbon::createFromFormat('d/m/Y', $dataPreliminaryData['validity_start'])->format('Y-m-d H:i:s');
        $validity_end = Carbon::createFromFormat('d/m/Y', $dataPreliminaryData['validity_end'])->format('Y-m-d H:i:s');

        $dataPreliminaryData = array_merge($dataPreliminaryData,
            ['claim_id' => $claimId,
                'visit_date' => $visit_date,
                'claim_date' => $claim_date,
                'validity_start' => $validity_start,
                'validity_end' => $validity_end]);

        // Get DataPreliminary
        $dataPreliminary = DataPreliminary::findOrFail($dataPreliminaryId);
        $dataPreliminary->fill($dataPreliminaryData);

        // Save DataPreliminary
        $claim->dataPreliminary()->save($dataPreliminary);

        event(new ClaimWasUpdated($claim, ['type' => 'preliminary']));

        return back()->with('messages.success', [trans('global.The preliminary report has been updated successfully')]);
    }
}