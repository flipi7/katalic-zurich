<?php

namespace App\Http\Controllers\Resources\Claim;

use App\Events\ClaimWasUpdated;
use App\Http\Requests\Claim\AddressesStorePostRequest;
use App\Models\Claim;
use App\Models\DataIke;
use App\Models\DataVerification;
use App\Models\Role;
use App\Models\Status;
use App\Models\Activity;
use App\Events\ClaimWasVerified;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class DataVerificationController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param AddressesStorePostRequest|Request $request
     * @param $claimId
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $claimId)
    {
        // Validate
        $this->validate($request, [
            'claim.data_verification.policy_payment' => ['required'],
            'claim.data_verification.validity_start' => ['required', 'date'],
            'claim.data_verification.validity_end' => ['required', 'date'],
            'claim.data_verification.siu' => ['required'],
            'claim.data_verification.coinsurance' => ['required'],
            'claim.data_verification.coinsurance_company' => ['required_if:claim.data_verification.coinsurance,yes'],
            'claim.data_verification.coinsurance_percentage' => ['required_if:claim.data_verification.coinsurance,yes'],
            'claim.data_verification.complaint' => ['required'],
            'claim.data_verification.analyst_id' => ['required', 'numeric', 'exists:users,id', 'exists:role_user,user_id,role_id,' . Role::ANALISTA],
            'claim.data_verification.zurich_claim_number' => ['required', 'numeric'],
        ]);

        $claim = Claim::findOrFail($claimId);

        $isCoverCorrect = $request->input('claim.data_verification.is_cover_correct');
        if($isCoverCorrect == 'no') {
            // Delete previous survey
            \DB::table('claim_survey')->where('claim_id', $claim->id)->delete();

            // Create Survey
            $survey = (int)$request->input('claim.data_verification.survey');
            $claim->surveys()->attach($survey);

            $isCoverCorrect = 'yes';
        }

        // Store Ike
        $claim->dataVerification()->save(new DataVerification($request->input('claim.data_verification')));

        // Store Zurich Claim Number and is_cover_correct
        $claim->fill([
            'is_cover_correct' => $isCoverCorrect,
            'zurich_claim_number' => $request->input('claim.data_verification.zurich_claim_number')
        ]);
        $claim->save();

        // Fire event
        event(new ClaimWasUpdated($claim, ['type' => 'data verification', 'add' => TRUE]));

        return back()->with('messages.success', [trans('global.The verification data has been saved successfully')]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AddressesStorePostRequest|Request $request
     * @param $claimId
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $claimId)
    {
        // Validate
        $this->validate($request, [
            'claim.data_verification.policy_payment' => ['required'],
            'claim.data_verification.validity_start' => ['required', 'date'],
            'claim.data_verification.validity_end' => ['required', 'date'],
            'claim.data_verification.siu' => ['required'],
            'claim.data_verification.coinsurance' => ['required'],
            'claim.data_verification.coinsurance_company' => ['required_if:claim.data_verification.coinsurance,yes'],
            'claim.data_verification.coinsurance_percentage' => ['required_if:claim.data_verification.coinsurance,yes'],
            'claim.data_verification.complaint' => ['required'],
            'claim.data_verification.analyst_id' => ['required', 'numeric', 'exists:users,id', 'exists:role_user,user_id,role_id,' . Role::ANALISTA],
        ]);

        $claim = Claim::findOrFail($claimId);

        // Get Input
        $inputClaim = $request->input('claim.data_verification');

        // Save Verification
        $claim->dataVerification->fill($inputClaim);
        $claim->dataVerification->save();

        // Store Zurich Claim Number and is_cover_correct
        $claim->fill([
            'is_cover_correct' => $request->input('claim.data_verification.is_cover_correct'),
            'zurich_claim_number' => $request->input('claim.data_verification.zurich_claim_number')
        ]);
        $claim->save();

        // Fire event
        event(new ClaimWasUpdated($claim, ['type' => 'data verification']));

        return back()->with('messages.success', [trans('global.The verification data has been saved successfully')]);
    }

    /**
     * Approve data verification
     *
     * @param AddressesStorePostRequest|Request $request
     * @return \Illuminate\Http\Response
     */
    public function approve(Request $request, $claimId)
    {
        $claim = Claim::findOrFail($claimId);

        // Change Status
        $status = Status::find(Status::VERIFICATION);
        if ($claim->isDataComplete()) {
            $claim->statuses()->save($status);

            // Create Activity
            $activity = new Activity(['activity' => 'verification', 'date' => Carbon::now(), 'claim_id' => $claim->id, 'user_id' => Auth::user()->id]);
            $claim->activities()->save($activity);

            return back()->with('messages.success', [trans('global.The verification data has been approved')]);
        }
        else {
            return back()->with('messages.success', [trans('global.This claim is not complete. Please, check the data before confirming')]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AddressesStorePostRequest|Request $request
     * @return \Illuminate\Http\Response
     */
    public function cancel(Request $request, $claimId)
    {
        $claim = Claim::findOrFail($claimId);

        // Change Status
        $status = Status::find(Status::CANCELLED);
        $claim->statuses()->save($status);

        $claim->last_status = $status;
        $claim->save();

        // Create Activity
        $activity = new Activity(['activity' => 'cancelled', 'date' => Carbon::now(), 'claim_id' => $claim->id, 'user_id' => Auth::user()->id]);
        $claim->activities()->save($activity);

        return back()->with('messages.success', [trans('global.The claim has been cancelled')]);
    }
}
