<?php

namespace App\Http\Controllers\Resources\Claim;

use App\Http\Requests\Claim\DocumentsStorePostRequest;
use App\Http\Requests\Claim\DocumentsUpdatePostRequest;
use App\Models\Claim;
use App\Models\Document;
use App\Models\Activity;
use Illuminate\Http\Request;
use Illuminate\Routing\UrlGenerator;
use Illuminate\Routing\RouteCollection;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Resources\Claim\NotesController;
use Illuminate\Support\Facades\Redirect;
use File;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class DocumentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @param $claimId
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $claimId)
    {
        $this->validate($request, [
            'description' => ['required'],
            'attachment' => 'required|max:2048',
        ]);

        // Get Claim
        $claim = Claim::findOrFail($claimId);

        // Parse Data
        $documentData = $request->input();
        $date = Carbon::now();
        if(Auth::check()) {
            $userId = Auth::user()->id;
        }
        else {
            $userId = null;
        }

        $file = $request->file('attachment');

        $filehash = $claimId . '-' . NotesController::random_name();
        $filename = $file->getClientOriginalName();
        $file->move(public_path('static'), $filehash . '-' . $filename);
        $documentData = array_merge($documentData, ['claim_id' => $claimId, 'user_id' => $userId, 'date' => $date, 'attachmentHash' => $filehash, 'attachmentName' => $filename]);

        $document = new Document($documentData);
        $claim->documents()->save($document);

        // Create Activity
        $activity = new Activity(['activity' => 'document', 'date' => Carbon::now(), 'claim_id' => $claim->id, 'user_id' => $userId, 'attachment' => $document->id]);
        $claim->activities()->save($activity);
         
        $urlGenerator = new UrlGenerator(new RouteCollection(),$request);

        $url = $urlGenerator->previous() . '#tab-docs';
        return Redirect::to($url)->with('messages.success', [trans('global.The document has been created successfully')]);; // domain.com/welcome#hash

        //return back()->with('messages.success', [trans('global.The document has been created successfully')]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $claimId
     * @param  int $documentId
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $claimId, $documentId)
    {
        $this->validate($request, [
            'description' => ['required'],
            'attachment' => ['required'],
        ]);

        // Get Claim
        $claim = Claim::findOrFail($claimId);

        // Get Document
        $document = Document::findOrFail($documentId);

        $documentData = $request->input();

        // Get File
        $file = $request->file('attachment');

        // If there is a file, it has to be a new one, so we save it.
        if ($file != null) {
            $filehash = $claimId . '-' . $this->random_name();
            $filename = $file->getClientOriginalName();
            $file->move(public_path('static'), $filehash . '-' . $filename);
            $documentData = array_merge($documentData, ['attachmentHash' => $filehash, 'attachmentName' => $filename]);
        }
        // Otherwise, if the 'attachment' flag is present and empty,
        // it means that the current attachment must be removed.
        else {
            if (array_key_exists('attachment', $documentData)) {
                if ($documentData['attachment'] == '') {
                    $filepath = public_path('static') . '/' . $document->attachmentHash . '-' . $document->attachmentName;
                    if(File::exists($filepath)) {
                        File::delete($filepath);
                    }
                    $documentData = array_merge($documentData, ['attachmentHash' => '', 'attachmentName' => '']);
                }
            }
        }

        $document->fill($documentData);

        // Store Document
        $claim->documents()->save($document);

        return back()->with('messages.success', [trans('global.The document has been updated successfully')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
