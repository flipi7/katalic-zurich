<?php

namespace App\Http\Controllers\Resources\Claim;

use App\Events\ClaimWasUpdated;
use App\Http\Requests\Claim\SurveysStorePostRequest;
use App\Models\Claim;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class SurveysController extends Controller
{
    public function store(SurveysStorePostRequest $request, $claimId)
    {
        $claim = Claim::findOrFail($claimId);

        // Update Survey
        $syncArray = [];
        foreach ($request->input('claim.surveys') as $surveyId => $questions) {

            foreach ($questions as $questionId => $answer) {
                $syncArray[$questionId] = ['answer' => $answer];
                $claim->surveyAnswers()->sync($syncArray);
            }
        }

        // Fire event
        // event(new ClaimWasUpdated($claim));

        return back()->with('messages.success', [trans('global.The survey has been saved successfully')]);
    }
}
