<?php

namespace App\Http\Controllers\Resources\Claim;

use App\Http\Requests\Claim\NotesStorePostRequest;
use App\Http\Requests\Claim\NotesUpdatePostRequest;
use App\Models\Claim;
use App\Models\Note;
use App\Models\Status;
use App\Models\NoteVisibility;
use App\Models\Role;
use App\Models\Activity;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

use File;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class NotesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param NotesStorePostRequest|Request $request
     * @param $noteId
     * @return \Illuminate\Http\Response
     */
    public function store(NotesStorePostRequest $request, $claimId)
    {
        // Get Claim
        $claim = Claim::findOrFail($claimId);

        // Parse Data
        $noteData = $request->input();
        $date = Carbon::now();
        if(Auth::check()) {
            $userId = Auth::user()->id;
        }
        else {
            $userId = null;
        }

        $file = $request->file('attachment');

        if ($file) {
            $filehash = $claimId . '-' . $this->random_name();
            $filename = $file->getClientOriginalName();
            $file->move(public_path('static'), $filehash . '-' . $filename);
            $noteData = array_merge($noteData, ['claim_id' => $claimId, 'user_id' => $userId, 'date' => $date, 'attachmentHash' => $filehash, 'attachmentName' => $filename]);
        }
        else {
            $noteData = array_merge($noteData, ['claim_id' => $claimId, 'user_id' => $userId, 'date' => $date]);
        }

        $note = new Note($noteData);
        $claim->notes()->save($note);

        // Visibilites
        if (isset($visibilities['visibility'])) {
            $visibilities = $noteData['visibility'];
            if (isset($visibilities[0])) {
                $noteVisibility = new NoteVisibility(['note_id' => $note->id, 'role_id' => null]);
                $note->visibilities()->save($noteVisibility);
            }
            foreach (Role::getRoles() as $role) {
                if (isset($visibilities[$role->id])) {
                    $noteVisibility = new NoteVisibility(['note_id' => $note->id, 'role_id' => $role->id]);
                    $note->visibilities()->save($noteVisibility);
                }
            }
        }
        else {
            foreach (Role::getRoles() as $role) {
                $noteVisibility = new NoteVisibility(['note_id' => $note->id, 'role_id' => $role->id]);
                $note->visibilities()->save($noteVisibility);
            }
            $noteVisibility = new NoteVisibility(['note_id' => $note->id, 'role_id' => null]);
            $note->visibilities()->save($noteVisibility);
        }

        // Create Activity
        $activity = new Activity(['activity' => 'note', 'date' => Carbon::now(), 'claim_id' => $claim->id, 'user_id' => $userId, 'attachment' => $note->id]);
        $claim->activities()->save($activity);

        return back()->with('messages.success', [trans('global.The note has been created successfully')]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(NotesUpdatePostRequest $request, $claimId, $noteId)
    {
        // Get Claim
        $claim = Claim::findOrFail($claimId);

        // Get Note
        $note = Note::findOrFail($noteId);

        $noteData = $request->input();

        // Get File
        $file = $request->file('attachment');

        // If there is a file, it has to be a new one, so we save it.
        if ($file != null) {
            $filehash = $claimId . '-' . NotesController::random_name();
            $filename = $file->getClientOriginalName();
            $file->move(public_path('static'), $filehash . '-' . $filename);
            $noteData = array_merge($noteData, ['attachmentHash' => $filehash, 'attachmentName' => $filename]);
        }
        // Otherwise, if the 'attachment' flag is present and empty,
        // it means that the current attachment must be removed.
        else {
            if (array_key_exists('attachment', $noteData)) {
                if ($noteData['attachment'] == '') {
                    $filepath = public_path('static') . '/' . $note->attachmentHash . '-' . $note->attachmentName;
                    if(File::exists($filepath)) {
                        File::delete($filepath);
                    }
                    $noteData = array_merge($noteData, ['attachmentHash' => '', 'attachmentName' => '']);
                }
            }
        }

        // Update visibilites
        NoteVisibility::destroy(array_column($note->visibilities()->getResults()->toArray(), 'id'));
        
        if (isset($visibilities['visibility'])) {
            $visibilities = $noteData['visibility'];
            if (isset($visibilities[0])) {
                $noteVisibility = new NoteVisibility(['note_id' => $note->id, 'role_id' => null]);
                $note->visibilities()->save($noteVisibility);
            }
            foreach (Role::getRoles() as $role) {
                if (isset($visibilities[$role->id])) {
                    $noteVisibility = new NoteVisibility(['note_id' => $note->id, 'role_id' => $role->id]);
                    $note->visibilities()->save($noteVisibility);
                }
            }
        }

        $note->fill($noteData);

        // Store Note
        $claim->notes()->save($note);

        return back()->with('messages.success', [trans('global.The note has been updated successfully')]);
    }

    /**
     * Remove the specified resource in storage.
     *
     * @param  Request $request
     * @param  int $claimId
     * @param  int $noteId
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $claimId, $noteId)
    {
        // Get Claim
        $claim = Claim::findOrFail($claimId);
        $note = Note::findOrFail($noteId);
        $note->delete();

        // If a damage assessment is updated after the report
        // has been confirmed, notify it with an activity
        if ($claim->status->id >= Status::REPORT) {
            event(new ClaimWasUpdated($claim, ['type' => 'note', 'delete' => TRUE]));
        }

        return back()->with('messages.success', [trans('global.The note has been deleted successfully')]);
    }

    public static function random_name() {
        $key = '';
        $keys = array_merge(range(0, 9), range('a', 'z'));

        for ($i = 0; $i < 20; $i++) {
            $key .= $keys[array_rand($keys)];
        }

        return $key;
    }
}
