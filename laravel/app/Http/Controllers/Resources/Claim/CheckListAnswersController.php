<?php

namespace App\Http\Controllers\Resources\Claim;

use App\Models\CheckListAnswer;
use App\Models\Claim;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class CheckListAnswersController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param AddressesStorePostRequest|Request $request
     * @param $claimId
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $claimId)
    {
        // Get Claim
        $claim = Claim::findOrFail($claimId);

        // Parse Date
        $answers = $request->input('claim.check_list');

        if (count($answers) == \App\Models\CheckListQuestion::all()->count()) {
            \DB::table('check_list_answers')->where('claim_id', $claim->id)->delete();
            foreach ($answers as $questionId => $answer) {
                $a = new CheckListAnswer(['claim_id' => $claim->id, 'question_id' => $questionId, 'answer' => $answer]);
                $claim->checkListAnswers()->save($a);
            }
            return back()->with('messages.success', [trans('global.The survey has been saved successfully')]);
        }
        else {
            return back()->with('messages.success', [trans('global.Missing data')]);
        }
    }
}
