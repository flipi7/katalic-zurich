<?php

namespace App\Http\Controllers\Resources\Claim;

use App\Events\ClaimWasUpdated;
use App\Http\Requests\Claim\ContactsStorePostRequest;
use App\Http\Requests\Claim\ContactsUpdatePostRequest;
use App\Models\Claim;
use App\Models\Contact;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ContactsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @param $claimId
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $claimId)
    {
        $claim = Claim::findOrFail($claimId);

        // Store Contact
        $contact = new Contact($request->input('claim.contact'));
        $claim->contacts()->save($contact);

        // Fire event
        event(new ClaimWasUpdated($claim, ['type' => 'contact', 'value' => $contact, 'add' => TRUE]));

        return back()->with('messages.success', [trans('global.The contact has been created successfully')]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $claimId
     * @param  int $contactId
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $claimId, $contactId)
    {
        // Get Claim
        $claim = Claim::findOrFail($claimId);

        // Get Contact
        $contact = Contact::findOrFail($contactId);
        $contact->fill($request->input('claim.contact'));

        // Store Contact
        $claim->contacts()->save($contact);

        // Fire event
        event(new ClaimWasUpdated($claim, ['type' => 'contact', 'value' => $contact]));

        return back()->with('messages.success', [trans('global.The contact has been updated successfully')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
