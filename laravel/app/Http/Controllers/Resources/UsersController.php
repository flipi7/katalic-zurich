<?php

namespace App\Http\Controllers\Resources;

use App\Http\Controllers\AuthenticatedController;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;

use App\Http\Requests;

class UsersController extends AuthenticatedController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $itemsPerPage = 10;

        // Get Users
        if ($request->has('role_id')) {
            $role = Role::findOrFail($request->input('role_id'));
            $users = $role->users()->paginate($itemsPerPage);
        } else {
            $users = User::paginate($itemsPerPage);
        }

        // Get Roles
        $roles = Role::all();

        return view('resources.users.index', [
            'users' => $users,
            'roles' => $roles,
            'role_id' => $request->input('role_id', null),
            'totalUsers' => User::count()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Get Roles
        $roles = Role::all()->except(Role::SUPER_ADMIN);

        return view('resources.users.create', ['roles' => $roles]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required|min:6',
            'role_id' => 'required|numeric|exists:roles,id'
        ]);

        // Get Input
        $input = $request->input();
        $input['password'] = bcrypt($input['password']);

        // Save User
        $user = User::create($input);

        // Create Role
        $role = Role::findOrFail($input['role_id']);
        $user->roles()->save($role, ['created_by' => $this->authUser->id]);

        return redirect()->route('users.index')->with('messages.success', [trans('global.The user has been created successfully')]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // Get User
        $user = User::findOrFail($id);

        // Get Roles
        $roles = Role::all()->except(Role::SUPER_ADMIN);

        return view('resources.users.edit', ['user' => $user, 'roles' => $roles]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'min:6',
            'role_id' => 'required|numeric|exists:roles,id'
        ]);

        // Get Input
        $input = $request->input();
        $input['password'] = bcrypt($input['password']);

        // Save User
        $user = User::findOrFail($id);
        $user->fill($input);
        $user->save();

        // Save Role
        $role = Role::findOrFail($input['role_id']);
        $user->roles()->sync([
            $role->id => ['created_by' => $this->authUser->id]
        ]);

        return redirect()->route('users.index')->with('messages.success', [trans('global.The data has been saved successfully')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Get User
        $user = User::findOrFail($id);

        // Delete User
        $user->delete();

        return redirect()->route('users.index')->with('messages.success', [trans('global.The user has been deleted successfully')]);
    }
}
