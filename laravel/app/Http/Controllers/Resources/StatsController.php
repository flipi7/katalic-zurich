<?php

namespace App\Http\Controllers\Resources;

use App\Http\Controllers\AuthenticatedController;
use App\Models\SurveyTemplate;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Repositories\Models\ClaimRepository;
use Carbon\Carbon;
use App\Models\StatsSerializer;
use App\Models\Claim;

class StatsController extends AuthenticatedController
{
    /**
     * @var ClaimRepository
     */
    protected $claimRepository;

    public function __construct(ClaimRepository $claimRepository){
        $this->claimRepository = $claimRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('resources.stats.index');
    }

    public function users(Request $request)
    {
        return view('resources.stats.users');
    }

    public function external(Request $request)
    {
        return view('resources.stats.external');
    }

    public function getClaims(Request $request)
    {

    }

    public function ClaimsByCities(Request $request)
    {
        $dates = $request->input('dates');
        $startDate = substr($dates, 0, 10);
        $endDate   = substr($dates, 13, 10);

        $start = Carbon::createFromFormat('d/m/Y', $startDate);
        $end   = Carbon::createFromFormat('d/m/Y', $endDate);

        $data = $this->claimRepository->findByDateRange($start, $end, ['id']);

        $stateCounts = [];

        $addresses = \DB::table('addresses')
            ->select('*')
            ->whereIn('addressable_id', $data)
            ->get();

        foreach ($addresses as $address) {
            if (isset($stateCounts[$address->state_id])) {
                if (isset($stateCounts[$address->state_id][$address->city_id])) {
                    $stateCounts[$address->state_id][$address->city_id]['claims'] += 1;
                } else {
                    $stateCounts[$address->state_id][$address->city_id] = [
                            'claims' => 1,
                            'city_id' => $address->city_id,
                            'state_id' => $address->state_id
                        ];
                }
            }
            else {
                $stateCounts[$address->state_id] = [
                    $address->city_id => [
                        'claims' => 1,
                        'city_id' => $address->city_id,
                        'state_id' => $address->state_id
                    ]
                ];
            }
        }

        $result = [];

        foreach ($stateCounts as $key => $value) {
            $stateName = \DB::table('states')->select('*')->where('id', $key)->get()[0]->name;
            foreach ($value as $cityKey => $cityValue) {
                if ($cityKey == '') {
                    $cityName = '-';
                }
                else {
                    $cityName = \DB::table('cities')->select('*')->where('id', $cityKey)->get()[0]->name;
                }
                $result[] = [
                    'city_name' => $cityName,
                    'state_name' => $stateName,
                    'claims' => $cityValue['claims']
                ];

            }
        }

        // Sort by claims
        usort($result, function ($a, $b) {
            return $b['claims'] - $a['claims'];
        });

        return [ 'data' => $result];
    }

    public function claimsStats(Request $request)
    {
        $dates = $request->input('dates');
        $startDate = substr($dates, 0, 10);
        $endDate   = substr($dates, 13, 10);

        $start = Carbon::createFromFormat('d/m/Y', $startDate);
        $end   = Carbon::createFromFormat('d/m/Y', $endDate);

        // $data = $this->claimRepository->findByDate($start);

        $data = StatsSerializer::getClaimsStatsData($this->claimRepository, $start, $end);

        return $data;
    }
}
