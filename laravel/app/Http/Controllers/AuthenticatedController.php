<?php

namespace App\Http\Controllers;

abstract class AuthenticatedController extends Controller
{
    /**
     * @var \App\Models\User|null
     */
    protected $authUser;

    public function __construct()
    {
        $this->middleware('auth');
        $this->authUser = auth()->user();
        view()->share('authUser', $this->authUser);
    }
}
