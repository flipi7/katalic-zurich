<?php

namespace App\Http\ViewComposers;

use App\Models\Status;
use Illuminate\Contracts\View\View;

class ClaimStatusesComposer
{
    public function compose(View $view)
    {
        // Get Claim Statuses
        $claimStatuses = Status::all();

        $view->with('claimStatuses', $claimStatuses);
    }
}