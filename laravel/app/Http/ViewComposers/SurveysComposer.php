<?php

namespace App\Http\ViewComposers;

use App\Models\Survey;
use App\Models\SurveyTemplate;
use Illuminate\Contracts\View\View;

class SurveysComposer
{
    public function compose(View $view)
    {
        // Get Survey Templates
        $surveys = Survey::all();
        $view->with('surveys', $surveys);
    }
}