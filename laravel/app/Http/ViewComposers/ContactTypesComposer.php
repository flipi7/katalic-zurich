<?php

namespace App\Http\ViewComposers;

use App\Models\Contact;
use Illuminate\Contracts\View\View;

class ContactTypesComposer
{
    public function compose(View $view)
    {
        // Get Contact Types
        $contactTypes = Contact::$types;

        $view->with('contactTypes', $contactTypes);
    }
}