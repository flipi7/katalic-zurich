<?php

namespace App\Http\ViewComposers;

use App\Models\Country;
use Illuminate\Contracts\View\View;

class CountriesComposer
{
    public function compose(View $view)
    {
        // Get Countries
        $countries = Country::all();

        $view->with('countries', $countries);
    }
}