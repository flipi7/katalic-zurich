<?php

namespace App\Http\ViewComposers;

use App\Models\Role;
use Illuminate\Contracts\View\View;

class AnalystsComposer
{
    public function compose(View $view)
    {
        // Get Role
        $role = Role::findOrFail(Role::ANALISTA);

        // Get Analysts
        $analysts= $role->users;

        $view->with('analysts', $analysts);
    }
}