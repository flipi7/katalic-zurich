<?php

namespace App\Http\ViewComposers;

use App\Models\Role;
use Illuminate\Contracts\View\View;

class AdjustersComposer
{
    public function compose(View $view)
    {
        // Get Role
        $role = Role::findOrFail(Role::AJUSTADOR);

        // Get Adjusters
        $adjusters = $role->users;

        $view->with('adjusters', $adjusters);
    }
}