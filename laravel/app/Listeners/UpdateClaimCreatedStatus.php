<?php

namespace App\Listeners;

use App\Events\ClaimWasCreated;
use App\Models\Status;

class UpdateClaimCreatedStatus
{
    public function handle(ClaimWasCreated $event)
    {
        // Get Claim
        $claim = $event->claim;

        // Get Status
        $status = Status::find(Status::CREATED);

        $claim->last_status = $status->id;
        $claim->save();

        // Save Claim Status
        $claim->statuses()->save($status);
    }

}