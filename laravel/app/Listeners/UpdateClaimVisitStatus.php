<?php

namespace App\Listeners;

use App\Events\ClaimWasVisited;
use App\Models\Status;

class UpdateClaimVisitStatus
{
    public function handle(ClaimWasVisited $event)
    {
        // Get Claim
        $claim = $event->claim;

        // Get Status
        $status = Status::find(Status::VISIT);

        if ($claim->status->id === Status::CONTACT || $claim->status->id === Status::VERIFICATION) {
            $claim->statuses()->save($status);
        }
    }
}
