<?php

namespace App\Listeners;

use App\Events\ClaimWasVerified;
use App\Models\Status;

class UpdateClaimVerificationStatus
{
    public function handle(ClaimWasVerified $event)
    {
        // Get Claim
        $claim = $event->claim;

        // Get Status
        $status = Status::find(Status::VERIFICATION);
        if ($claim->isDataComplete() && $claim->status->id === Status::CREATED) {
            $claim->statuses()->save($status);
        }
    }

}