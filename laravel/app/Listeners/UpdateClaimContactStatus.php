<?php

namespace App\Listeners;

use App\Events\ClaimVisitDateWasSet;
use App\Models\Status;

class UpdateClaimContactStatus
{
    public function handle(ClaimVisitDateWasSet $event)
    {
        // Get Claim
        $claim = $event->claim;

        // Get Status
        $status = Status::find(Status::CONTACT);
        if ($claim->status->id === Status::VERIFICATION) {
            $claim->statuses()->save($status);
        }
    }
}
