<?php

namespace App\Listeners;

use App\Events\ClaimWasUpdated;
use App\Models\Activity;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class UpdateClaim
{
    public function handle(ClaimWasUpdated $event)
    {
        // Get Claim
        $claim = $event->claim;
        $properties = $event->properties;

        if (isset($properties['add'])) {
            $activity = new Activity(['activity' => 'add', 'attachment' => $properties['type'], 'date' => Carbon::now(), 'claim_id' => $claim->id, 'user_id' => Auth::user()->id]);
        }
        elseif (isset($properties['delete'])){
            $activity = new Activity(['activity' => 'delete', 'attachment' => $properties['type'], 'date' => Carbon::now(), 'claim_id' => $claim->id, 'user_id' => Auth::user()->id]);
        }
        else {
            $activity = new Activity(['activity' => 'update', 'attachment' => $properties['type'], 'date' => Carbon::now(), 'claim_id' => $claim->id, 'user_id' => Auth::user()->id]);
        }
        $claim->activities()->save($activity);
    }

}