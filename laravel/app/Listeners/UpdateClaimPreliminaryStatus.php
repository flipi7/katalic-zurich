<?php

namespace App\Listeners;

use App\Events\ClaimVisitDateWasSet;
use App\Models\Status;

class UpdateClaimPreliminaryStatus
{
    public function handle(ClaimVisited $event)
    {
        // Get Claim
        $claim = $event->claim;

        // Get Status
        $status = Status::find(Status::PRELIMINARY);

        if ($claim->status->id === Status::CONTACT) {
            $claim->statuses()->save($status);
        }
    }
}
