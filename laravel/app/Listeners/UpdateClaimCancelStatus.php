<?php

namespace App\Listeners;

use App\Events\ClaimWasCanceled;
use App\Models\Status;

class UpdateClaimCancelStatus
{
    public function handle(ClaimWasCanceled $event)
    {
        // Get Claim
        $claim = $event->claim;

        // Get Status
        $status = Status::find(Status::CANCELLED);
        if ($claim->status->id !== Status::CANCELLED) {
            $claim->statuses()->save($status);

            $claim->last_status = $status->id;
            $claim->save();
        }
    }
}
