<?php

namespace App\Providers;

use App\Events\ClaimVisitDateWasSet;
use App\Events\ClaimWasCanceled;
use App\Events\ClaimWasCreated;
use App\Events\ClaimWasUpdated;
use App\Events\ClaimWasVerified;
use App\Listeners\UpdateClaim;
use App\Listeners\UpdateClaimCancelStatus;
use App\Listeners\UpdateClaimContactStatus;
use App\Listeners\UpdateClaimCreatedStatus;
use App\Listeners\UpdateClaimVerificationStatus;
use App\Listeners\UpdateClaimVisitStatus;
use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        ClaimWasCreated::class => [
            UpdateClaimCreatedStatus::class,
        ],
        ClaimWasUpdated::class => [
            UpdateClaim::class,
        ],
        ClaimWasVerified::class => [
            UpdateClaimVerificationStatus::class,
        ],
        ClaimVisitDateWasSet::class => [
            UpdateClaimContactStatus::class,
        ],
        ClaimWasVisited::class => [
            UpdateClaimVisitStatus::class,
        ],
        ClaimWasCanceled::class => [
            UpdateClaimCancelStatus::class,
        ],
    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher $events
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);

        //
    }
}
