<?php

namespace App\Providers;

use App\Http\ViewComposers\AdjustersComposer;
use App\Http\ViewComposers\AnalystsComposer;
use App\Http\ViewComposers\ClaimStatusesComposer;
use App\Http\ViewComposers\CountriesComposer;
use App\Http\ViewComposers\SurveysComposer;
use Illuminate\Support\ServiceProvider;
use App\Http\ViewComposers\ContactTypesComposer;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot()
    {
        // Contact Types
        view()->composer([
            'resources.claims.contacts.form',
        ], ContactTypesComposer::class);

        // Countries
        view()->composer(['resources.claims.*'], CountriesComposer::class);
        // Claim Statuses
        view()->composer(['resources.claims.*'], ClaimStatusesComposer::class);
        // Survey Templates
        view()->composer(['resources.claims.*'], SurveysComposer::class);
        // Adjusters Users
        view()->composer(['resources.claims.*'], AdjustersComposer::class);
        // Analyst Users
        view()->composer(['resources.claims.*'], AnalystsComposer::class);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}