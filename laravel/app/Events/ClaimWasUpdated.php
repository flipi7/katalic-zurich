<?php

namespace App\Events;


use App\Models\Claim;
use Illuminate\Queue\SerializesModels;

class ClaimWasUpdated extends Event
{
    use SerializesModels;
    public $claim;
    public $properties;

    public function __construct(Claim $claim, $properties)
    {
        $this->claim = $claim;
        $this->properties = $properties;
    }

}