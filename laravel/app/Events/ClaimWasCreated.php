<?php

namespace App\Events;


use App\Models\Claim;
use Illuminate\Queue\SerializesModels;

class ClaimWasCreated extends Event
{
    use SerializesModels;
    public $claim;

    public function __construct(Claim $claim)
    {
        $this->claim = $claim;
    }

}