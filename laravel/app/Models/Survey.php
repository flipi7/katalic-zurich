<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class Survey extends Model implements AuthorizableContract

{
    use Authorizable;

    protected $table = 'surveys';
    protected $fillable = ['name'];

    public function questions()
    {
        return $this->hasMany(SurveyQuestion::class, 'template_id');
    }
}
