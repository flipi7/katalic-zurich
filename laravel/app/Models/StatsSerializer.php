<?php

namespace App\Models;

use App\Models\SurveyTemplate;
use App\Http\Requests;
use App\Repositories\Models\ClaimRepository;

class StatsSerializer
{
    static public function getClaimsStatsData($claimRepository, $start, $end)
    {
        $claims = $claimRepository->findByDateRange($start, $end);
        
        $claimsByDay = StatsSerializer::getClaimsByDay($claimRepository, $start, $end);

        $claimsByCover = StatsSerializer::getClaimsByCover($claimRepository, $start, $end);

        $claimsByStates = StatsSerializer::getClaimsByStates($claimRepository, $start, $end);

        $claimsByCities = StatsSerializer::getClaimsByCities($claimRepository, $start, $end);

        $data = [
            'claimsByDay' => $claimsByDay,
            'claimsByCover' => $claimsByCover,
            'claimsByStates' => $claimsByStates,
            'counters' => [
                'totalClaims' => count($claims),
                'averageClaims' => count($claims) / count(StatsSerializer::generateDateRange($start, $end))
                ],
        ];

        return $data;
    }

    /**
     * @param ClaimRepository $claimRepository
     * @param Carbon @start
     * @param Carbon @end
     * @return array
     */
    static protected function getClaimsByDay(ClaimRepository $claimRepository, $start, $end)
    {
        $claimsByDay = [];
        $datesInRange = StatsSerializer::generateDateRange($start, $end);

        foreach ($datesInRange as $date) {
            $numberOfClaims = count($claimRepository->findByDate($date));
            $claimsByDay[] = [
                'date' => $date->format('d-m-Y'),
                'claims' => $numberOfClaims,
            ];
        }

        return $claimsByDay;
    }

    static protected function getClaimsByCover($claimRepository, $start, $end)
    {
        $data = $claimRepository->findByDateRange($start, $end, ['id']);

        $surveyCounts = [];

        $claim_surveys = \DB::table('claim_survey')
            ->join('surveys', 'claim_survey.survey_id', '=', 'surveys.id')
            ->select('name')
            ->whereIn('claim_id', $data)
            ->get();

        foreach ($claim_surveys as $claim_survey) {
            if (!isset($surveyCounts[$claim_survey->name])) {
                $surveyCounts[$claim_survey->name] = 1;
            }
            else {
                $surveyCounts[$claim_survey->name] += 1;
            }
        }

        $result = [];

        foreach ($surveyCounts as $key => $value) {
            $result[] = [
                'cover' => $key,
                'claims' => $value
            ];
        }

        // Sort by claims
        usort($result, function ($a, $b) {
            return $b['claims'] - $a['claims'];
        });

        return $result;
    }

    static protected function getClaimsByStates($claimRepository, $start, $end)
    {
        $data = $claimRepository->findByDateRange($start, $end, ['id']);

        $stateCounts = [];

        $addresses = \DB::table('addresses')
            ->select('*')
            ->whereIn('addressable_id', $data)
            ->get();

        foreach ($addresses as $address) {
            if (!isset($stateCounts[$address->state_id])) {
                $stateCounts[$address->state_id] = 1;
            }
            else {
                $stateCounts[$address->state_id] += 1;
            }
        }

        $result = [];

        foreach ($stateCounts as $key => $value) {
            $result[] = [
                'state_id' => $key,
                'claims' => $value
            ];
        }

        // Sort by claims
        usort($result, function ($a, $b) {
            return $a['state_id'] - $b['state_id'];
        });

        return $result;
    }

    static protected function getClaimsByCities($claimRepository, $start, $end)
    {
        $data = $claimRepository->findByDateRange($start, $end, ['id']);

        $stateCounts = [];

        $addresses = \DB::table('addresses')
            ->select('*')
            ->whereIn('addressable_id', $data)
            ->get();

        foreach ($addresses as $address) {
            if (!isset($stateCounts[$address->city_id])) {
                $stateCounts[$address->city_id] = 1;
            }
            else {
                $stateCounts[$address->city_id] += 1;
            }
        }

        $result = [];

        foreach ($stateCounts as $key => $value) {
            $cityName = '-';
            $query = \DB::table('cities')->select('name')->where('id', $key)->get();
            if(count($query) > 0) {
                $cityName = $query[0]->name;
            }
            $result[] = [
                'city_id' => $key,
                'city_name' => $cityName,
                'claims' => $value
            ];
        }

        // Sort by claims
        usort($result, function ($a, $b) {
            return $a['claims'] - $b['claims'];
        });

        return $result;
    }

    static protected function generateDateRange($start_date, $end_date)
    {
        $dates = [];

        for($date = $start_date->copy(); $date->lte($end_date); $date->addDay()) {
            $dates[] = $date->copy();
        }

        return $dates;
    }

    static function maxValueInArray($array, $keyToSearch)
    {
        $currentMax = NULL;
        foreach($array as $arr)
        {
            foreach($arr as $key => $value)
            {
                if ($key == $keyToSearch && ($value >= $currentMax))
                {
                    $currentMax = $value;
                }
            }
        }

        return $currentMax;
    }

    static function minValueInArray($array, $keyToSearch)
    {
        $currentMin = NULL;
        foreach($array as $arr)
        {
            foreach($arr as $key => $value)
            {
                if ($key == $keyToSearch && ($value <= $currentMin))
                {
                    $currentMin = $value;
                }
            }
        }

        return $currentMin;
    }
}