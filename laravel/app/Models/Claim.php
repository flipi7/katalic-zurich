<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class Claim extends Model implements AuthorizableContract
{
    use Authorizable, SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'claims';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'policy_number',
        'policy_holder',
        'description',
        'occurred_at',
        'amount_loss',
        'currency',
        'zurich_claim_number',
        'is_cover_correct',
        'last_status',
        'hash'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'occurred_at', 'deleted_at'];

    /**
     * The user that belongs to the claim.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function address()
    {
        return $this->morphOne(Address::class, 'addressable');
    }

    public function dataIke()
    {
        return $this->hasOne(DataIke::class);
    }

    public function dataVerification()
    {
        return $this->hasOne(DataVerification::class);
    }

    public function surveyAnswers()
    {
        return $this->belongsToMany(SurveyQuestion::class, 'survey_answers', 'claim_id', 'question_id')->withPivot('answer');
    }

    public function surveys()
    {
        return $this->belongsToMany(Survey::class);
    }

    public function adjuster()
    {
        return $this->belongsTo(User::class);
    }

    public function contacts()
    {
        return $this->hasMany(Contact::class);
    }

    public function notes()
    {
        return $this->hasMany(Note::class);
    }

    public function documents()
    {
        return $this->hasMany(Document::class);
    }

    public function activities()
    {
        return $this->hasMany(Activity::class);
    }

    public function dataContact()
    {
        return $this->hasMany(DataContact::class);
    }

    public function dataVisits()
    {
        return $this->hasMany(DataVisit::class);
    }

    public function dataPreliminary()
    {
        return $this->hasOne(DataPreliminary::class);
    }

    public function dataReport()
    {
        return $this->hasOne(DataReport::class);
    }

    public function damageAssessments()
    {
        return $this->hasMany(DamageAssessment::class);
    }

    public function damageAssessmentsLoss()
    {
        return $this->damageAssessments()->sum('amount_loss');
    }

    public function damageAssessmentsDeductible()
    {
        return $this->damageAssessments()->sum('deductible');
    }

    public function checkListAnswers()
    {
        return $this->hasMany(CheckListAnswer::class);
    }

    /**
     * The statuses that belong to the claim.
     */
    public function statuses()
    {
        return $this->belongsToMany(Status::class)->withTimestamps();
    }

    public function getStatusAttribute()
    {
        return $this->statuses()->orderBy('created_at', 'DESC')->first();
    }

    public function getHighestStatus()
    {
        return $this->statuses()->max('status_id');
    }

    /*
     * Scopes
     */
    public function scopeRecent($query)
    {
        return $query->orderBy('occurred_at', 'DESC');
    }

    public function scopeSearch($query, $data)
    {
        if (!empty($data['claim']['ike_reference'])) {
            $query->where('ike_reference', $data['claim']['ike_reference']);
        }
    }

    public function isDataComplete()
    {
        // Get required fields
        $exclude = [
            'zurich_claim_number',
            'deleted_at',
            'is_cover_correct'
        ];
        $fields = array_diff($this->fillable, $exclude);

        // Check fields
        foreach ($fields as $property) {
            if (empty($this->{$property})) {
                return false;
            }
        }

        // Check for Ike
        if (empty($this->dataIke->id)) {
            return false;
        }

        // Check for Address
        if (empty($this->address->id)) {
            return false;
        }

        // Check for Contacts
        if ($this->contacts->isEmpty()) {
            return false;
        }

        // Check for a survey
        if (empty($this->surveys()->first()->id)) {
            return false;
        }


        // Check for Survey Answers
//        if ($this->surveyAnswers->isEmpty()) {
//            return false;
//        }
//
//        // Check for each Survey Answers
//        foreach ($this->surveys as $survey) {
//            if ($survey->questions->count() != $this->surveyAnswers->where('template_id', $survey->id)->count()) {
//                return false;
//            }
//        }

        return true;
    }

    public function isDataVerificationComplete()
    {
        // Required fields
        $fields = [
            'zurich_claim_number',
            'is_cover_correct',
        ];

        // Check fields
        foreach ($fields as $property) {
            if (empty($this->{$property})) {
                return false;
            }
        }

        // Check for Address
        if (empty($this->dataVerification->id)) {
            return false;
        }

        return true;
    }

    public function hasStatus($statusId)
    {
        $statuses = $this->statuses()->get()->toArray();
        foreach($statuses as $status) {
            if($status['pivot']['status_id'] == $statusId){
                return TRUE;
            }
        }
        return FALSE;
    }
    
    public function canBeVerified()
    {
        return !$this->hasStatus(\App\Models\Status::VERIFICATION) &&
               ($this->status->id == \App\Models\Status::CREATED     ||
                $this->status->id == \App\Models\Status::CONTACT     ||
                $this->status->id == \App\Models\Status::VISIT       ||
                $this->status->id == \App\Models\Status::PRELIMINARY ||
                $this->status->id == \App\Models\Status::REPORT);
    }

    public function thereIsAVisibleNoteForUser($userRoleId)
    {
        if ($userRoleId == Role::COORDINADOR || $userRoleId == Role::SUPER_ADMIN) {
            return TRUE;
        }
        else {
            foreach ($this->notes()->getResults() as $note) {
                if ($note->noteIsVisibleForUser($userRoleId)) {
                    return TRUE;
                }
            }
            return FALSE;
        }
    }
}
