<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class DataPreliminary extends Model
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'claim_preliminary';

    protected $dates = ['visit_date','claim_date','validity_start','validity_end','created_at','updated_at','deleted_at'];

    /**
     * The user that belongs to the claim.
     */
    public function claim()
    {
        return $this->belongsTo(Claim::class);
    }

    protected $fillable = ['visit_date','claim_date','validity_start','validity_end', 'description', 'third_party', 'cover', 'amount_loss'];

}
