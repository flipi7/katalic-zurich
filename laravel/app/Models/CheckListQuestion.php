<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class CheckListQuestion extends Model implements AuthorizableContract

{
    use Authorizable;

    protected $table = 'check_list_questions';
    protected $fillable = ['name'];
}
