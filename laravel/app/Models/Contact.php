<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Contact extends Model
{
    use SoftDeletes;

    protected $fillable = ['name', 'phone', 'email', 'description', 'type'];
    public static $types = [
        'insured' => 'Insured',
        'agent' => 'Agent',
        'injured' => 'Injured',
        'causative' => 'Causative',
        'other' => 'Other'
    ];

}
