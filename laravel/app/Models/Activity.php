<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Activity extends Model
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'claim_activities';

    protected $dates = ['date','created_at','updated_at','deleted_at'];

    /**
     * The user that belongs to the claim.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * The user that belongs to the claim.
     */
    public function claim()
    {
        return $this->belongsTo(Claim::class);
    }

    public function getAttachment()
    {
        if ($this->activity == 'contact') {
            return DataContact::findOrFail($this->attachment);
        }
        elseif ($this->activity == 'visit') {
            return DataVisit::findOrFail($this->attachment);
        }
        elseif ($this->activity == 'preliminary') {
            return DataPreliminary::findOrFail($this->attachment);
        }
        elseif ($this->activity == 'report') {
            return DataReport::findOrFail($this->attachment);
        }
        elseif ($this->activity == 'note') {
            try {
                $note = Note::findOrFail($this->attachment);
                return $note;
            } catch(\Exception $e) {
                return null;
            }
        }
        elseif($this->activity == 'document') {
            return Document::findOrFail($this->attachment);
        }
        elseif($this->activity == 'update' || $this->activity == 'add' || $this->activity == 'delete') {
            return $this->attachment;
        }
    }

    public function Activity()
    {

    }

    protected $fillable = ['activity','date','user_id','claim_id','attachment'];

}
