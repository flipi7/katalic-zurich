<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class DataVisit extends Model
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'claim_visits';

    protected $dates = ['date','created_at','updated_at','deleted_at'];

    /**
     * The user that belongs to the claim.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * The user that belongs to the claim.
     */
    public function claim()
    {
        return $this->belongsTo(Claim::class);
    }

    protected $fillable = ['date', 'description', 'user_id'];

}
