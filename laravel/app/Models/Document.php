<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Document extends Model
{
    use SoftDeletes;

    /**
     * The user that belongs to the claim.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * The user that belongs to the claim.
     */
    public function claim()
    {
        return $this->belongsTo(Claim::class);
    }

    public function attachmentExtension()
    {
        preg_match('/\.([^\.]+)$/', $this->attachmentName, $result);
        return $result[0];
    }

    public function isDocument()
    {
        $documents = ['.pdf','.odt','.ods','.odp'];
        return in_array($this->attachmentExtension(), $documents);
    }

    protected $fillable = ['date', 'description', 'user_id', 'attachmentHash', 'attachmentName'];

}
