<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class DataReport extends Model
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'data_report';

    protected $dates = ['created_at','updated_at','deleted_at'];

    /**
     * The user that belongs to the claim.
     */
    public function claim()
    {
        return $this->belongsTo(Claim::class);
    }

    /**
     * The user that belongs to the claim.
     */
    public function damageAssessment()
    {
        return $this->hasMany(DamageAssessment::class);
    }

    protected $fillable = ['salvage', 'subrogation', 'agreement', 'insurance', 'fraud', 'claim_report', 'damage_assessment'];

}
