<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class Status extends Model implements AuthorizableContract

{
    use Authorizable;

    const CREATED = 1;
    const VERIFICATION = 2;
    const CONTACT = 3;
    const VISIT = 4;
    const PRELIMINARY = 5;
    const REPORT = 6;
    const PROCESSING = 7;
    const FINISHED = 8;
    const CANCELLED = 9;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'statuses';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * The claims that belong to the status.
     */
    public function claims()
    {
        return status::belongsToMany(Claim::class);
    }

    static public function getStatusId($literal)
    {
        if ($literal == 'created') {
            return Status::CREATED;
        }

        elseif ($literal == 'verification') {
            return Status::VERIFICATION;
        }

        elseif ($literal == 'contact') {
            return Status::CONTACT;
        }

        elseif ($literal == 'visit') {
            return Status::VISIT;
        }

        elseif ($literal == 'preliminary') {
            return Status::PRELIMINARY;
        }

        elseif ($literal == 'report') {
            return Status::REPORT;
        }

        elseif ($literal == 'processing') {
            return Status::PROCESSING;
        }

        elseif ($literal == 'finished') {
            return Status::FINISHED;
        }

        elseif ($literal == 'cancelled') {
            return Status::CANCELLED;
        }
    }

    
    static public function getStatusName($id)
    {
        if ($id == 1) {
            return 'created';
        }

        elseif ($id == 2) {
            return 'verification';
        }

        elseif ($id == 3) {
            return 'contact';
        }

        elseif ($id == 4) {
            return 'visit';
        }

        elseif ($id == 5) {
            return 'preliminary';
        }

        elseif ($id == 6) {
            return 'report';
        }

        elseif ($id == 7) {
            return 'processing';
        }

        elseif ($id == 8) {
            return 'finished';
        }

        elseif ($id == 9) {
            return 'cancelled';
        }
    }

    static public function statusIsEditableForUserRole($status, $roleId)
    {
        if ($roleId == Role::SUPER_ADMIN || $roleId == Role::COORDINADOR) {
            return TRUE;
        }
        elseif ($roleId == Role::CABINA_DANOS) {
            if ($status == Status::CREATED) {
                return TRUE;
            }
        }
        elseif ($roleId == Role::MESA_CONTROL) {
            if ($status == Status::VERIFICATION) {
                return TRUE;
            }
        }
        elseif ($roleId == Role::AJUSTADOR) {
            if ($status == Status::CONTACT
                || $status == Status::VISIT
                || $status == Status::PRELIMINARY
                || $status == Status::REPORT) {
                return TRUE;
            }
        }
        elseif ($roleId == Role::ANALISTA) {
            if ($status == Status::PROCESSING) {
                return TRUE;
            }
        }
        return FALSE;
    }

    static public function statusIsAvailableForClaim($status, $claim, $roleId)
    {
        if ($status <= 3) {
            return TRUE;
        }
        elseif ($status == 9 && ($roleId == Role::SUPER_ADMIN || $roleId == Role::COORDINADOR)) {
            if($claim->status->id == Status::FINISHED) {
                return FALSE;
            }
            else {
                return TRUE;
            }
        }
        else {
            return $status <= $claim->status->id + 1;
        }
    }
}
