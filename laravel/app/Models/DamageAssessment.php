<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class DamageAssessment extends Model
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'damage_assessment';

    protected $dates = ['created_at','updated_at','deleted_at'];

    /**
     * The user that belongs to the claim.
     */
    public function dataReport()
    {
        return $this->belongsTo(DataReport::class);
    }

    protected $fillable = ['cover', 'amount_loss', 'deductible', 'adjustment_description'];
}
