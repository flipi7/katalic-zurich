<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class SurveyAnswer extends Model implements AuthorizableContract

{
    use Authorizable;

    protected $table = 'survey_answers';
    protected $fillable = ['claim_id', 'question_id'];
}
