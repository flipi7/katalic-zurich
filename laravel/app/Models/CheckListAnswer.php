<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class CheckListAnswer extends Model implements AuthorizableContract

{
    use Authorizable;

    protected $table = 'check_list_answers';
    protected $fillable = ['claim_id', 'question_id', 'answer'];

    static public function getAnswerByQuestionId($claimId, $questionId) {
        return CheckListAnswer::where('claim_id', $claimId)->where('question_id', $questionId)->get()->first()['answer'];
    }
}
