<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class Role extends Model implements AuthorizableContract

{
    use Authorizable;

    const SUPER_ADMIN = 1;
    const CABINA_DANOS = 2;
    const MESA_CONTROL = 3;
    const ANALISTA = 4;
    const AJUSTADOR = 5;
    const COORDINADOR = 6;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'roles';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * The users that belong to the role.
     */
    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public static function getRoles()
    {
        return Role::select('*')->get();
    }
}
