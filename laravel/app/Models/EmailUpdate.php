<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class EmailUpdate extends Model
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'email_update';

    protected $dates = ['created_at','updated_at','deleted_at'];

    protected $fillable = ['last_email_id', 'number_of_emails'];

}
