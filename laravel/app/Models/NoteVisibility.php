<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class NoteVisibility extends Model
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'notes_visibilities';

    /**
     * The note to which it belongs
     */
    public function note()
    {
        return $this->belongsTo(Note::class);
    }

    /**
     * The roles related to the note
     */
    public function roles()
    {
        return $this->hasMany(Role::class);
    }

    protected $fillable = ['note_id', 'role_id'];
}
