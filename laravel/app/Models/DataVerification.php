<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DataVerification extends Model
{
    protected $table = 'claim_data_verification';
    protected $fillable = [
        'policy_payment',
        'validity_start',
        'validity_end',
        'siu',
        'coinsurance',
        'coinsurance_company',
        'coinsurance_percentage',
        'complaint',
        'analyst_id'
    ];
}
