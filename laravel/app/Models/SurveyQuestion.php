<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class SurveyQuestion extends Model implements AuthorizableContract

{
    use Authorizable;

    protected $table = 'survey_questions';
    protected $fillable = ['name', 'template_id'];
}
