<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DataContact extends Model
{
    protected $table = 'claim_data_contact';
    protected $fillable = ['contact', 'visit_date', 'note'];
}
