<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DataIke extends Model
{
    protected $table = 'claim_data_ike';
    protected $fillable = ['reference', 'adjuster_id'];
}
