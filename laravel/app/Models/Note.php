<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Note extends Model
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'notes';

    /**
     * The user to which it belongs
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * The claim to which it belongs
     */
    public function claim()
    {
        return $this->belongsTo(Claim::class);
    }

    /**
     * The visibilities of the note
     */
    public function visibilities()
    {
        return $this->hasMany(NoteVisibility::class);
    }

    protected $fillable = ['date', 'description', 'user_id', 'attachmentHash', 'attachmentName'];

    public function noteIsVisibleForUser($userRoleId)
    {
        if ($userRoleId == Role::COORDINADOR || $userRoleId == Role::SUPER_ADMIN) {
            return TRUE;
        }
        else {
            foreach ($this->visibilities()->getResults() as $visibility) {
                if ($visibility['role_id'] == $userRoleId) {
                    return TRUE;
                }
            }
            return FALSE;
        }
    }

}
