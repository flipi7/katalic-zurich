@extends('layouts.master')

@section('content')

    <div class="page-title">

        <div class="title-env">
            <h1 class="title">{!! trans('global.Statistics') !!}</h1>

            <p class="description">{!! trans('global.Statistics subtitle') !!}</p>
        </div>

        <div class="breadcrumb-env">
            {!! Breadcrumbs::render('stats') !!}
        </div>

    </div>

    <div class="stats">

        <div class="panel panel-default" id="stats-selector">
            <div class="panel-heading">
                <h3 class="panel-title">{!! trans('global.Statistics options') !!}</h3>
            </div>
            <div class="panel-body">
                <form role="form" class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="stats-range">{!! trans('global.Date range') !!}</label>
                        <div class="col-sm-9">
                            <input type="text" id="stats-range" class="form-control">
                            <input type="submit" class="hidden" id="refresh-data" action="{!! route('stats.get-claims') !!}">
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4">
                <div class="chart-item-bg">
                    <div class="chart-label">
                        <div class="h3 text-bold" style="color: #08407A;">{!! trans('global.Affected products') !!}</div>
                        <span class="text-medium text-muted">{!! trans('global.Affected policies by category') !!}</span>
                    </div>
                    <div id="affected-policies-category"></div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="chart-item-bg">
                    <div class="chart-label">
                        <div class="h3 text-bold" style="color: #08407A;">{!! trans('global.Top 5 covers') !!}</div>
                        <span class="text-medium text-muted">{!! trans('global.Claims by cover') !!}</span>
                    </div>
                    <div id="claims-by-cover"></div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="chart-item-bg">
                    <div class="chart-label">
                        <div class="h3 text-bold" id="average-claims" style="color: #08407A;" data-count="this" data-from="0" data-to="0" data-duration="1"></div>
                        <span class="text-medium text-muted">{!! trans('global.Claims average') !!}</span>
                    </div>
                    <div id="new-claims-day"></div>
                </div>
            </div>
        </div>

        <div class="row" id="counters">
            <div class="col-sm-3">
                <div class="xe-widget xe-counter xe-counter-primary" id="number-of-claims" data-count=".num" data-from="0" data-to="0" data-duration="1">
                    <div class="xe-icon">
                        <i class="fa fa-ambulance"></i>
                    </div>
                    <div class="xe-label">
                        <strong class="num">0</strong>
                        <span>{!! trans('global.Claims') !!}</span>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="xe-widget xe-counter" id="average-cost" data-count=".num" data-from="0" data-to="0" data-duration="1">
                    <div class="xe-icon">
                        <i class="fa fa-usd"></i>
                    </div>
                    <div class="xe-label">
                        <strong class="num">0</strong>
                        <span>{!! trans('global.Average cost') !!}</span>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <?php $averageClosingDays = rand(0,14);
                      $averageClosingHours  = rand(0,23);
                      $averageClosingMins  = rand(10,59); ?>
                <div class="xe-widget xe-counter xe-counter-info" data-count=".num">
                    <div class="xe-icon">
                        <i class="fa fa-clock-o"></i>
                    </div>
                    <div class="xe-label">
                        <strong class="num">{!! $averageClosingDays !!}d {!! $averageClosingHours !!}h {!! $averageClosingMins !!}m</strong>
                        <span>{!! trans('global.Average closing time') !!}</span>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="xe-widget xe-counter xe-counter-danger" id="no-coverage-claims" data-count=".num" data-from="0" data-to="0" data-duration="1">
                    <div class="xe-icon">
                        <i class="fa fa-times"></i>
                    </div>
                    <div class="xe-label">
                        <strong class="num">0</strong>
                        <span>{!! trans('global.No coverage claims') !!}</span>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">{!! trans('global.Claims by state in') !!} {!! trans('global.Mexico') !!}</h3>
                        <div class="panel-options">
                            <a href="#" data-toggle="panel">
                                <span class="collapse-icon">–</span>
                                <span class="expand-icon">+</span>
                            </a>
                            <a href="#" data-toggle="remove">×</a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-12">
                                @include('resources.stats.mexico')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">{!! trans('global.List of claims by district') !!}</h3>
                        <div class="panel-options">
                            <a href="#" data-toggle="panel">
                                <span class="collapse-icon">–</span> <span class="expand-icon">+</span>
                            </a>
                            <a href="#" data-toggle="remove">
                                ×
                            </a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="pre-scrollable" style="max-height: 400px;">
                            <table id="cities-table" class="table table-striped dataTable">
                                <thead>
                                <tr>
                                    <th>{!! trans('global.District') !!}</th>
                                    <th>{!! trans('global.State') !!}</th>
                                    <th>{!! trans('global.Claims') !!}</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('pageStyles')
    <link rel="stylesheet" href="{!! asset('assets/js/daterangepicker/daterangepicker-bs3.css') !!}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jvectormap/2.0.4/jquery-jvectormap.css">
@endsection

@section('pageScripts')
    <script src="{!! asset('assets/js/jquery-validate/jquery.validate.min.js') !!}"></script>
    <script src="{!! asset('assets/js/inputmask/jquery.inputmask.bundle.js') !!}"></script>
    <script src="{!! asset('assets/js/formwizard/jquery.bootstrap.wizard.min.js') !!}"></script>
    <script src="{!! asset('assets/js/datepicker/bootstrap-datepicker.js') !!}"></script>
    <script src="{!! asset('assets/js/multiselect/js/jquery.multi-select.js') !!}"></script>
    <script src="{!! asset('assets/js/jquery-ui/jquery-ui.min.js') !!}"></script>
    <script src="{!! asset('assets/js/selectboxit/jquery.selectBoxIt.min.js') !!}"></script>
    <script src="{!! asset('assets/js/app-geolocation.js') !!}"></script>

    <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/moment-range/2.1.0/moment-range.min.js"></script>

    <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery/jquery-2.1.4.min.js"></script>
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/knockout/3.4.0/knockout-min.js"></script>
    <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/globalize/0.1.1/globalize.min.js"></script>
    <script type="text/javascript" src="http://cdn3.devexpress.com/jslib/15.2.4/js/dx.chartjs.js"></script>
    <script src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
    <script src="{!! asset('assets/js/daterangepicker/daterangepicker.js') !!}"></script>
    <script src="{!! asset('assets/js/stats_index.js') !!}"></script>
@endsection