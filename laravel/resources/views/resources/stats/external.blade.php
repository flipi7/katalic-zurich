@extends('layouts.master')

@section('content')

    <div class="page-title">

        <div class="title-env">
            <h1 class="title">{!! trans('global.Statistics') !!}</h1>

            <p class="description">{!! trans('global.External statistics subtitle') !!}</p>
        </div>

        <div class="breadcrumb-env">
            {!! Breadcrumbs::render('stats') !!}
        </div>

    </div>

    <div class="row">
        <div class="col-sm-12">
            <table id="external-table" class="table table-striped table-bordered dataTable">
                <thead style="font-size: 8pt;">
                    <tr>
                        <th rowspan="2" style="vertical-align: middle !important">{!! trans('global.Adjuster') !!}</th>
                        <th rowspan="2" style="vertical-align: middle !important">{!! trans('global.Assigned') !!}</th>
                        <th rowspan="2" style="vertical-align: middle !important">{!! trans('global.Closed') !!}</th>
                        <th colspan="5" style="text-align: center;">{!! trans('global.Closed time') !!}</th>
                        <th rowspan="2" style="vertical-align: middle !important">% INF</th>
                        <th rowspan="2" style="vertical-align: middle !important">% EXC</th>
                        <th rowspan="2" style="vertical-align: middle !important">% FRA</th>
                        <th rowspan="2" style="vertical-align: middle !important">% CON</th>
                        <th rowspan="2" style="vertical-align: middle !important">% INC</th>
                        <th colspan="5" style="text-align: center;">{!! trans('global.Average cost') !!}</th>
                        <th rowspan="2" style="vertical-align: middle !important">{!! trans('global.Score') !!}</th>
                    </tr>
                    <tr>
                        <th>0 - 3K</th>
                        <th>3K - 8K</th>
                        <th>8K - 30K</th>
                        <th>> 30K</th>
                        <th>Total</th>
                        <th>0 - 3K</th>
                        <th>3K - 8K</th>
                        <th>8K - 30K</th>
                        <th>> 30K</th>
                        <th>Total</th>
                    </tr>
                </thead>
                <tbody>
                {{--@foreach(range(0, 20) as $number)--}}
                    {{--<tr>--}}
                        {{--<td>{!! floor($number * rand(2,10)) !!}</td>--}}
                        {{--<td>{!! rand(15, 30) !!}</td>--}}
                        {{--<td>{!! rand(5, 15) !!}</td>--}}
                        {{--<td>{!! rand(200, 3000) !!}</td>--}}
                        {{--<td>{!! rand(3001, 8000) !!}</td>--}}
                        {{--<td>{!! rand(8001, 30000) !!}</td>--}}
                        {{--<td>{!! rand(30000, 60000) !!}</td>--}}
                        {{--<td>{!! rand(20000, 90000) !!}</td>--}}
                        {{--<td>{!! number_format(rand(0, 10000) * 100 / 10000, 2, ',', '') !!}</td>--}}
                        {{--<td>{!! number_format(rand(0, 10000) * 100 / 10000, 2, ',', '') !!}</td>--}}
                        {{--<td>{!! number_format(rand(0, 10000) * 100 / 10000, 2, ',', '') !!}</td>--}}
                        {{--<td>{!! number_format(rand(0, 10000) * 100 / 10000, 2, ',', '') !!}</td>--}}
                        {{--<td>{!! number_format(rand(0, 10000) * 100 / 10000, 2, ',', '') !!}</td>--}}
                        {{--<td>{!! rand(200, 3000) !!}</td>--}}
                        {{--<td>{!! rand(3001, 8000) !!}</td>--}}
                        {{--<td>{!! rand(8001, 30000) !!}</td>--}}
                        {{--<td>{!! rand(30000, 60000) !!}</td>--}}
                        {{--<td>{!! rand(20000, 90000) !!}</td>--}}
                        {{--<td>{!! number_format(rand(0, 1000) * 10 / 1000, 2, ',', '') !!}</td>--}}
                    {{--</tr>--}}
                    {{--@endforeach--}}
                <tr><td>Juan Martín Zorita</td>	<td>22</td>	<td>11</td>	<td>8,9</td>	<td>8,8</td>	<td>12,9</td>	<td>29,6</td>	<td>15,05</td>	<td>13</td>	<td>18</td>	<td>3</td>	<td>9</td>	<td>12</td>	<td>1064</td>	<td>6682</td>	<td>19117</td>	<td>46245</td>	<td>48819</td>	<td>1,88</td></tr>
                <tr><td>Inhué Rubio</td>	<td>30</td>	<td>6</td>	<td>8,5</td>	<td>12,9</td>	<td>22,4</td>	<td>32,4</td>	<td>19,05</td>	<td>15</td>	<td>11</td>	<td>5</td>	<td>13</td>	<td>18</td>	<td>2663</td>	<td>6603</td>	<td>22439</td>	<td>54051</td>	<td>52944</td>	<td>0,54</td></tr>
                <tr><td>Raul Laguna	<td>24</td>	<td>5</td>	<td>4,7</td>	<td>7,6</td>	<td>14</td>	<td>32,7</td>	<td>14,75</td>	<td>17</td>	<td>11</td>	<td>2</td>	<td>12</td>	<td>20</td>	<td>614</td>	<td>4454</td>	<td>29816</td>	<td>39749</td>	<td>34977</td>	<td>2,67</td></tr>
                <tr><td>Efrain Ferrant</td>	<td>16</td>	<td>13</td>	<td>6,3</td>	<td>10,6</td>	<td>13</td>	<td>27,5</td>	<td>14,35</td>	<td>23</td>	<td>33</td>	<td>2</td>	<td>15</td>	<td>12</td>	<td>1367</td>	<td>5011</td>	<td>27651</td>	<td>34744</td>	<td>43496</td>	<td>9,45</td></tr>
                <tr><td>Aarón Saelice</td>s	<td>25</td>	<td>13</td>	<td>5,3</td>	<td>12,2</td>	<td>20,5</td>	<td>21,1</td>	<td>14,775</td>	<td>17</td>	<td>18</td>	<td>7</td>	<td>11</td>	<td>20</td>	<td>1735</td>	<td>6450</td>	<td>18300</td>	<td>47210</td>	<td>30247</td>	<td>8,35</td></tr>
                <tr><td>Rigoberto Puig</td>	<td>16</td>	<td>6</td>	<td>9</td>	<td>8,1</td>	<td>12</td>	<td>33,5</td>	<td>15,65</td>	<td>19</td>	<td>33</td>	<td>1</td>	<td>11</td>	<td>27</td>	<td>1355</td>	<td>5578</td>	<td>26311</td>	<td>59462</td>	<td>70677</td>	<td>9,34</td></tr>
                <tr><td>Orlando Illescas</td>	<td>22</td>	<td>6</td>	<td>1</td>	<td>11,5</td>	<td>20,7</td>	<td>19,8</td>	<td>13,25</td>	<td>18</td>	<td>32</td>	<td>4</td>	<td>6</td>	<td>16</td>	<td>650</td>	<td>3875</td>	<td>14373</td>	<td>55840</td>	<td>62873</td>	<td>1,54</td></tr>
                <tr><td>Joaquín Beldad</td>	<td>21</td>	<td>11</td>	<td>6,2</td>	<td>11,5</td>	<td>19,7</td>	<td>19,9</td>	<td>14,325</td>	<td>26</td>	<td>35</td>	<td>6</td>	<td>9</td>	<td>27</td>	<td>2746</td>	<td>5358</td>	<td>15488</td>	<td>49756</td>	<td>44550</td>	<td>8,42</td></tr>
                <tr><td>Alexis Cambeiro</td>	<td>29</td>	<td>13</td>	<td>5,5</td>	<td>12,2</td>	<td>14,6</td>	<td>18,6</td>	<td>12,725</td>	<td>5</td>	<td>33</td>	<td>7</td>	<td>13</td>	<td>33</td>	<td>1639</td>	<td>4268</td>	<td>19422</td>	<td>53176</td>	<td>66804</td>	<td>1,76</td></tr>
                <tr><td>Cristian Manzanares</td>	<td>25</td>	<td>9</td>	<td>2,8</td>	<td>8,7</td>	<td>19,5</td>	<td>23,4</td>	<td>13,6</td>	<td>5</td>	<td>35</td>	<td>1</td>	<td>9</td>	<td>29</td>	<td>1288</td>	<td>6145</td>	<td>23416</td>	<td>33814</td>	<td>79981</td>	<td>7,26</td></tr>
                <tr><td>Facundo Nores</td>	<td>24</td>	<td>10</td>	<td>8,9</td>	<td>12,2</td>	<td>11,4</td>	<td>31,2</td>	<td>15,925</td>	<td>7</td>	<td>35</td>	<td>6</td>	<td>10</td>	<td>19</td>	<td>2254</td>	<td>6005</td>	<td>15160</td>	<td>38641</td>	<td>89989</td>	<td>2,48</td></tr>
                <tr><td>Manuel Arnal</td>	<td>15</td>	<td>10</td>	<td>4,9</td>	<td>11</td>	<td>19,9</td>	<td>22,1</td>	<td>14,475</td>	<td>14</td>	<td>15</td>	<td>2</td>	<td>6</td>	<td>9</td>	<td>1287</td>	<td>4986</td>	<td>13824</td>	<td>48284</td>	<td>43016</td>	<td>5,66</td></tr>
                <tr><td>Justin Carvallo</td>	<td>29</td>	<td>12</td>	<td>3,2</td>	<td>9,5</td>	<td>17,6</td>	<td>17,4</td>	<td>11,925</td>	<td>17</td>	<td>32</td>	<td>4</td>	<td>15</td>	<td>11</td>	<td>666</td>	<td>6075</td>	<td>9448</td>	<td>30213</td>	<td>47689</td>	<td>7,75</td></tr>
                <tr><td>Rafael Céspedes</td>	<td>16</td>	<td>10</td>	<td>4,9</td>	<td>9,1</td>	<td>20,9</td>	<td>15,4</td>	<td>12,575</td>	<td>21</td>	<td>18</td>	<td>5</td>	<td>13</td>	<td>11</td>	<td>1545</td>	<td>3769</td>	<td>20768</td>	<td>41767</td>	<td>74000</td>	<td>7,42</td></tr>
                <tr><td>Macos Martínez</td>	<td>17</td>	<td>8</td>	<td>4,7</td>	<td>10,3</td>	<td>18,3</td>	<td>21,1</td>	<td>13,6</td>	<td>22</td>	<td>30</td>	<td>5</td>	<td>8</td>	<td>19</td>	<td>1283</td>	<td>5150</td>	<td>13997</td>	<td>47889</td>	<td>84370</td>	<td>3,55</td></tr>
                <tr><td>Lucho Puig</td>	<td>17</td>	<td>13</td>	<td>5,5</td>	<td>12,1</td>	<td>10,8</td>	<td>31,2</td>	<td>14,9</td>	<td>19</td>	<td>32</td>	<td>4</td>	<td>13</td>	<td>7</td>	<td>2222</td>	<td>7708</td>	<td>24857</td>	<td>31379</td>	<td>28371</td>	<td>7,80</td></tr>
                <tr><td>Dídac Muñoz</td>	<td>24</td>	<td>12</td>	<td>7,3</td>	<td>9</td>	<td>11</td>	<td>34,5</td>	<td>15,45</td>	<td>17</td>	<td>31</td>	<td>4</td>	<td>8</td>	<td>23</td>	<td>1027</td>	<td>3624</td>	<td>15036</td>	<td>49126</td>	<td>29353</td>	<td>0,72</td></tr>
                <tr><td>Alfredo Varela</td>	<td>30</td>	<td>11</td>	<td>5,4</td>	<td>7,2</td>	<td>11,6</td>	<td>25</td>	<td>12,3</td>	<td>16</td>	<td>33</td>	<td>3</td>	<td>10</td>	<td>6</td>	<td>275</td>	<td>6963</td>	<td>10685</td>	<td>52243</td>	<td>26062</td>	<td>6,35</td></tr>
                <tr><td>Rigoberto Arnal</td>	<td>26</td>	<td>9</td>	<td>2,1</td>	<td>12</td>	<td>10,4</td>	<td>15,9</td>	<td>10,1</td>	<td>4</td>	<td>35</td>	<td>4</td>	<td>7</td>	<td>30</td>	<td>1393</td>	<td>4251</td>	<td>12822</td>	<td>30256</td>	<td>65827</td>	<td>6,31</td></tr>
                <tr><td>Juanita Pozo</td>	<td>20</td>	<td>11</td>	<td>2,3</td>	<td>7,7</td>	<td>10</td>	<td>22,4</td>	<td>10,6</td>	<td>8</td>	<td>31</td>	<td>4</td>	<td>14</td>	<td>21</td>	<td>2743</td>	<td>4575</td>	<td>8429</td>	<td>44673</td>	<td>44602</td>	<td>5,48</td></tr>
                <tr><td>Álvaro Cortés</td>	<td>28</td>	<td>13</td>	<td>1,8</td>	<td>10,1</td>	<td>22,9</td>	<td>20,8</td>	<td>13,9</td>	<td>7</td>	<td>19</td>	<td>7</td>	<td>9</td>	<td>33</td>	<td>1890</td>	<td>5437</td>	<td>25947</td>	<td>47153</td>	<td>48604</td>	<td>2,81</td></tr>
                </tbody>
                <tfoot>
                {{--<tr>--}}
                    {{--<td>{!! trans('global.Average') !!}</td>--}}
                    {{--<td>{!! rand(20, 25) !!}</td>--}}
                    {{--<td>{!! rand(8, 13) !!}</td>--}}
                    {{--<td>{!! rand(600, 2600) !!}</td>--}}
                    {{--<td>{!! rand(3401, 7600) !!}</td>--}}
                    {{--<td>{!! rand(8401, 26000) !!}</td>--}}
                    {{--<td>{!! rand(34000, 56000) !!}</td>--}}
                    {{--<td>{!! rand(24000, 86000) !!}</td>--}}
                    {{--<td>{!! number_format(rand(2000, 9000) * 100 / 10000, 2, ',', '') !!}</td>--}}
                    {{--<td>{!! number_format(rand(2000, 9000) * 100 / 10000, 2, ',', '') !!}</td>--}}
                    {{--<td>{!! number_format(rand(2000, 9000) * 100 / 10000, 2, ',', '') !!}</td>--}}
                    {{--<td>{!! number_format(rand(2000, 9000) * 100 / 10000, 2, ',', '') !!}</td>--}}
                    {{--<td>{!! number_format(rand(2000, 9000) * 100 / 10000, 2, ',', '') !!}</td>--}}
                    {{--<td>{!! rand(600, 2600) !!}</td>--}}
                    {{--<td>{!! rand(3401, 7600) !!}</td>--}}
                    {{--<td>{!! rand(8401, 26000) !!}</td>--}}
                    {{--<td>{!! rand(34000, 56000) !!}</td>--}}
                    {{--<td>{!! rand(24000, 86000) !!}</td>--}}
                    {{--<td>{!! number_format(rand(200, 900) * 10 / 1000, 2, ',', '') !!}</td>--}}
                {{--</tr>--}}
                <tr><td>Media</td>	<td>23</td>	<td>11</td>	<td>5,2</td>	<td>10,2</td>	<td>15,9</td>	<td>24,5</td>	<td>13,9</td>	<td>14,7</td>	<td>27,1</td>	<td>4</td>	<td>10,5</td>	<td>19,1</td>	<td>1028</td>	<td>4663</td>	<td>8488</td>	<td>52062</td>	<td>81426</td>	<td>4,92</td></tr>
                </tfoot>
            </table>

            <script src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
            <script>
                $('#external-table').DataTable({
                    paging: false,
                    info: false,
                    searching: false,
                });
            </script>
        </div>
    </div>

@endsection

@section('pageStyles')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css">
@endsection
