@extends('layouts.master')

@section('content')

    <div class="page-title">

        <div class="title-env">
            <h1 class="title">{!! trans('global.Statistics') !!}</h1>

            <p class="description">{!! trans('global.User statistics subtitle') !!}</p>
        </div>

        <div class="breadcrumb-env">
            {!! Breadcrumbs::render('stats') !!}
        </div>

    </div>

    <div class="stats">
        <div class="row" id="counters">
            <div class="col-sm-3">
                <div class="xe-widget xe-counter xe-counter-warning">
                    <div class="xe-icon">
                        <i class="fa fa-star-o"></i>
                    </div>
                    <div class="xe-label">
                        <strong class="num">Juan Palomo</strong>
                        <span>{!! trans('global.Most closed claims') !!}</span>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="xe-widget xe-counter">
                    <div class="xe-icon">
                        <i class="fa fa-smile-o"></i>
                    </div>
                    <div class="xe-label">
                        <strong class="num">Manuel García</strong>
                        <span>{!! trans('global.Best analyst (reviewd by insured)') !!}</span>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="xe-widget xe-counter xe-counter-info">
                    <div class="xe-icon">
                        <i class="fa fa-clock-o"></i>
                    </div>
                    <div class="xe-label">
                        <strong class="num">3h 47m</strong>
                        <span>{!! trans('global.Best time') !!} (Juan Palomo)</span>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="xe-widget xe-counter xe-counter-danger">
                    <div class="xe-icon">
                        <i class="fa fa-times"></i>
                    </div>
                    <div class="xe-label">
                        <strong class="num">0</strong>
                        <span>{!! trans('global.No coverage claims') !!}</span>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4">
                <div class="chart-item-bg">
                    <div class="chart-label">
                        <div class="h3 text-bold" style="color: #08407A;">Top 6</div>
                        <span class="text-medium text-muted">{!! trans('global.Claims by status') !!}</span>
                    </div>
                    <div id="affected-policies-category"></div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="chart-item-bg">
                    <div class="chart-label">
                        <div class="h3 text-bold" style="color: #08407A;">{!! trans('global.Top 5 covers') !!}</div>
                        <span class="text-medium text-muted">{!! trans('global.Claims by analyst') !!}</span>
                    </div>
                    <div id="claims-by-cover"></div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">{!! trans('global.List of claims by analyst') !!}</h3>
                    </div>
                    <div class="panel-body">
                        <div class="pre-scrollable" style="max-height: 304px;">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>{!! trans('global.Analyst') !!}</th>
                                    <th>{!! trans('global.Assigned') !!}</th>
                                    <th>{!! trans('global.Closed') !!}</th>
                                    <th>%</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $analysts = [
                                        ['name' => 'José García', 'assignedClaims' => rand(7, 15), 'closedClaims' => rand(1,7)],
                                        ['name' => 'María Peinado', 'assignedClaims' => rand(7, 15), 'closedClaims' => rand(1,7)],
                                        ['name' => 'Mario Gallego', 'assignedClaims' => rand(7, 15), 'closedClaims' => rand(1,7)],
                                        ['name' => 'Ana López', 'assignedClaims' => rand(7, 15), 'closedClaims' => rand(1,7)],
                                        ['name' => 'Joaquín Reyes', 'assignedClaims' => rand(7, 15), 'closedClaims' => rand(1,7)],
                                        ['name' => 'Lorenzo Castro', 'assignedClaims' => rand(7, 15), 'closedClaims' => rand(1,7)],
                                        ['name' => 'Guillermo Montoya', 'assignedClaims' => rand(7, 15), 'closedClaims' => rand(1,7)],
                                        ['name' => 'Carlos Sanz', 'assignedClaims' => rand(7, 15), 'closedClaims' => rand(1,7)],
                                        ['name' => 'Enrique Espada', 'assignedClaims' => rand(7, 15), 'closedClaims' => rand(1,7)],
                                ]; ?>
                                @foreach($analysts as $analyst)
                                    <tr>
                                        <td>{!! $analyst['name'] !!}</td>
                                        <td>{!! $analyst['assignedClaims'] !!}</td>
                                        <td>{!! $analyst['closedClaims'] !!}</td>
                                        <td>{!! number_format((float)$analyst['closedClaims'] / $analyst['assignedClaims'] * 100, 2, ',','') !!}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('pageScripts')
    <script src="{!! asset('assets/js/jquery-validate/jquery.validate.min.js') !!}"></script>
    <script src="{!! asset('assets/js/inputmask/jquery.inputmask.bundle.js') !!}"></script>
    <script src="{!! asset('assets/js/formwizard/jquery.bootstrap.wizard.min.js') !!}"></script>
    <script src="{!! asset('assets/js/datepicker/bootstrap-datepicker.js') !!}"></script>
    <script src="{!! asset('assets/js/multiselect/js/jquery.multi-select.js') !!}"></script>
    <script src="{!! asset('assets/js/jquery-ui/jquery-ui.min.js') !!}"></script>
    <script src="{!! asset('assets/js/selectboxit/jquery.selectBoxIt.min.js') !!}"></script>
    <script src="{!! asset('assets/js/app-geolocation.js') !!}"></script>

    <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/moment-range/2.1.0/moment-range.min.js"></script>

    <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery/jquery-2.1.4.min.js"></script>
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/knockout/3.4.0/knockout-min.js"></script>
    <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/globalize/0.1.1/globalize.min.js"></script>
    <script type="text/javascript" src="http://cdn3.devexpress.com/jslib/15.2.4/js/dx.chartjs.js"></script>
    <script>
        function claimsByCover() {

            var dataSource = [
                {
                    analyst: 'Juan Palomo',
                    claims: Math.floor(Math.random() * (13 - 11) + 11)
                },{
                    analyst: 'Sergio Ramos',
                    claims: Math.floor(Math.random() * (11 - 9) + 9)
                },{
                    analyst: 'Raúl Cimas',
                    claims: Math.floor(Math.random() * (9 - 7) + 7)
                },{
                    analyst: 'Marina Villar',
                    claims: Math.floor(Math.random() * (7 - 5) + 5)
                },{
                    analyst: 'Rosa Quintana',
                    claims: Math.floor(Math.random() * (5 - 3) + 3)
                }
            ];

            var chartOptions = {
                dataSource: dataSource,
                series: {
                    argumentField: "analyst",
                    valueField: "claims",
                    name: "{!! trans('global.Claims by analyst') !!}",
                    type: "bar",
                    label: {
                        visible: true,
                        connector: {visible: true},
                        format: 'Number',
                        customizeText: function (e) {
                            return e.valueText;
                        }
                    },
                },
                customizePoint: function (bar) {
                    var colors = ['#08407A','#1D558F','#326AA4','#477FB9','#5C94CE'];
                    return {color: colors[bar.index]};
                },
                margin: {
                    top: 90,
                    bottom: 10,
                    left: 10,
                    right: 30
                },
                argumentAxis: {
                    valueMarginsEnabled: false,
                    visible: true,
                    label: {visible: true}
                },
                valueAxis: {
                    argumentMarginsEnabled: false,
                    visible: true,
                    grid: {
                        visible: true,
                    },
                    label: {visible: true},
                    max: 15,
                    min: 1
                },
                legend: {
                    visible: false
                },
                tooltip: {
                    enabled: true,
                    zIndex: 2000,
                    customizeTooltip: function (arg) {
                        return {
                            text: arg.argumentText
                        };
                    }
                }
            };

            $("#claims-by-cover").dxChart(chartOptions);
        }

        function affectedPolicies() {

            var dataSource = [
                {
                    status: '{!! trans('global.Verification') !!}',
                    claims: Math.floor(Math.random() * (15 - 1) + 1)
                },
                {
                    status: '{!! trans('global.Contact') !!}',
                    claims: Math.floor(Math.random() * (15 - 1) + 1)
                },
                {
                    status: '{!! trans('global.Visit') !!}',
                    claims: Math.floor(Math.random() * (15 - 1) + 1)
                },
                {
                    status: '{!! trans('global.Preliminary') !!}',
                    claims: Math.floor(Math.random() * (15 - 1) + 1)
                },
                {
                    status: '{!! trans('global.Report') !!}',
                    claims: Math.floor(Math.random() * (15 - 1) + 1)
                },
                {
                    status: '{!! trans('global.Processing') !!}',
                    claims: Math.floor(Math.random() * (15 - 1) + 1)
                },
            ];

            $("#affected-policies-category").dxPieChart({
                dataSource: dataSource,
                series: [
                    {
                        argumentField: "status",
                        valueField: "claims",
                        label:{
                            visible: true,
                            connector: {
                                visible: true,
                                width: 1
                            },
                            customizeText: function (e) {
                                return e.argumentText + ' (' + e.valueText + ')';
                            }
                        },
                        border: {
                            visible: true,
                            color: '#00306A',
                            width: 0.5
                        },
                    }
                ],
                customizePoint: function (bar) {
                    var colors = ['#08407A','#164E86','#245C93','#336BA0','#4179AD','#5088BA','#5E96C6','#6CA4D3','#7BB3E0','#89C1ED'];
                    return {color: colors[bar.index]};
                },
                legend: {
                    visible: false
                },
                margin: {
                    top: 80,
                    bottom: 10,
                }
            });
        }

        claimsByCover();
        affectedPolicies();
    </script>
@endsection