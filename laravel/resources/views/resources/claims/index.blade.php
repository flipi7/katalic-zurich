@extends('layouts.master')

@section('content')

    <div class="page-title">

        <div class="title-env">
            <h1 class="title">{!! trans('global.Claims') !!}</h1>

            <p class="description">{!! trans('global.Claims subtitle') !!}</p>
        </div>

        <div class="breadcrumb-env">
            {!! Breadcrumbs::render('claims') !!}
        </div>

    </div>

    <section class="mailbox-env">

        <div class="row">

            <div class="col-sm-10 mailbox-right">

                <div class="mail-env">
                    @if(!empty($claims))
                        <table class="table mail-table">
                            <thead>
                            <tr>
                                <th class="col-header-options">
                                    {!! trans('global.Claims') !!}
                                </th>
                                <th class="col-header-options">
                                    {!! trans('global.Status') !!}
                                </th>
                                <th class="col-header-options">
                                    {!! trans('global.Date') !!}
                                </th>
                                <th class="col-header-options">
                                    {!! trans('global.Action') !!}
                                </th>
                            </tr>
                            </thead>

                            <tfoot>
                            <tr>
                                <th colspan="4" class="col-header-options">
                                    <div class="mail-pagination">
                                        <strong>{!! $totalClaims !!}</strong> {!! trans('global.Claims') !!}
                                    </div>
                                </th>
                            </tr>
                            </tfoot>
                            @if(count($claims) > 0)
                                @foreach($claims as $claim)
                                    <?php $statusName = \App\Models\Status::getStatusName($claim->last_status); ?>
                                    <tbody>

                                    <tr>
                                        <td class="col-claim">
                                            <table class="table table-condensed claim-info">
                                                <tr>
                                                    <th rowspan="3" class="claim-star">
                                                        @if(rand(0, 1))
                                                            <i class="fa fa-star"></i>
                                                        @endif
                                                    </th>
                                                    <td>IKE #{!! $claim->id !!} - {!! $claim->policy_holder !!} - {!! strtoupper(substr($claim->hash, 0, 7)) !!}</td>
                                                </tr>
                                                <tr>
                                                    @if(!empty($claim->address->id))
                                                        <td>{!! $claim->address->address !!}, {!! $claim->address->city != null ? $claim->address->city->name . ', ' : '' !!} {!! $claim->address->state->name !!}, {!! $claim->address->country->name !!}</td>
                                                    @else
                                                        <td>{!! trans('global.No Address') !!}</td>
                                                    @endif
                                                </tr>
                                            </table>
                                        </td>
                                        <td class="col-status">
                                            <div class="table-status">
                                                <div class="table-status-icon">
                                                    @if($statusName == 'created')
                                                        <i class="fa fa-plus"></i>
                                                    @elseif($statusName == 'verification')
                                                        <i class="fa fa-search"></i>
                                                    @elseif($statusName == 'contact')
                                                        <i class="fa fa-phone"></i>
                                                    @elseif($statusName == 'visit')
                                                        <i class="fa fa-sign-in"></i>
                                                    @elseif($statusName == 'preliminary')
                                                        <i class="fa fa-mortar-board"></i>
                                                    @elseif($statusName == 'report')
                                                        <i class="fa fa-file-text-o"></i>
                                                    @elseif($statusName == 'processing')
                                                        <i class="fa fa-exchange"></i>
                                                    @elseif($statusName == 'finished')
                                                        <i class="fa fa-check-square-o"></i>
                                                    @elseif($statusName == 'cancelled')
                                                        <i class="fa fa-close"></i>
                                                    @endif
                                                </div>
                                                {!! trans('global.' . ucfirst($statusName)) !!}
                                            </div>
                                        </td>
                                        <td class="col-date">
                                            {!! $claim->created_at !!}
                                        </td>
                                        <td class="col-action">
                                            <a href="{!! route('claims.status', ['id' => $claim->id, 'status' => strtolower($statusName)]) !!}" class="btn btn-xs btn-default">
                                                <i class="fa fa-search"></i>
                                                {!! trans('global.View') !!}
                                            </a>
                                        </td>
                                    </tr>
                                    </tbody>
                                @endforeach
                            @else
                                <tbody>
                                <tr class="no-claim">
                                    <td colspan="4">
                                        <strong>{!! trans('global.No claims found') !!}</strong>
                                    </td>
                                </tr>
                                </tbody>
                            @endif
                        </table>
                    @endif
                    @if(empty($claims))
                        <h1>No claims found</h1>
                    @endif
                </div>

                <div class="row">
                    <div class="col-sm-offset-6 col-sm-6 text-right text-center-sm">
                        {!! $claims->render() !!}
                    </div>
                </div>

            </div>

            <!-- Sidebar -->
            <div class="col-sm-2 mailbox-left">

                <div class="mailbox-sidebar list-filter">

                    @if(Auth::user()->roles->first()->id == 1 || Auth::user()->roles->first()->id == 2 || Auth::user()->roles->first()->id == 6)
                    <a href="{!! route('claims.create') !!}" class="btn btn-block btn-danger btn-icon btn-icon-standalone btn-icon-standalone-right btn-add-claim">
                        <i class="fa fa-plus"></i>
                        <span>{!! trans('global.Claims') !!}</span>
                    </a>
                    @endif

                    <ul class="list-unstyled mailbox-list">
                        <li class="list-header">{!! trans('global.Search') !!}</li>
                        <li class="active">
                            @include('resources.claims.form-search')
                        </li>
                    </ul>

                    <?php $range = isset($_GET['range']) ? $_GET['range'] : null ?>
                    <?php $status = isset($_GET['status']) ? $_GET['status'] : null ?>
                    <?php $q = isset($_GET['q']) ? $_GET['q'] : null ?>

                    <ul class="list-unstyled mailbox-list">
                        <li class="list-header">{!! trans('global.Filter by date') !!}</li>
                        <li @if($range == null && $status == null && $q == null)
                                class="active"
                            @endif
                        >
                            <a href="{!! route('claims.index') !!}">
                                {!! trans('global.All') !!}
                            </a>
                        </li>
                        <li @if($range != null && $range == 'hours')
                                class="active"
                            @endif
                        >
                            <a href="{!! route('claims.index', ['range' => 'hours']) !!}">
                                {!! trans('global.24 hours') !!}
                            </a>
                        </li>
                        <li @if($range != null && $range == 'week')
                                class="active"
                            @endif
                        >
                            <a href="{!! route('claims.index', ['range' => 'week']) !!}">
                                {!! trans('global.Week') !!}
                            </a>
                        </li>
                        <li @if($range != null && $range == 'month')
                                class="active"
                            @endif
                        >
                            <a href="{!! route('claims.index', ['range' => 'month']) !!}">
                                {!! trans('global.Month') !!}
                            </a>
                        </li>
                        <li @if($range != null && $range == 'year')
                                class="active"
                            @endif
                        >
                            <a href="{!! route('claims.index', ['range' => 'year']) !!}">
                                {!! trans('global.Year') !!}
                            </a>
                        </li>
                    </ul>

                    <ul class="list-unstyled mailbox-list">
                        <li class="list-header">Filtrar por estado</li>
                    </ul>
                    <div class="list-group list-status">
                        <a href="{!! route('claims.index', ['status' => 'created']) !!}"
                           class="list-group-item
                            @if($status != null && $status == 'created')
                                active
                            @endif
                        ">
                            <div class="list-status-icon">
                                <i class="fa fa-plus"></i>
                            </div>
                            {!! trans('global.Created') !!}
                        </a>
                        <a href="{!! route('claims.index', ['status' => 'verification']) !!}"
                           class="list-group-item
                            @if($status != null && $status == 'verification')
                                   active
                               @endif
                                   ">
                            <div class="list-status-icon">
                                <i class="fa fa-search"></i>
                            </div>
                            {!! trans('global.Verification') !!}
                        </a>
                        <a href="{!! route('claims.index', ['status' => 'contact']) !!}"
                           class="list-group-item
                            @if($status != null && $status == 'contact')
                                   active
                               @endif
                                   ">
                            <div class="list-status-icon">
                                <i class="fa fa-phone"></i>
                            </div>
                            {!! trans('global.Contact') !!}
                        </a>
                        <a href="{!! route('claims.index', ['status' => 'visit']) !!}"
                           class="list-group-item
                            @if($status != null && $status == 'visit')
                                   active
                               @endif
                                   ">
                            <div class="list-status-icon">
                                <i class="fa fa-sign-in"></i>
                            </div>
                            {!! trans('global.Visit') !!}
                        </a>
                        <a href="{!! route('claims.index', ['status' => 'preliminary']) !!}"
                           class="list-group-item
                            @if($status != null && $status == 'preliminary')
                                   active
                               @endif
                                   ">
                            <div class="list-status-icon">
                                <i class="fa fa-mortar-board"></i>
                            </div>
                            {!! trans('global.Preliminary') !!}
                        </a>
                        <a href="{!! route('claims.index', ['status' => 'report']) !!}"
                           class="list-group-item
                            @if($status != null && $status == 'report')
                                   active
                               @endif
                                   ">
                            <div class="list-status-icon">
                                <i class="fa fa-file-text-o"></i>
                            </div>
                            {!! trans('global.Report') !!}
                        </a>
                        <a href="{!! route('claims.index', ['status' => 'processing']) !!}"
                           class="list-group-item
                            @if($status != null && $status == 'processing')
                                   active
                               @endif
                                   ">
                            <div class="list-status-icon">
                                <i class="fa fa-exchange"></i>
                            </div>
                            {!! trans('global.Processing') !!}
                        </a>
                        <a href="{!! route('claims.index', ['status' => 'finished']) !!}"
                           class="list-group-item
                            @if($status != null && $status == 'finished')
                                   active
                               @endif
                                   ">
                            <div class="list-status-icon">
                                <i class="fa fa-check-square-o"></i>
                            </div>
                            {!! trans('global.Finished') !!}
                        </a>
                        <a href="{!! route('claims.index', ['status' => 'cancelled']) !!}"
                           class="list-group-item
                            @if($status != null && $status == 'cancelled')
                                   active
                               @endif
                                   ">
                            <div class="list-status-icon">
                                <i class="fa fa-close"></i>
                            </div>
                            {!! trans('global.Cancelled') !!}
                        </a>
                    </div>

                </div>

            </div>

        </div>
    </section>


@endsection