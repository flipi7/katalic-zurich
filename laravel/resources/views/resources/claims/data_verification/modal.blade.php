<form role="form" method="POST" action="{!! $action !!}" class="validate" novalidate>
    {!! csrf_field() !!}
    @yield('form-data-verification')
</form>