<div class="row">

    <div class="col-md-12">
        <div class="form-group {!! $errors->first('claim.zurich_claim_number') ? 'has-error' : '' !!}">
            <strong>{!! trans('global.Zurich Claim Number') !!}</strong>
            <input
                    type="text"
                    class="form-control"
                    name="claim[data_verification][zurich_claim_number]"
                    value="{!! $claimAttributes['zurich_claim_number'] !!}"
                    @if(!$editable)
                        disabled
                    @endif
            >
        </div>
    </div>

    <div class="col-md-12">
        <div class="form-group {!! $errors->first('claim.data_verification.policy_payment') ? 'has-error' : '' !!}">
            <strong>{!! trans('global.Policy Payment') !!}</strong>

            <p>
                <label class="radio-inline">
                    <input type="radio"
                           name="claim[data_verification][policy_payment]"
                           value="yes" {!! $dataVerification['policy_payment'] == 'yes'  ? 'checked' : '' !!}
                           @if(!$editable)
                               disabled
                           @endif>
                    {!! trans('global.Yes') !!}
                </label>
                <label class="radio-inline">
                    <input type="radio"
                           name="claim[data_verification][policy_payment]"
                           value="no" {!! $dataVerification['policy_payment'] == 'no'  ? 'checked' : '' !!}
                           @if(!$editable)
                               disabled
                           @endif>
                    {!! trans('global.No') !!}
                </label>
            </p>
        </div>

        <div class="form-group {!! $errors->first('claim.data_verification.validity_start') ? 'has-error' : '' !!}">
            <strong>{!! trans('global.Validity Start') !!}</strong>

            <p>
                <input
                        type="text"
                        name="claim[data_verification][validity_start]"
                        class="form-control datepicker"
                        data-start-view="2"
                        data-format="yyyy-mm-dd"
                        value="{!! $dataVerification['validity_start'] !!}"
                        @if(!$editable)
                            disabled
                        @endif>
            </p>
        </div>

        <div class="form-group {!! $errors->first('claim.data_verification.validity_end') ? 'has-error' : '' !!}">
            <strong>{!! trans('global.Validity End') !!}</strong>

            <p>
                <input
                        type="text"
                        name="claim[data_verification][validity_end]"
                        class="form-control datepicker"
                        data-format="yyyy-mm-dd"
                        data-start-view="2"
                        value="{!! $dataVerification['validity_end'] !!}"
                        @if(!$editable)
                            disabled
                        @endif>
            </p>
        </div>

        <div class="form-group {!! $errors->first('claim.data_verification.policy_payment') ? 'has-error' : '' !!}">
            <strong>{!! trans('global.SIU') !!}</strong>

            <p>
                <label class="radio-inline">
                    <input type="radio"
                           name="claim[data_verification][siu]"
                           value="yes" {!! $dataVerification['siu'] == 'yes'  ? 'checked' : '' !!}
                           @if(!$editable)
                               disabled
                           @endif>
                    {!! trans('global.Yes') !!}
                </label>
                <label class="radio-inline">
                    <input type="radio"
                           name="claim[data_verification][siu]"
                           value="no" {!! $dataVerification['siu'] == 'no'  ? 'checked' : '' !!}
                           @if(!$editable)
                               disabled
                           @endif>
                    {!! trans('global.No') !!}
                </label>
            </p>
        </div>

        <div class="form-group {!! $errors->first('claim.data_verification.coinsurance') ? 'has-error' : '' !!}">
            <strong>{!! trans('global.Coinsurance') !!}</strong>

            <p>
                <label class="radio-inline">
                    <input type="radio"
                           name="claim[data_verification][coinsurance]"
                           value="yes" {!! $dataVerification['coinsurance'] == 'yes'  ? 'checked' : '' !!}
                           @if(!$editable)
                               disabled
                           @endif>
                    {!! trans('global.Yes') !!}
                </label>
                <label class="radio-inline">
                    <input type="radio"
                           name="claim[data_verification][coinsurance]"
                           value="no" {!! $dataVerification['coinsurance'] == 'no'  ? 'checked' : '' !!}
                           @if(!$editable)
                               disabled
                           @endif>
                    {!! trans('global.No') !!}
                </label>
            </p>

            <div id="js-coinsurance"
                 class="form-inline {!! $dataVerification['coinsurance'] == 'yes'  ? '' : 'hide' !!}">
                    <div class="form-group {!! $errors->first('claim.data_verification.coinsurance_company') ? 'has-error' : '' !!}">
                        <strong>{!! trans('global.Company') !!}</strong>
                        <p>
                            <input class="form-control"
                                   type="text"
                                   name="claim[data_verification][coinsurance_company]"
                                   value="{!! $dataVerification['coinsurance_company'] !!}"
                                   @if(!$editable)
                                       disabled
                                   @endif>
                        </p>
                    </div>

                    <div class="form-group {!! $errors->first('claim.data_verification.coinsurance_company') ? 'has-error' : '' !!}">
                        <strong>{!! trans('global.Percentage') !!}</strong>
                        <p>
                            <input
                                    class="form-control"
                                    type="text"
                                    name="claim[data_verification][coinsurance_percentage]"
                                    value="{!! $dataVerification['coinsurance_percentage'] !!}"
                                    @if(!$editable)
                                        disabled
                                    @endif>
                        </p>
                    </div>

            </div>
        </div>

        <div class="form-group {!! $errors->first('claim.data_verification.complaint') ? 'has-error' : '' !!}">
            <strong>{!! trans('global.Complaint') !!}</strong>

            <p>
                <label class="radio-inline">
                    <input type="radio"
                           name="claim[data_verification][complaint]"
                           value="yes" {!! $dataVerification['complaint'] == 'yes'  ? 'checked' : '' !!}
                           @if(!$editable)
                               disabled
                           @endif>
                    {!! trans('global.Yes') !!}
                </label>
                <label class="radio-inline">
                    <input type="radio"
                           name="claim[data_verification][complaint]"
                           value="no" {!! $dataVerification['complaint'] == 'no'  ? 'checked' : '' !!}
                           @if(!$editable)
                               disabled
                           @endif>
                    {!! trans('global.No') !!}
                </label>
            </p>
        </div>

        <div class="form-group {!! $errors->first('claim.data_verification.analyst_id') ? 'has-error' : '' !!}">
            <strong>{!! trans('global.Analyst') !!}</strong>
            <select class="form-control" name="claim[data_verification][analyst_id]"
                    data-validate="required" {!! !empty($disabled) || !$editable ? 'disabled' : '' !!}>
                <option value="">{!! trans('global.Select analyst') !!}</option>
                @foreach($analysts as $analyst)
                    <option value="{!! $analyst->id !!}" {!! $dataVerification['analyst_id'] == $analyst->id  ? 'selected' : '' !!}>{!! $analyst->name !!}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <strong>{!! trans('global.Is claim cover correct?') !!}</strong>

            <p>
                <label class="radio-inline">
                    <input type="radio" name="claim[data_verification][is_cover_correct]" value="yes" {!! $claimAttributes['is_cover_correct'] == 'yes' ? 'checked' : '' !!}
                    @if(!$editable)
                        disabled
                    @endif>
                    {!! trans('global.Yes') !!}
                </label>
                <label class="radio-inline">
                    <input type="radio" name="claim[data_verification][is_cover_correct]" value="no" {!! $claimAttributes['is_cover_correct'] == 'no' ? 'checked' : '' !!}
                    @if(!$editable)
                        disabled
                    @endif>
                    {!! trans('global.No') !!}
                </label>
            </p>
        </div>

        <div class="form-group hidden" id="claim-cover">
            <label>{!! trans('global.Cover') !!}</label>
            <select class="form-control multi-select" multiple="multiple"
                    name="claim[data_verification][survey]" {!! !empty($disabled) ? 'disabled' : '' !!}>
                @foreach($surveys as $survey)
                    <option value="{!! $survey->id !!}" {!! $claim->surveys->contains($survey->id) ? 'selected' : 'no' !!}>{!! $survey->name !!}</option>
                @endforeach
            </select>

        </div>

        @if($editable)
            <div class="form-group">
                <button type="submit" class="btn btn-primary">{!! trans('global.Save') !!}</button>
            </div>
        @endif
    </div>
</div>

<script>
    $(function () {
        $('input[name="claim[data_verification][coinsurance]"]').click(function () {
            if (this.value === 'yes') {
                $('#js-coinsurance').removeClass('hide');
            } else {
                $('#js-coinsurance').addClass('hide');
            }
        });
        $('input[name="claim[data_verification][is_cover_correct]"]').click(function () {
            if (this.value === 'no') {
                $('#claim-cover').removeClass('hidden');
            } else {
                $('#claim-cover').addClass('hidden');
            }
        });
    });
    jQuery(document).ready(function ($) {
        $(".multi-select").multiSelect({
            afterInit: function () {
                // Add alternative scrollbar to list
                this.$selectableContainer.add(this.$selectionContainer).find('.ms-list').perfectScrollbar();
            },
            afterSelect: function () {
                // Update scrollbar size
                this.$selectableContainer.add(this.$selectionContainer).find('.ms-list').perfectScrollbar('update');
            }
        });
    });
</script>