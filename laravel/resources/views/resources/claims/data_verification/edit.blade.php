@extends('resources.claims.data_verification.modal', [
    'action' => route('claims.dataVerification.update', [$claim, $claim->dataVerification])
])

@section('form-data-verification')
    {!! method_field('PUT') !!}
    @include('resources.claims.data_verification.form', [
        'dataVerification' => old('claim.data_verification') ? old('claim.data_verification') : $claim->dataVerification->toArray()
    ])
@endsection