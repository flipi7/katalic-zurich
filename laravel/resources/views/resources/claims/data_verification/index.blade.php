<div class="panel panel-default">
    <div class="panel-heading">
        {!! trans('global.Claim Verification') !!}

        @if($editable && $claim->canBeVerified())
            <div class="panel-options">
                <form method="POST" action="{!! route('claims.statuses.store', $claim) !!}" style="display:inline-block">
                    {!! csrf_field() !!}
                    <input type="hidden" name="status_id" value="{!! \App\Models\Status::CONTACT !!}">
                    <button class="btn btn-single btn-success {!! $claim->isDataVerificationComplete() ? '' : 'disabled' !!}">{!! trans('global.Approve Claim') !!}</button>
                </form>
                <button class="btn btn-single btn-danger"
                        data-toggle="modal"
                        data-target="#modal-claims-cancel">{!! trans('global.Cancel Claim') !!}
                </button>
            </div>
        @endif

    </div>
    <div class="panel-body">
        @if(!empty($claim->dataVerification->id))
            @include('resources.claims.data_verification.edit', [
                'claimAttributes' => [
                    'zurich_claim_number' => $claim->zurich_claim_number != null ? $claim->zurich_claim_number : '' ,
                    'is_cover_correct' => $claim->is_cover_correct != null ? $claim->is_cover_correct : ''
                ]
            ])
        @else
            @include('resources.claims.data_verification.create', [
                'claimAttributes' => [
                    'zurich_claim_number' => '',
                    'is_cover_correct' => null
                ]
            ])
        @endif
    </div>
    @include('resources.claims.data_verification.modal-claim-cancel')
</div>