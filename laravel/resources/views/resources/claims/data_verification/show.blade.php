@if(!empty($claim->dataIke->id))
    @include('resources.claims.data_ike.form', [
        'disabled' => true,
        'ike' => $claim->dataIke->toArray()
    ])
@else
    <div class="alert alert-danger">{!! trans('global.No data provided') !!}</div>
@endif