@extends('resources.claims.data_verification.modal', [
    'action' => route('claims.dataVerification.store', [$claim])
])

@section('form-data-verification')
    @include('resources.claims.data_verification.form', [
        'dataVerification' => [
            'policy_payment' => old('claim.data_verification.policy_payment', ''),
            'validity_start' => old('claim.data_verification.validity_start', ''),
            'validity_end' => old('claim.data_verification.validity_end', ''),
            'siu' => old('claim.data_verification.siu', ''),
            'coinsurance' => old('claim.data_verification.coinsurance', ''),
            'complaint' => old('claim.data_verification.complaint', ''),
            'analyst_id' => old('claim.data_verification.analyst_id', ''),
            'coinsurance_company' => old('claim.data_verification.coinsurance_company', ''),
            'coinsurance_percentage' => old('claim.data_verification.coinsurance_percentage', ''),
        ]
    ])
@endsection