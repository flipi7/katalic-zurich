<div class="modal fade" id="modal-claims-cancel">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">{!! trans('global.Are you sure?') !!}</h4>
            </div>

            <div class="modal-body">
                {!! trans('global.Are you sure you want to cancel this claim?') !!}
            </div>

            <div class="modal-footer">
                <form action="{!! route('claims.statuses.store', $claim) !!}" method="POST">
                    {!! csrf_field() !!}
                    <input  type="hidden" name="status_id" value="{!! \App\Models\Status::CANCELLED !!}">
                    <button type="button" class="btn btn-white" data-dismiss="modal">
                        {!! trans('global.Close') !!}
                    </button>
                    <button type="submit" class="btn btn-danger">
                        {!! trans('global.Cancel Claim') !!}
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>