@extends('resources.claims.data_basic.modal', [
    'action' => route('claims.store', [$claim])
])

@section('modal-claim-basic-body')
    @include('resources.claims.data_basic.form', [
        'basic' => old('claim') ? old('claim') : [
            'policy_number' => '',
            'policy_holder' => '',
            'occurred_at' => '',
            'zurich_claim_number' => '',
        ]
    ])
@endsection