<div class="form-group {!! $errors->first('claim.policy_number') ? 'has-error' : '' !!}">
    <label>{!! trans('global.Policy Number') !!}</label>
    <input
            type="text"
            class="form-control"
            name="claim[policy_number]"
            value="{!! $basic['policy_number'] !!}"
            {!! !empty($disabled) ? 'disabled' : '' !!}
            >
</div>

<div class="form-group {!! $errors->first('claim.policy_holder') ? 'has-error' : '' !!}">
    <label>{!! trans('global.Policy Holder') !!}</label>
    <input type="text"
           class="form-control"
           name="claim[policy_holder]"
           value="{!! $basic['policy_holder'] !!}"
            {!! !empty($disabled) ? 'disabled' : '' !!}
            >
</div>

<div class="form-group {!! $errors->first('claim.occurred') ? 'has-error' : '' !!}">
    <label class="control-label" for="about">{!! trans('global.Claim date occurrence') !!}</label>
    <input class="form-control" name="claim[occurred_at]" data-validate="required" data-mask="datetime"
           placeholder="{!! trans('global.Claim date occurrence') !!}" aria-required="true"
           aria-invalid="false"
           value="{!! $basic['occurred_at'] !!}"
            {!! !empty($disabled) ? 'disabled' : '' !!}
            >
</div>

<?php $statusId = \App\Models\Status::getStatusId($claim->last_status); ?>
@if($statusId >= \App\Models\Status::VERIFICATION)
    <div class="form-group {!! $errors->first('claim.zurich_claim_number') ? 'has-error' : '' !!}">
        <label>{!! trans('global.Zurich Claim Number') !!}</label>
        <input
                type="text"
                class="form-control"
                name="claim[zurich_claim_number]"
                value="{!! $basic['zurich_claim_number'] !!}"
                disabled
                >
    </div>
@endif