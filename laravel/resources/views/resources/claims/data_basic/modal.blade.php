<div class="modal {!! old('modal_old') == 'modal-claims-update-info' ? 'modal_old' : '' !!}" id="modal-claims-update-info">
    <div class="modal-dialog">
        <div class="modal-content">

            <form role="form" method="POST" action="{!! $action !!}" class="validate" novalidate>
                {!! csrf_field() !!}
                <input type="hidden" name="modal_old" value="modal-claims-update-info">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{!! trans('global.Edit Claim Information') !!}</h4>
                </div>

                <div class="modal-body">
                    @yield('modal-claim-basic-body')
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">
                        {!! trans('global.Close') !!}
                    </button>
                    <button type="submit" class="btn btn-success">
                        {!! trans('global.Save') !!}
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>