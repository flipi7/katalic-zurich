@extends('resources.claims.data_basic.modal', [
    'action' => route('claims.update-info', [$claim])
])

@section('modal-claim-basic-body')
    @include('resources.claims.data_basic.form', [
        'basic' => [
            'policy_number' => old('claim.policy_number', $claim->policy_number),
            'policy_holder' => old('claim.policy_holder', $claim->policy_holder),
            'zurich_claim_number' => old('claim.policy_number', $claim->zurich_claim_number),
            'occurred_at' => old('claim.occurred_at', $claim->occurred_at->format('d/m/Y H:i')),
        ]
    ])
@endsection