<div class="panel panel-default">
    <div class="panel-heading">
        {!! trans('global.Claim Information') !!}

        @if($editable)
            <div class="panel-options">
                <a class="btn btn-single btn-gray" data-toggle="modal" data-target="#modal-claims-update-info">Edit</a>
            </div>
        @endif
    </div>

    <div class="panel-body">
        <!-- Show -->
        @include('resources.claims.data_basic.show')

        <!-- Modal -->
        @include('resources.claims.data_basic.edit')
    </div>
</div>