@if(!empty($claim->id))
    @include('resources.claims.data_basic.form', [
        'disabled' => true,
        'basic' => [
            'policy_number' => $claim->policy_number,
            'policy_holder' => $claim->policy_holder,
            'zurich_claim_number' => $claim->zurich_claim_number,
            'occurred_at' => $claim->occurred_at->format('d/m/Y H:i'),
        ]
    ])
@else
    <div class="alert alert-danger">{!! trans('global.No data provided.') !!}</div>
@endif