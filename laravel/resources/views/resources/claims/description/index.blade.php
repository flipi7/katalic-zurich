<div class="panel panel-default">
    <div class="panel-heading">
        {!! trans('global.Claim Description') !!}
        @if($editable)
            <div class="panel-options">
                <a class="btn btn-single btn-gray" data-toggle="modal" data-target="#modal-claims-description">Edit</a>
            </div>
        @endif
    </div>

    <div class="panel-body">

        <!-- Show -->
        @include('resources.claims.description.show')


        <!-- Modal -->
        @include('resources.claims.description.edit')

    </div>
</div>