@extends('resources.claims.description.modal', [
    'action' => route('claims.update-description', [$claim])
])

@section('modal-description-body')
    @include('resources.claims.description.form', [
        'claimDescription' => [
            'description' => old('claim.description', $claim->description),
            'surveys' => old('claim.surveys') ? collect(old('claim.surveys')) : $claim->surveys,
            'is_cover_correct' => old('claim.is_cover_correct', $claim->is_cover_correct),
            'amount_loss' => old('claim.amount_loss', $claim->amount_loss),
            'currency' => old('claim.currency', $claim->currency),
        ]
    ])
@endsection