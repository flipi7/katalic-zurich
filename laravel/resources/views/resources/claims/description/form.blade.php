<div class="form-group">
    <label>{!! trans('global.Description') !!}</label>
    <textarea name="claim[description]"
              rows="3"
              class="form-control" {!! !empty($disabled) ? 'disabled' : '' !!}>{!! $claimDescription['description'] !!}</textarea>
</div>

<div class="form-group">
    <label>{!! trans('global.Cover') !!}</label>
    <select class="form-control multi-select" multiple="multiple"
            name="claim[surveys][]" {!! !empty($disabled) ? 'disabled' : '' !!}>
        @foreach($surveys as $survey)
            <option value="{!! $survey->id !!}" {!! $claimDescription['surveys']->contains($survey->id) ? 'selected' : 'no' !!}>{!! $survey->name !!}</option>
        @endforeach
    </select>

</div>
@if($claim->hasStatus(\App\Models\Status::VERIFICATION))
    <div class="form-group">
        <strong>{!! trans('global.Is claim cover correct?') !!}</strong>

        <p>
            <label class="radio-inline">
                <input type="radio" name="claim[is_cover_correct]" value="yes" {!! $claimDescription['is_cover_correct'] == 'yes' ? 'checked' : '' !!} disabled>
                {!! trans('global.Yes') !!}
            </label>
            <label class="radio-inline">
                <input type="radio" name="claim[is_cover_correct]" value="no" {!! $claimDescription['is_cover_correct'] == 'no' ? 'checked' : '' !!} disabled>
                {!! trans('global.No') !!}
            </label>
        </p>
    </div>
@endif


<div class="row">
    <div class="col-md-8">
        <div class="form-group {!! $errors->first('claim.amount_loss') ? 'has-error' : '' !!}">
            <label>{!! trans('global.Amount Loss (create)') !!}</label>
            <input class="form-control" name="claim[amount_loss]" data-validate="required"
                   placeholder="{!! trans('global.Amount Loss (create)') !!}"
                   value="{!! $claimDescription['amount_loss'] !!}"
                    {!! !empty($disabled) ? 'disabled' : '' !!}
                    />
        </div>
    </div>

    <div class="col-md-4">
        <div class="form-group {!! $errors->first('claim.currency') ? 'has-error' : '' !!}">
            <label>{!! trans('global.Currency') !!}</label>
            <select class="form-control" name="claim[currency]" data-validate="required" {!! !empty($disabled) ? 'disabled' : '' !!}>
                <option value=""></option>
                <option value="mxn" {!! $claimDescription['currency'] == 'mxn' ? 'selected' : '' !!}>$ MXN</option>
                <option value="usd" {!! $claimDescription['currency'] == 'usd' ? 'selected' : '' !!}>$ USD</option>
            </select>
        </div>
    </div>

</div>



<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $(".multi-select").multiSelect({
            afterInit: function () {
                // Add alternative scrollbar to list
                this.$selectableContainer.add(this.$selectionContainer).find('.ms-list').perfectScrollbar();
            },
            afterSelect: function () {
                // Update scrollbar size
                this.$selectableContainer.add(this.$selectionContainer).find('.ms-list').perfectScrollbar('update');
            }
        });
    });
</script>