@include('resources.claims.description.form', [
    'disabled' => true,
    'claimDescription' => [
        'description' => $claim->description,
        'surveys' => $claim->surveys,
        'amount_loss' => $claim->amount_loss,
        'currency' => $claim->currency,
        'is_cover_correct' => $claim->is_cover_correct,
    ]
])