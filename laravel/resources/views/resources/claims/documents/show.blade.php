<table class="table table-hover members-table middle-align documents-table">
    <thead>
    <tr>
        <th>{!! trans('global.Description') !!}</th>
        <th>{!! trans('global.Name') !!}</th>
        <th>{!! trans('global.Date') !!}</th>
        <th>{!! trans('global.Added by') !!}</th>
        <th>{!! trans('global.Options') !!}</th>
    </tr>
    </thead>
    <tbody>
    @if($claim->documents->isEmpty())
        <tr>
            <td colspan="4">
                <strong>{!! trans('global.No Documents') !!}</strong>.
            </td>
        </tr>
    @else
        @foreach($claim->documents as $document)
            @if(!isset($public))
            <tr>
                <td>
                    {!! $document->description !!}
                </td>
                <td>
                    {!! $document->attachmentName !!}
                </td>
                <td>
                    {!! $document->date !!}
                </td>
                <td>
                    @if($document->user != null)
                        {!! $document->user->name !!}
                    @else
                        {!! trans('global.Insured') !!}
                    @endif
                </td>
                <td>
                    @if($document->isDocument())
                    <a class="btn btn-xs btn-default" href="{!! '/ViewerJS/#../static/' . $document->attachmentHash . '-' . $document->attachmentName !!}" target="_blank">
                        <i class="fa fa-search"></i>
                        {!! trans('global.View') !!}
                    </a>
                    @endif
                    <a class="btn btn-xs btn-default" href="{!! '/static/' . $document->attachmentHash . '-' . $document->attachmentName !!}" download target="_blank">
                        <i class="fa fa-download"></i>
                        {!! trans('global.Download') !!}
                    </a>
                    <a class="btn btn-xs btn-default" href="#" data-toggle="modal"
                       data-target="#modal-claims-documents-{!! $document->id !!}">
                        <i class="fa fa-pencil"></i>
                        {!! trans('global.Edit') !!}
                    </a>
                    @include('resources.claims.documents.edit')
                </td>
            </tr>
            @else
                @if($document->user == null)
                    <tr>
                        <td>
                            {!! $document->description !!}
                        </td>
                        <td>
                            {!! $document->attachmentName !!}
                        </td>
                        <td>
                            {!! $document->date !!}
                        </td>
                        <td>
                            {!! trans('global.Insured') !!}
                        </td>
                        <td>
                            @if($document->isDocument())
                                <a class="btn btn-xs btn-default" href="{!! '/ViewerJS/#../static/' . $document->attachmentHash . '-' . $document->attachmentName !!}" target="_blank">
                                    <i class="fa fa-search"></i>
                                    {!! trans('global.View') !!}
                                </a>
                            @endif
                            <a class="btn btn-xs btn-default" href="{!! '/static/' . $document->attachmentHash . '-' . $document->attachmentName !!}" download target="_blank">
                                <i class="fa fa-download"></i>
                                {!! trans('global.Download') !!}
                            </a>
                            <a class="btn btn-xs btn-default" href="#" data-toggle="modal"
                               data-target="#modal-claims-documents-{!! $document->id !!}">
                                <i class="fa fa-pencil"></i>
                                {!! trans('global.Edit') !!}
                            </a>
                            @include('resources.claims.documents.edit')
                        </td>
                    </tr>
                @endif
            @endif
        @endforeach
    @endif
    </tbody>
</table>
