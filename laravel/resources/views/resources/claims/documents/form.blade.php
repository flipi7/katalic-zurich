<div class="row">

    <div class="col-md-12">
        <div class="form-group {!! $errors->{$bagName}->first('description') ? 'has-error' : '' !!}">
            <label class="control-label" for="about">{!! trans('global.Document') !!}</label>
            <input class="form-control" name="description" data-validate="required"
                      placeholder="{!! trans('global.Description') !!}" value="{!! $description !!}" />
        </div>
    </div>

    <div class="col-md-12">
        <div class="form-group document {!! $errors->{$bagName}->first('attachmentName') ? 'has-error' : '' !!}">
            <label class="control-label" for="about">{!! trans('global.Attachment') !!}</label>
            @if($hasAttachment)
            <div class="current-attachment">
                <i class="fa fa-file"></i> <span>{!! $attachmentName !!}</span>
                <div class="row">
                    <div class="col-md-12">
                        <a class="btn btn-xs btn-default" href="{!! '/ViewerJS/#../static/' . $document->attachmentHash . '-' . $document->attachmentName !!}" target="_blank">
                            <i class="fa fa-search"></i>
                            {!! trans('global.View') !!}
                        </a>
                        <a class="btn btn-xs btn-default" href="{!! '/static/' . $document->attachmentHash . '-' . $document->attachmentName !!}" download target="_blank">
                            <i class="fa fa-download"></i>
                            {!! trans('global.Download') !!}
                        </a>
                        <a class="btn btn-xs btn-default remove-attachment" href="javascript:;">
                            <i class="fa fa-times"></i>
                            {!! trans('global.Delete') !!}
                        </a>
                        <input id="document-attachment" class="hidden" type="text" name="attachment" value="{!! $attachmentName !!}">
                    </div>
                </div>
            </div>
            @endif
            <input id="document-attachment" class="@if($hasAttachment) hidden @endif" type="file" name="attachment" data-validate="required">
        </div>
    </div>

</div>