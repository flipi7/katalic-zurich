@extends('resources.claims.documents.modal', [
    'action' => route('claims.documents.store', $claim),
    'section' => 'modal-body-documents-create'
])

@section('modal-body-documents-create')
    @include('resources.claims.documents.form', [
        'bagName' => 'claim_documents',
        'description' => old('description'),
        'hasAttachment' => !empty(old('attachmentName')),
        'attachmentName' => old('attachmentName')
    ])
@endsection