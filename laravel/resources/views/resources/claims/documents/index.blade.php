<div class="panel panel-default">
    <div class="panel-heading">
        {!! trans('global.Documents') !!}
        <div class="panel-options">
            <a class="btn btn-single btn-gray" href="javascript:" data-toggle="modal"
               data-target="#modal-claims-documents">
                {!! trans('global.Add New Document') !!}
            </a>
        </div>
    </div>

    <div class="panel-body">
        <!-- Show -->
        @include('resources.claims.documents.show')

         <!-- Modal -->
        @include('resources.claims.documents.create')
    </div>
</div>