@extends('resources.claims.documents.modal', [
    'modalId' => 'modal-claims-documents-' . $document->id,
    'action' => route('claims.documents.update', [$claim, $document]),
    'section' => 'modal-body-documents-edit-' . $document->id
])

@section('modal-body-documents-edit-' . $document->id)
    {!! method_field('PUT') !!}
    @include('resources.claims.documents.form', [
        'bagName' => 'claim_documents',
        'description' => old('description', $document->description),
        'hasAttachment' => !empty(old('attachmentName', $document->attachmentName)),
        'attachmentName' => old('attachmentName', $document->attachmentName)
    ])
@endsection