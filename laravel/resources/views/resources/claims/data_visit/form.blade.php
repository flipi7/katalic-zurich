<div class="row">

    <div class="col-md-12">
        <div class="form-group {!! $errors->first('claim.visit.date') ? 'has-error' : '' !!}">
            <label class="control-label" for="about">{!! trans('global.Date') !!}</label>
            <input class="form-control" name="claim[visit][date]" data-validate="required" data-mask="datetime"
                   placeholder="dd/mm/yyyy hh:mm" aria-required="true"
                   aria-invalid="false" value="{!! $contactDate !!}" />
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group {!! $errors->first('claim.visit.description') ? 'has-error' : '' !!}">
            <label class="control-label" for="about">{!! trans('global.Description') !!}</label>
            <textarea rows="3" class="form-control autogrow" name="claim[visit][description]" data-validate="required"
                      placeholder="{!! trans('global.Description') !!}">{!! $claimVisit['description'] !!}</textarea>
        </div>
    </div>

</div>
