<table class="table table-hover members-table middle-align">
    <thead>
    <tr>
        <th>{!! trans('global.Name') !!}</th>
        <th>{!! trans('global.Date') !!}</th>
        <th>{!! trans('global.Description') !!}</th>
{{--        <th>{!! trans('global.Options') !!}</th>--}}
    </tr>
    </thead>
    <tbody>
    @if($claim->dataVisits->isEmpty())
        <tr>
            <td colspan="5">
                <strong>{!! trans('global.No visits') !!}</strong>. {!! trans('global.Please create new visit') !!}.
            </td>
        </tr>
    @else
        @foreach($claim->dataVisits as $dataVisit)
            <tr>
                <td>
                    {!! $dataVisit->user->name !!}
                </td>
                <td>
                    {!! $dataVisit->date !!}
                </td>
                <td>
                    {!! $dataVisit->description !!}
                </td>
                {{--<td>--}}
                    {{--<a class="btn btn-xs btn-default" href="#" data-toggle="modal"--}}
                       {{--data-target="#modal-claims-visits-{!! $dataVisit->id !!}">--}}
                        {{--<i class="fa fa-pencil"></i>--}}
                        {{--{!! trans('global.Edit') !!}--}}
                    {{--</a>--}}
                    {{--<a href="javascript:" class="delete" data-toggle="modal" data-target="#modal-delete-dataVisit-{!! $dataVisit->id !!}">--}}
                    {{--<i class="linecons-trash"></i>--}}
                    {{--{!! trans('global.Delete') !!}--}}
                    {{--</a>--}}
                    {{--@include('modals.alert-delete', ['modalId' => 'delete-dataVisit-' . $dataVisit->id, 'modalAction' => ''])--}}
                    {{--@include('resources.claims.data_visit.edit')--}}
                {{--</td>--}}
            </tr>
        @endforeach
    @endif
    </tbody>
</table>
