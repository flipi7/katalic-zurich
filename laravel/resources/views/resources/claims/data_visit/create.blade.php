@extends('resources.claims.data_visit.modal', [
    'action' => route('claims.dataVisits.store', $claim),
])

@section('modal-visit-body')
    @include('resources.claims.data_visit.form', [
        'contactDate' => \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $claim->dataContact()->orderBy('created_at', 'DESC')->first()->visit_date)->format('d/m/Y H:i'),
        'claimVisit' => old('claim.visit') ? old('claim.visit') : ['date' => '', 'description' => '']
    ])
@endsection

