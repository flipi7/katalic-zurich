@extends('resources.claims.data_visit.modal', [
    'action' => route('claims.dataVisits.update', [$claim, $dataVisit]),
])

@section('modal-body-data_visit-edit-' . $dataVisit->id)
    {!! method_field('PUT') !!}
    @include('resources.claims.data_visit.form', [
        'claimVisit' => old('claim.visit') ? old('claim.visit') : ['date' => '', 'description' => '']
    ])
@endsection
