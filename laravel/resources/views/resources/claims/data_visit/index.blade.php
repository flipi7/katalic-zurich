<div class="panel panel-default">
    <div class="panel-heading">
        {!! trans('global.Visits') !!}
        @if($editable)
        <div class="panel-options">
            <a class="btn btn-single btn-gray" href="javascript:" data-toggle="modal"
               data-target="#modal-claims-visit">
                {!! trans('global.Add new visit') !!}
            </a>
        </div>
        @endif
    </div>

    <div class="panel-body">
        <!-- Show -->
        @include('resources.claims.data_visit.visits-list')

                <!-- Modal -->
        @include('resources.claims.data_visit.create')
    </div>
</div>
