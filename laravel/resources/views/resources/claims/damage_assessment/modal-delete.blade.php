<div class="modal fade" id="modal-damage-assessment-delete-{!! $damageAssessment->id !!}">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">{!! trans('global.Delete') !!} {!! trans('global.Damage Assessment') !!}</h4>
            </div>

            <div class="modal-body">
                <p>{!! trans('global.Are you sure you want to delete this damage assessment?') !!}</p>
            </div>

            <div class="modal-footer">
                <form method="POST" action="{!! route('claims.damageAssessment.destroy', [$claim, $damageAssessment]) !!}">
                    {!! csrf_field() !!}
                    {!! method_field('DELETE') !!}
                    <button type="button" class="btn btn-white" data-dismiss="modal">
                        {!! trans('global.Close') !!}
                    </button>
                    <button type="submit" class="btn btn-danger">
                        {!! trans('global.Delete') !!}
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>