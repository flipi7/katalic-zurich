<div class="panel panel-default">
    <div class="panel-heading">
        {!! trans('global.Damage Assessment') !!}
        @if($editable)
            <div class="panel-options">
                <a class="btn btn-single btn-gray" data-toggle="modal" data-target="#modal-damage-assessment-create">{!! trans('global.Create') !!}</a>
            </div>
        @endif
    </div>

    <div class="panel-body">

        <!-- Show -->
        <table class="table responsive">
            <thead>
            <tr>
                <th>{!! trans('global.Cover') !!}</th>
                <th>{!! trans('global.Adjustment Description') !!}</th>
                <th>{!! trans('global.Amount Loss (damage)') !!}</th>
                <th>{!! trans('global.Deductible') !!}</th>
                <th>{!! trans('global.Total') !!}</th>
                <th>{!! trans('global.Options') !!}</th>
            </tr>
            </thead>
            <tbody>
            @if(!$claim->damageAssessments->isEmpty())
                @foreach($claim->damageAssessments as $damageAssessment)
                    <tr>
                        <td>{!! $claim->surveys()->first()['name'] !!}</td>
                        <td>{!! $damageAssessment->adjustment_description !!}</td>
                        <td>{!! number_format($damageAssessment->amount_loss, 2, ',', '.') !!}</td>
                        <td>{!! number_format($damageAssessment->deductible, 2, ',', '.')!!}</td>
                        <td>{!! number_format(($damageAssessment->amount_loss - $damageAssessment->deductible), 2, ',', '.') !!}</td>
                        <td>
                            <a class="btn btn-xs btn-default" href="#" data-toggle="modal"
                               data-target="#modal-damage-assessment-{!! $damageAssessment->id !!}">
                                <i class="fa fa-pencil"></i>
                                {!! trans('global.Edit') !!}
                            </a>
                            <a class="btn btn-xs btn-default" href="#" data-toggle="modal"
                               data-target="#modal-damage-assessment-delete-{!! $damageAssessment->id !!}">
                                <i class="fa fa-remove"></i>
                                {!! trans('global.Delete') !!}
                            </a>
                            @include('resources.claims.damage_assessment.edit')
                            @include('resources.claims.damage_assessment.modal-delete')
                        </td>
                    </tr>
                @endforeach
                <tr style="background-color: #f9f9f9;">
                    <td></td>
                    <td><strong>Total</strong></td>
                    <td>{!! number_format($claim->damageAssessmentsLoss(), 2, ',', '.') !!}</td>
                    <td>{!! number_format($claim->damageAssessmentsDeductible(), 2, ',', '.')!!}</td>
                    <td>{!! number_format(($claim->damageAssessmentsLoss() - $claim->damageAssessmentsDeductible()), 2, ',', '.') !!}</td>
                    <td></td>
                </tr>
            @else
                <tr>
                    <td colspan="6">{!! trans('global.No damage assessment') !!}</td>
                </tr>
            @endif
            </tbody>
        </table>

        <!-- Modal -->
        @include('resources.claims.damage_assessment.create')
    </div>
</div>