<div class="row">

    <div class="col-md-6">
        <div class="form-group">
            <label><strong>{!! trans('global.Cover') !!}</strong></label>
            <input
                    type="text"
                    class="form-control"
                    value="{!! $damageAssessment['cover'] !!}"
                    disabled
                    />
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group {!! $errors->first('claim.damage_assessment.adjustment_description') ? 'has-error' : '' !!}">
            <label class="control-label" for="street">
                {!! trans('global.Adjustment Description') !!}
            </label>
            <textarea class="form-control"
                      name="claim[damage_assessment][adjustment_description]"
                      data-validate="required"
                      placeholder="{!! trans('global.Adjustment Description') !!}"
                      @if(!$editable)
                          disabled
                      @endif
            >{!! $damageAssessment['adjustment_description'] !!}</textarea>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group {!! $errors->first('claim.damage_assessment.amount_loss') ? 'has-error' : '' !!}">
            <label><strong>{!! trans('global.Amount Loss (damage)') !!}</strong></label>
            <input
                    type="text"
                    class="form-control total-updater amount-loss"
                    name="claim[damage_assessment][amount_loss]"
                    data-validate="required"
                    value="{!! $damageAssessment['amount_loss'] !!}"
                    @if(!$editable)
                    disabled
                    @endif />
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group {!! $errors->first('claim.damage_assessment.deductible') ? 'has-error' : '' !!}">
            <label><strong>{!! trans('global.Deductible') !!}</strong></label>
            <input
                    type="text"
                    class="form-control total-updater deductible"
                    name="claim[damage_assessment][deductible]"
                    data-validate="required"
                    value="{!! $damageAssessment['deductible'] !!}"
                    @if(!$editable)
                    disabled
                    @endif />
        </div>
    </div>
</div>
