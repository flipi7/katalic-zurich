@extends('resources.claims.damage_assessment.modal', [
    'modalId' => 'modal-damage-assessment-' . $damageAssessment->id,
    'action' => route('claims.damageAssessment.update', [$claim, $damageAssessment]),
    'section' => 'modal-damage-assessment-edit-' . $damageAssessment->id
])

@section('modal-damage-assessment-edit-' . $damageAssessment->id)
    {!! method_field('PUT') !!}
    @include('resources.claims.damage_assessment.form', [
        'action' => route('claims.damageAssessment.update', [$claim]),
        'damageAssessment' => old('claim.damage_assessment') ? old('claim.damage_assessment') : array_merge($damageAssessment->toArray(), ['cover' => $claim->surveys()->first()['name']])
    ])
@endsection
