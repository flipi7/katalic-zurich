@extends('resources.claims.damage_assessment.modal', [
    'action' => route('claims.damageAssessment.store', [$claim]),
    'section' => 'modal-damage-assessment-create'
])

@section('modal-damage-assessment-create')
    @include('resources.claims.damage_assessment.form', [
        'damageAssessment' => [
            'cover' => $claim->surveys()->first()['name'],
            'amount_loss' => old('claim.data_report.amount_loss', ''),
            'deductible' => old('claim.data_report.deductible', ''),
            'adjustment_description' => old('claim.data_report.adjustment_description', ''),
        ]
    ])
@endsection