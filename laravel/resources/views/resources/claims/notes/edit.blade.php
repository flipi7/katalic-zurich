@extends('resources.claims.notes.modal', [
    'modalId' => 'modal-claims-notes-' . $note->id,
    'action' => route('claims.notes.update', [$claim, $note]),
    'section' => 'modal-body-notes-edit-' . $note->id
])

@section('modal-body-notes-edit-' . $note->id)
    {!! method_field('PUT') !!}
    @include('resources.claims.notes.form', [
        'bagName' => 'claim_notes',
        'note' => $note,
        'visibility' => old('visibility', $note->visibilities()->getResults()),
        'description' => old('description', $note->description),
        'hasAttachment' => !empty(old('attachmentName', $note->attachmentName)),
        'attachmentName' => old('attachmentName', $note->attachmentName)
    ])
@endsection