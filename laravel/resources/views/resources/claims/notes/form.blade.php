<div class="row">

    <div class="col-md-12">
        <div class="form-group {!! $errors->{$bagName}->first('description') ? 'has-error' : '' !!}">
            <label class="control-label" for="about">{!! trans('global.Note') !!}</label>
            <textarea rows="3" class="form-control autogrow" name="description" data-validate="required"
                      placeholder="{!! trans('global.Note') !!}">{!! $description !!}</textarea>
        </div>
    </div>

    <div class="col-md-12">
        <div class="form-group {!! $errors->{$bagName}->first('attachmentName') ? 'has-error' : '' !!}">
            <label class="control-label" for="about">{!! trans('global.Attachment') !!}</label>
            @if($hasAttachment)
                <div class="current-attachment">
                    <i class="fa fa-file"></i> <span>{!! $attachmentName !!}</span>
                    <div class="row">
                        <div class="col-md-12">
                            <a class="btn btn-xs btn-default" href="{!! '/static/' . $note->attachmentHash . '-' . $note->attachmentName !!}" download target="_blank">
                                <i class="fa fa-download"></i>
                                {!! trans('global.Download') !!}
                            </a>
                            <a class="btn btn-xs btn-default remove-attachment" href="javascript:;">
                                <i class="fa fa-times"></i>
                                {!! trans('global.Delete') !!}
                            </a>
                            <input id="note-attachment" class="hidden" type="text" name="attachment" value="{!! $attachmentName !!}">
                        </div>
                    </div>
                </div>
            @endif
            <input id="note-attachment" class="@if($hasAttachment) hidden @endif" type="file" name="attachment">
        </div>
    </div>

    @if(isset(Illuminate\Support\Facades\Auth::user()->id))
    <div class="form-group">
        <div class="col-sm-12">
            <label>{!! trans('global.Visibility') !!}</label>
            @foreach(\App\Models\Role::getRoles() as $role)
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="visibility[{!! $role->id !!}]"
                    @if(isset($note))
                        @if($note->noteIsVisibleForUser($role->id))
                            checked
                        @endif
                    @else
                        checked
                    @endif
                    >
                    {!! $role->name !!}
                </label>
            </div>
            @endforeach
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="visibility[0]"
                    @if(isset($note) && $note->noteIsVisibleForUser(null))
                        checked
                    @endif
                    >
                    {!! trans('global.Insured') !!}
                </label>
            </div>
        </div>
    </div>
    @endif

</div>