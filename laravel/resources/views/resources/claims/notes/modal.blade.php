 <div class="modal {!! old('modal_old') == (!empty($modalId) ? $modalId : 'modal-claims-notes') ? 'modal_old' : '' !!}"
     id="{!! !empty($modalId) ? $modalId : 'modal-claims-notes'!!}">
    <div class="modal-dialog">
        <div class="modal-content">

            <form role="form" method="POST" action="{!! $action !!}" class="validate" enctype="multipart/form-data" novalidate>
                {!! csrf_field() !!}
                <input type="hidden" name="modal_old"
                       value="{!! !empty($modalId) ? $modalId : 'modal-claims-notes'!!}">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{!! trans('global.Notes') !!}</h4>
                </div>

                <div class="modal-body">
                    @yield($section)
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">
                        {!! trans('global.Close') !!}
                    </button>
                    <button type="submit" class="btn btn-success">
                        {!! trans('global.Save') !!}
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>