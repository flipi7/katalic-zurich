<table class="table table-hover members-table middle-align notes-table">
    <thead>
    <tr>
        <th>{!! trans('global.Name') !!}</th>
        <th>{!! trans('global.Date') !!}</th>
        <th>{!! trans('global.Note') !!}</th>
        <th>{!! trans('global.Attachment') !!}</th>
        <th>{!! trans('global.Options') !!}</th>
    </tr>
    </thead>
    <tbody>
    @if($claim->notes->isEmpty())
        <tr style="text-align: center">
            <td colspan="5">
                <strong>{!! trans('global.No Notes') !!}</strong>. {!! trans('global.Please create new note') !!}.
            </td>
        </tr>
    @else
        @if($claim->thereIsAVisibleNoteForUser($userRoleId))
        @foreach($claim->notes as $note)
            @if($note->noteIsVisibleForUser($userRoleId))
            <tr>
                <td>
                    @if($note->user != null)
                        {!! $note->user->name !!}
                    @else
                        {!! trans('global.Insured') !!}
                    @endif
                </td>
                <td>
                    {!! $note->date !!}
                </td>
                <td class="note-description">
                    {!! $note->description !!}
                </td>
                <td class="note-attachment">
                    @if(!empty($note->attachmentName))
                        <a href="{!! '/static/' . $note->attachmentHash . '-' . $note->attachmentName !!}" target="_blank"><i class="fa fa-search"></i>
                        <a href="{!! '/static/' . $note->attachmentHash . '-' . $note->attachmentName !!}" download target="_blank"><i class="fa fa-download"></i>
                    @else
                        -
                    @endif
                </td>
                <td>
                    @if( (Illuminate\Support\Facades\Auth::guest() && $note->user == null) ||
                         (!Illuminate\Support\Facades\Auth::guest() && $note->user != null && Illuminate\Support\Facades\Auth::user()->id == $note->user->id) ||
                         (!Illuminate\Support\Facades\Auth::guest() && Illuminate\Support\Facades\Auth::user()->id == \App\Models\Role::SUPER_ADMIN) ||
                         (!Illuminate\Support\Facades\Auth::guest() && Illuminate\Support\Facades\Auth::user()->id == \App\Models\Role::COORDINADOR) )
                    <a class="btn btn-xs btn-default" href="#" data-toggle="modal"
                       data-target="#modal-claims-notes-{!! $note->id !!}">
                        <i class="fa fa-pencil"></i>
                        {!! trans('global.Edit') !!}
                    </a>
                    @include('resources.claims.notes.edit')
                    <a class="btn btn-xs btn-default" href="#" data-toggle="modal"
                       data-target="#modal-note-delete-{!! $note->id !!}">
                        <i class="fa fa-remove"></i>
                        {!! trans('global.Delete') !!}
                    </a>
                    @include('resources.claims.notes.modal-delete')
                    @endif
                </td>
            </tr>
            @endif
        @endforeach
        @else
            <tr style="text-align: center">
                <td colspan="5">
                    <strong>{!! trans('global.No available notes') !!}</strong>.
                </td>
            </tr>
        @endif
    @endif
    </tbody>
</table>
