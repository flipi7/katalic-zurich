<div class="modal fade" id="modal-note-delete-{!! $note->id !!}">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">{!! trans('global.Delete') !!} {!! trans('global.Note') !!}</h4>
            </div>

            <div class="modal-body">
                <p>{!! trans('global.Are you sure you want to delete this?') !!}</p>
            </div>

            <div class="modal-footer">
                <form method="POST" action="{!! route('claims.notes.destroy', [$claim, $note]) !!}">
                    {!! csrf_field() !!}
                    {!! method_field('DELETE') !!}
                    <button type="button" class="btn btn-white" data-dismiss="modal">
                        {!! trans('global.Close') !!}
                    </button>
                    <button type="submit" class="btn btn-danger">
                        {!! trans('global.Delete') !!}
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>