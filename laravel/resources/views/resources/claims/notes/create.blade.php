@extends('resources.claims.notes.modal', [
    'action' => route('claims.notes.store', $claim),
    'section' => 'modal-body-notes-create'
])

@section('modal-body-notes-create')
    @include('resources.claims.notes.form', [
        'bagName' => 'claim_notes',
        'note' => old('note'),
        'visibility' => old('visibility'),
        'description' => old('description'),
        'hasAttachment' => !empty(old('attachmentName')),
        'attachmentName' => old('attachmentName')
    ])
@endsection