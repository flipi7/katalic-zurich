<div class="modal {!! $errors->claim_notes->isEmpty() ? '' : 'js-modal-error' !!}">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" method="POST" action="{!! session('formResponse')['route'] !!}"
                  class="validate" novalidate>
                {!! csrf_field() !!}
                {!! method_field(session('formResponse')['method']) !!}

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{!! trans('global.Create New Note') !!}</h4>
                </div>

                <div class="modal-body">

                    @include('resources.claims.notes.form', [
                        'bagName' => 'claim_notes',
                        'description' => old('description'),
                        'attachment' => old('attachment')
                    ])

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">
                        {!! trans('global.Close') !!}
                    </button>
                    <button type="submit" class="btn btn-success">
                        {!! trans('global.Create') !!}
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>