<div class="panel panel-default">
    <div class="panel-heading">
        {!! trans('global.Notes') !!}
        <div class="panel-options">
            <a class="btn btn-single btn-gray" href="javascript:" data-toggle="modal"
               data-target="#modal-claims-notes">
                {!! trans('global.Add New Note') !!}
            </a>
        </div>
    </div>

    <div class="panel-body">
        <!-- Show -->
        @include('resources.claims.notes.notes-list', [
            'userRoleId'  => isset(Illuminate\Support\Facades\Auth::user()->id) ? Illuminate\Support\Facades\Auth::user()->id : null
        ])

         <!-- Modal -->
        @include('resources.claims.notes.create')
    </div>
</div>