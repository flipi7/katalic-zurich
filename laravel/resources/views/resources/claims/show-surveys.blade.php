<div class="panel-group panel-group-joined">

    @foreach($claim->surveys as $survey)
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion-test" href="#collapse_{!! $survey->id !!}" class="collapsed">
                        {!! $survey->name !!}
                    </a>
                </h4>
            </div>
            <div id="collapse_{!! $survey->id !!}" class="panel-collapse collapse">
                <div class="panel-body">
                    @foreach($claim->surveyAnswers->where('template_id', $survey->id) as $question)
                        <div class="form-group">
                            <label>{!! $question->name !!}</label>
                            <input class="form-control"
                                   value="{!! trans('global.' . $question->pivot->answer) !!}" disabled>
                        </div>
                        <div class="form-group-separator"></div>
                    @endforeach
                </div>
            </div>
        </div>
    @endforeach
</div>