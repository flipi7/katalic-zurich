<form role="form" id="report-form" method="POST" action="{!! $action !!}" class="validate" novalidate>
    {!! csrf_field() !!}
    @yield('form-data-report')
</form>