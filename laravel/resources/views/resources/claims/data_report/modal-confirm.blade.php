<div class="modal fade" id="modal-report-confirm">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">{!! trans('global.Confirm report') !!}</h4>
            </div>

            <div class="modal-body">
                @if(empty($claim->damageAssessments()->first()))
                    <p><strong>{!! trans('global.No damage assessments have been provided.') !!}</strong></p>
                @endif
                    <p>{!! trans('global.Are you sure you want to confirm the report?') !!}</p>
            </div>

            <div class="modal-footer">
                <form method="POST" action="{!! route('claims.dataReport.confirm', $claim) !!}">
                    {!! csrf_field() !!}
                    <button type="button" class="btn btn-white" data-dismiss="modal">
                        {!! trans('global.Close') !!}
                    </button>
                    <button type="submit" class="btn btn-success">
                        {!! trans('global.Confirm') !!}
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>