@extends('resources.claims.data_report.show-form', [
    'action' => route('claims.dataReport.update', [$claim, $claim->dataReport])
])

@section('form-data-report')
    {!! method_field('PUT') !!}
    @include('resources.claims.data_report.form', [
        'claimReport' => old('claim.data_report') ? old('claim.data_report') : $claim->dataReport()->first()
    ])
@endsection
