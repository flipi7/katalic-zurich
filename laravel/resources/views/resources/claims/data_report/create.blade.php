@extends('resources.claims.data_report.show-form', [
    'action' => route('claims.dataReport.store', [$claim])
])

@section('form-data-report')
    @include('resources.claims.data_report.form', [
        'claimReport' => [
            'salvage' => old('claim.data_report.salvage', ''),
            'subrogation' => old('claim.data_report.subrogation', ''),
            'agreement' => old('claim.data_report.agreement', ''),
            'fraud' => old('claim.data_report.fraud', ''),
            'insurance' => old('claim.data_report.insurance', ''),
            'claim_report' => old('claim.data_report.claim_report', ''),
        ]
    ])
@endsection