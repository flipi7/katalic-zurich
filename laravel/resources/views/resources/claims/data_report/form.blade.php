<div class="panel-body">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group {!! $errors->first('claim.report.salvage') ? 'has-error' : '' !!}">
                <strong>{!! trans('global.Salvage') !!}</strong>

                <p>
                    <label class="radio-inline">
                        <input type="radio"
                               name="claim[report][salvage]"
                               value="yes" {!! $claimReport['salvage'] == 'yes'  ? 'checked' : '' !!}
                               @if(!$editable)
                               disabled
                                @endif>
                        {!! trans('global.Yes') !!}
                    </label>
                    <label class="radio-inline">
                        <input type="radio"
                               name="claim[report][salvage]"
                               value="no" {!! $claimReport['salvage'] == 'no'  ? 'checked' : '' !!}
                               @if(!$editable)
                               disabled
                                @endif>
                        {!! trans('global.No') !!}
                    </label>
                </p>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group {!! $errors->first('claim.report.subrogation') ? 'has-error' : '' !!}">
                <strong>{!! trans('global.Subrogation') !!}</strong>

                <p>
                    <label class="radio-inline">
                        <input type="radio"
                               name="claim[report][subrogation]"
                               value="yes" {!! $claimReport['subrogation'] == 'yes'  ? 'checked' : '' !!}
                               @if(!$editable)
                               disabled
                                @endif>
                        {!! trans('global.Yes') !!}
                    </label>
                    <label class="radio-inline">
                        <input type="radio"
                               name="claim[report][subrogation]"
                               value="no" {!! $claimReport['subrogation'] == 'no'  ? 'checked' : '' !!}
                               @if(!$editable)
                               disabled
                                @endif>
                        {!! trans('global.No') !!}
                    </label>
                </p>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group {!! $errors->first('claim.report.agreement') ? 'has-error' : '' !!}">
                <strong>{!! trans('global.Agreement') !!}</strong>

                <p>
                    <label class="radio-inline">
                        <input type="radio"
                               name="claim[report][agreement]"
                               value="yes" {!! $claimReport['agreement'] == 'yes'  ? 'checked' : '' !!}
                               @if(!$editable)
                               disabled
                                @endif>
                        {!! trans('global.Yes') !!}
                    </label>
                    <label class="radio-inline">
                        <input type="radio"
                               name="claim[report][agreement]"
                               value="no" {!! $claimReport['agreement'] == 'no'  ? 'checked' : '' !!}
                               @if(!$editable)
                               disabled
                                @endif>
                        {!! trans('global.No') !!}
                    </label>
                </p>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group {!! $errors->first('claim.report.fraud') ? 'has-error' : '' !!}">
                <strong>{!! trans('global.Fraud') !!}</strong>

                <p>
                    <label class="radio-inline">
                        <input type="radio"
                               name="claim[report][fraud]"
                               value="yes" {!! $claimReport['fraud'] == 'yes'  ? 'checked' : '' !!}
                               @if(!$editable)
                               disabled
                                @endif>
                        {!! trans('global.Yes') !!}
                    </label>
                    <label class="radio-inline">
                        <input type="radio"
                               name="claim[report][fraud]"
                               value="no" {!! $claimReport['fraud'] == 'no'  ? 'checked' : '' !!}
                               @if(!$editable)
                               disabled
                                @endif>
                        {!! trans('global.No') !!}
                    </label>
                </p>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group {!! $errors->first('claim.report.insurance') ? 'has-error' : '' !!}">
                <label for="state">{!! trans('global.Insurance') !!}</label>

                <select id="insurance" name="claim[report][insurance]" class="selectboxit" data-validate="required" {!! !empty($disabled) ? 'disabled' : '' !!}>
                    <option value="">{!! trans('global.Select Insurance') !!}</option>
                    <option value="under" {!! $claimReport['insurance'] == 'under'  ? 'selected' : '' !!}>{!! trans('global.Under') !!}</option>
                    <option value="over" {!! $claimReport['insurance'] == 'over'  ? 'selected' : '' !!}>{!! trans('global.Over') !!}</option>
                    <option value="right" {!! $claimReport['insurance'] == 'right'  ? 'selected' : '' !!}>{!! trans('global.Right') !!}</option>
                </select>
            </div>
        </div>

        <div class="col-md-12">
            <div class="form-group {!! $errors->first('claim.report.claim_report') ? 'has-error' : '' !!}">
                <label class="control-label" for="about"><strong>{!! trans('global.Claim Report') !!}</strong></label>
                <textarea id="claim-report" rows="3" class="form-control autogrow" name="claim[report][claim_report]" data-validate="required"
                          placeholder="{!! trans('global.Claim Report') !!}"
                          @if(!$editable)
                          disabled
                        @endif >{!! $claimReport['claim_report'] !!}</textarea>
            </div>
        </div>

        @if($editable)
            <div class="col-md-12 white-row">
                <button id="report-submit" type="submit" class="btn btn-primary btn-single">
                    <?php echo trans('global.Save'); ?>
                </button>
            </div>
        @endif

    </div>

    <script src="{!! asset('assets/js/wysihtml5/wysihtml5-angular.js') !!}"></script>
    @if($editable)
        <script>
            $('#claim-report').wysihtml5({
                "font-styles": true, //Font styling, e.g. h1, h2, etc. Default true
                "emphasis": true, //Italics, bold, etc. Default true
                "lists": true, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
                "html": false, //Button which allows you to edit the generated HTML. Default false
                "link": false, //Button to insert a link. Default true
                "image": false, //Button to insert an image. Default true,
                "color": false //Button to change color of font
            });
        </script>
    @else
        <script>
            $('#claim-report').wysihtml5({
                "font-styles": false, //Font styling, e.g. h1, h2, etc. Default true
                "emphasis": false, //Italics, bold, etc. Default true
                "lists": false, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
                "html": false, //Button which allows you to edit the generated HTML. Default false
                "link": false, //Button to insert a link. Default true
                "image": false, //Button to insert an image. Default true,
                "color": false //Button to change color of font
            });
        </script>
    @endif
</div>