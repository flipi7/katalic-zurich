<div class="panel panel-default">
    <div class="panel-heading">
        {!! trans('global.Claim Report') !!}
        @if($editable && $claim->hasStatus(\App\Models\Status::PRELIMINARY) && !$claim->hasStatus(\App\Models\Status::REPORT))
            <div class="panel-options">
                <button class="btn btn-single btn-success {!! !empty($claim->dataReport()) ? '' : 'disabled' !!}"
                        data-toggle="modal"
                        data-target="#modal-report-confirm">{!! trans('global.Confirm report') !!}</button>
            </div>
        @endif
    </div>
    @if(empty($claim->dataReport()->first()))
        @include('resources.claims.data_report.create')
    @else
        @include('resources.claims.data_report.edit')
    @endif
    @include('resources.claims.data_report.modal-confirm')
</div>
@if(empty($claim->dataReport()->first()))
    @include('resources.claims.damage_assessment.show', ['editable' => FALSE])
@else
    @include('resources.claims.damage_assessment.show')
@endif