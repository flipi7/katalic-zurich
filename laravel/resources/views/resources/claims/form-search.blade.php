<form role="form" class="search-claims form-inline" name="q" method="GET" action="{!! route('claims.index') !!}">
    <div class="input-group search-group">
        <span class="input-group-btn">
            <button class="btn btn-default" type="button"><i class="linecons-search"></i></button>
        </span>
        <input type="text" class="form-control input-md" placeholder="{!! trans('global.Search') !!}..." value="{!! old('claim.id') !!}" name="q">
    </div>
</form>