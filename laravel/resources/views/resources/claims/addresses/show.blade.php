@if(!empty($claim->address->id))
    @include('resources.claims.addresses.form', [
        'disabled' => true,
        'claimAddress' => $claim->address->toArray()
    ])
@else
    <div class="alert alert-danger">{!! trans('global.No data provided') !!}</div>
@endif