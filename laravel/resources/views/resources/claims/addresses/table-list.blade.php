<table class="table table-hover members-table middle-align">
    <thead>
    <tr>
        <th></th>
        <th>{!! trans('global.Name') !!}</th>
        <th>{!! trans('global.Phone') !!}</th>
        <th>{!! trans('global.Email') !!}</th>
        <th>{!! trans('global.Options') !!}</th>
    </tr>
    </thead>
    <tbody>
    @foreach($addresses as $address)
        <tr>
            <td>
                {!! $address->id !!}
            </td>
            <td class="hidden-xs hidden-sm">
                <span class="email">{!! $user->email !!}</span>
            </td>
            <td class="user-id">
                {!! $user->id !!}
            </td>
            <td class="action-links">
                <a href="{!! route('users.edit', $user) !!}" class="edit">
                    <i class="linecons-pencil"></i>
                    {!! trans('global.Edit') !!}
                </a>

                <a href="javascript:" class="delete" data-toggle="modal" data-target="#modal-delete-user-{!! $user->id !!}">
                    <i class="linecons-trash"></i>
                    {!! trans('global.Delete') !!}
                </a>

                @include('modals.alert-delete', ['modalId' => 'delete-user-' . $user->id])

            </td>
        </tr>
    @endforeach
    </tbody>
</table>

<div class="row">
    <div class="col-sm-offset-6 col-sm-6 text-right text-center-sm">
        {!! $users->render() !!}
    </div>
</div>
