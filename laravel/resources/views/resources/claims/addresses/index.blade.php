<div class="panel panel-default">
    <div class="panel-heading">
        {!! trans('global.Claim Address') !!}
        @if($editable)
            <div class="panel-options">
                <a class="btn btn-single btn-gray" data-toggle="modal" data-target="#modal-claims-address">Edit</a>
            </div>
        @endif
    </div>

    <div class="panel-body">

        <!-- Show -->
        @include('resources.claims.addresses.show')

        <!-- Modal -->
        @if(!empty($claim->address->id))
            @include('resources.claims.addresses.edit')
        @else
            @include('resources.claims.addresses.create')
        @endif
    </div>
</div>