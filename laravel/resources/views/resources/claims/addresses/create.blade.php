@extends('resources.claims.addresses.modal', [
    'action' => route('claims.addresses.store', [$claim])
])

@section('modal-body')
    @include('resources.claims.addresses.form', [
        'claimAddress' => old('claim.address')
    ])
@endsection