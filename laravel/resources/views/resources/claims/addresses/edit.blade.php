@extends('resources.claims.addresses.modal', [
    'action' => route('claims.addresses.update', [$claim, $claim->address])
])

@section('modal-body')
    {!! method_field('PUT') !!}
    @include('resources.claims.addresses.form', [
        'claimAddress' => old('claim.address') ? old('claim.address') : $claim->address->toArray()
    ])
@endsection