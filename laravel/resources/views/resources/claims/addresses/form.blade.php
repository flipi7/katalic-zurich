<div class="row">

    <div class="col-md-12">
        <div class="form-group {!! $errors->first('claim.address.address') ? 'has-error' : '' !!}">
            <label class="control-label" for="street">{!! trans('global.Address') !!}</label>
            <input class="form-control" name="claim[address][address]" id="street" data-validate="required"
                   placeholder="{!! trans('global.Address') !!}"
                   value="{!! $claimAddress['address'] !!}"
                    {!! !empty($disabled) ? 'disabled' : '' !!}
                    />
        </div>
    </div>

</div>


<div class="row">

    <div class="col-md-4">
        <div class="form-group {!! $errors->first('claim.address.country_id') ? 'has-error' : '' !!}">
            <label for="state">{!! trans('global.Country') !!}</label>
            <select id="country" name="claim[address][country_id]" class="form-control" data-validate="required" {!! !empty($disabled) ? 'disabled' : '' !!}>
                <option value="">{!! trans('global.Select Country') !!}</option>
                @foreach($countries as $country)
                    <?php
                    $selected = $claimAddress['country_id'] == $country->id  ? 'selected' : '';
                    if ($selected == 'selected')
                        $countryName = $country->name;
                    ?>
                    <option value="{!! $country->id !!}" {!! $selected !!}>{!! $country->name !!}</option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="col-md-4">
        <div class="form-group {!! $errors->first('claim.address.state_id') ? 'has-error' : '' !!}">
            <label for="state">{!! trans('global.State') !!}</label>
            <select id="state" name="claim[address][state_id]" class="form-control" data-validate="required" {!! !empty($disabled) ? 'disabled' : '' !!}>
                <option value="">{!! trans('global.Select State') !!}</option>
                @if(!empty($selectedCountry))
                    @foreach($selectedCountry->states as $state)
                        <?php
                        $selected = $claimAddress['state_id'] == $state->id  ? 'selected' : '';
                        if ($selected == 'selected')
                            $stateName = $state->name;
                        ?>
                        <option value="{!! $state->id !!}" {!! $selected !!}>{!! $state->name !!}</option>
                    @endforeach
                @endif
            </select>
        </div>
    </div>

    <div class="col-md-4">
        <div class="form-group">
            <label for="state">{!! trans('global.City') !!}</label>

            <select id="city" name="claim[address][city_id]" class="form-control" {!! !empty($disabled) ? 'disabled' : '' !!}>
                <option value="">{!! trans('global.Select City') !!}</option>
                @if(!empty($selectedState))
                    @foreach($selectedState->cities as $city)
                        <?php
                        $selected = $claimAddress['city_id'] == $city->id  ? 'selected' : '';
                        if ($selected == 'selected')
                            $cityName = $city->name;
                        ?>
                        <option value="{!! $city->id !!}" {!! $selected !!}>{!! $city->name !!}</option>
                    @endforeach
                @endif
            </select>
        </div>
    </div>

</div>


<div class="row">

    <div class="col-md-12">
        <div id="address-map-container" class="hidden">
            <label for="state">{!! trans('global.Map') !!}</label>
            <div id="addressMap"></div>
        </div>
    </div>

</div>
<script src="https://maps.googleapis.com/maps/api/js"></script>
<script>
    function initialize(myLatLng) {

        // Create a map object and specify the DOM element for display.
        var map = new google.maps.Map(document.getElementById('addressMap'), {
            center: myLatLng,
            scrollwheel: false,
            zoom: 12
        });

        // Create a marker and set its position.
        var marker = new google.maps.Marker({
            map: map,
            position: myLatLng,
            title: 'Hello World!'
        });

        $('#address-map-container').removeClass('hidden');
    }

    function generateMapFromAddress(address) {
        var url = "http://maps.googleapis.com/maps/api/geocode/json?address=" + address + "&sensor=false";
        $.getJSON(url)
                .done(function (data) {
                    if (data.results.length > 0) {
                        var location = data.results[0].geometry.location;
                        initialize({'lat': parseFloat(location.lat), 'lng': parseFloat(location.lng)});
                    }
                    else {
                        console.log('no results');
                        $('#address-map-container').addClass('hidden');
                    }
                })
                .fail(function () {
                    console.log("error on address map");
                });
    }
</script>

@if(isset($claimAddress['address']))
    <?php
    $address = str_replace (" ", "+", str_replace ("\n", "+", $claimAddress['address']));
    if (!isset($cityName)) {
        $cityName = '';
    }
    $finalAddress = $address . ',+' . $cityName . ',+' . $stateName . ',+' . $countryName;
    ?>
    <script>
        var address = "{!! $finalAddress !!}";
        generateMapFromAddress(address);
    </script>
@else
    <script>
        function getAddress() {
            var street = $('#street').val();
            var country = '';
            var state = '';
            var city = '';

            if($('#country').val() != '') {
                country = ', ' + $('#country').find(":selected").text();
            }
            if($('#state').val() != '') {
                state = ', ' + $('#state').find(":selected").text();
            }
            if($('#city').val() != '') {
                city = ', ' + $('#city').find(":selected").text();
            }

            var address = street + city + state + country;
            return address;
        }

        function generate() {
            var address = getAddress();
            if(address != '') {
                generateMapFromAddress(address.replace(/\s+/g, '+'));
            }
            else {
                $('#address-map-container').addClass('hidden');
            }
        }

        // Update with every change
        $('#street').focusout(generate);
        $("select[name^='claim[address]']").change(generate);

        // Generates map only when all fields are complete
        //        $("[name^='claim[address]").change(function(){
        //            if( $('#street').val() != '' && ($('#country').val() != '') && ($('#state').val() != '') && ($('#city').val() != '')) {
        //                generate();
        //            }
        //        });

    </script>
@endif
