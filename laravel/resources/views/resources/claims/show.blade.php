@extends('layouts.master')

@section('content')

    <div class="page-title">

        <div class="title-env">
            <h1 class="title">{!! trans('global.View Claim') !!}</h1>

            <p class="description">{!! trans('global.View claim subtitle') !!}</p>
        </div>

        <div class="breadcrumb-env">
            {!! Breadcrumbs::render('claim', $claim) !!}
        </div>

    </div>

    <div class="row">
        <div class="col-md-12">

            <!-- Tabbed panel -->
            <div class="panel panel-flat"><!-- Add class "collapsed" to minimize the panel -->
                <div class="panel-heading">
                    <h3 class="panel-title">{!! trans('Claim') !!} #{!! $claim->id !!}</h3>
                </div>

                <div class="panel-body">
                    @include('resources.claims.show-data', ['status' => $status, 'latestStatus' => $claim->getHighestStatus()])
                </div>
            </div>

        </div>
    </div>


@endsection

@section('pageStyles')
    <link rel="stylesheet" href="{!! asset('assets/js/multiselect/css/multi-select.css') !!}">
@endsection

@section('pageScripts')
    <script src="{!! asset('assets/js/jquery-validate/jquery.validate.min.js') !!}"></script>
    <script src="{!! asset('assets/js/inputmask/jquery.inputmask.bundle.js') !!}"></script>
    <script src="{!! asset('assets/js/formwizard/jquery.bootstrap.wizard.min.js') !!}"></script>
    <script src="{!! asset('assets/js/datepicker/bootstrap-datepicker.js') !!}"></script>
    <script src="{!! asset('assets/js/multiselect/js/jquery.multi-select.js') !!}"></script>
    <script src="{!! asset('assets/js/jquery-ui/jquery-ui.min.js') !!}"></script>
    <script src="{!! asset('assets/js/selectboxit/jquery.selectBoxIt.min.js') !!}"></script>
    <script src="{!! asset('assets/js/app-geolocation.js') !!}"></script>


    <script>

        <!-- submenu -->
      $(window).ready(function() {
        var hash = window.location.hash;
        if (hash=="#tab-docs") {
            //timeout needed to make this work
            window.setTimeout(function() {
                $("#docs-action").click();
            }, 500);  
        }
      });

    </script>


@endsection