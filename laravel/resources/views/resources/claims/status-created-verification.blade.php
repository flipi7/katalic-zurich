<!-- Information -->
@include('resources.claims.data_basic.index')

<!-- Address -->
@include('resources.claims.addresses.index')

        <!-- Description -->
@include('resources.claims.description.index')

        <!-- Contacts -->
@include('resources.claims.contacts.index')

        <!-- Ike Additional -->
@include('resources.claims.data_ike.index')

        <!-- Surveys-->
<div class="panel panel-default">
    <div class="panel-heading">
        {!! trans('global.Claim Surveys') !!}

        @if(!$claim->surveys->isEmpty())
            @if($editable)
                <div class="panel-options">
                    <a class="btn btn-single btn-gray" data-toggle="modal"
                       data-target="#modal-claims-update-survey">Edit</a>
                </div>
            @endif
        @endif
    </div>

    <div class="panel-body">
        @if(!$claim->surveys->isEmpty())
            @include('resources.claims.show-surveys')
            @include('resources.claims.modal-form-survey')
        @else
            <div class="alert alert-danger">{!! trans('global.Please select claim covers first') !!}.</div>
        @endif
    </div>
</div>
