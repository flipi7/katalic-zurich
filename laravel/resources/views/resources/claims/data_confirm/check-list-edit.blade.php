{!! method_field('PUT') !!}
@include('resources.claims.data_confirm.check-list-form', [
    'action' => route('claims.checkList.update', [$claim]),
    'questions' => \App\Models\CheckListQuestion::all(),
    'answers' => \App\Models\CheckListAnswer::where('claim_id', $claim->id),
])
