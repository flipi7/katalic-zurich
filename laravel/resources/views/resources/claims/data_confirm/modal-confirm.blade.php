<div class="modal fade" id="modal-confirm">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">{!! trans('global.Confirm ' . \App\Models\Status::getStatusName($status)) !!}</h4>
            </div>

            <div class="modal-body">
                    <p>{!! trans('global.Are you sure you want to confirm?') !!}</p>
            </div>

            <div class="modal-footer">
                <form method="POST" action="{!! route('claims.' . \App\Models\Status::getStatusName($status), $claim) !!}">
                    {!! csrf_field() !!}
                    <button type="button" class="btn btn-white" data-dismiss="modal">
                        {!! trans('global.Close') !!}
                    </button>
                    <button type="submit" class="btn btn-success">
                        {!! trans('global.Confirm') !!}
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>