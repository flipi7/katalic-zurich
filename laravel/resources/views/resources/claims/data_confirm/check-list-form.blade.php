<form role="form" method="POST" action="{!! $action !!}">
    {!! csrf_field() !!}
    <div class="row">
        <div class="col-md-12">
            <div class="tab-content">
                @foreach($questions as $question)
                    <div class="form-group">
                        <strong>{!! $question['name'] !!}</strong>
                        <p>
                            <label class="radio-inline">
                                <input type="radio"
                                       name="claim[check_list][{!! $question['id'] !!}]"
                                       value="yes" {!! (\App\Models\CheckListAnswer::getAnswerByQuestionId($claim->id, $question['id'])) == 'yes' ? 'checked' : '' !!}>
                                {!! trans('global.Yes') !!}
                            </label>
                            <label class="radio-inline">
                                <input type="radio"
                                       name="claim[check_list][{!! $question['id'] !!}]"
                                       value="no" {!! (\App\Models\CheckListAnswer::getAnswerByQuestionId($claim->id, $question['id']))  == 'no'  ? 'checked' : '' !!}>
                                {!! trans('global.No') !!}
                            </label>
                        </p>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <button class="btn btn-primary" type="submit">
                {!! trans('global.Save') !!}
            </button>
        </div>
    </div>
</form>