@include('resources.claims.data_confirm.check-list-form', [
    'action' => route('claims.checkList.store', [$claim]),
    'questions' => \App\Models\CheckListQuestion::all()
])
