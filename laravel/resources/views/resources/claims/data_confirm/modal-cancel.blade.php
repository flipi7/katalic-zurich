<div class="modal fade" id="modal-cancel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">{!! trans('global.Cancel Claim') !!}</h4>
            </div>

            <div class="modal-body">
                <p>{!! trans('global.Are you sure you want to cancel this claim?') !!}</p>
            </div>

            <div class="modal-footer">
                <form method="POST" action="{!! route('claims.dataVerification.cancel', $claim) !!}">
                    {!! csrf_field() !!}
                    <button type="button" class="btn btn-white" data-dismiss="modal">
                        {!! trans('global.Close') !!}
                    </button>
                    <button type="submit" class="btn btn-success">
                        {!! trans('global.Confirm') !!}
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>