@if($editable)
    <div class="panel panel-default panel-cancelling">
        <div class="panel-heading">
            {!! trans('global.Are you sure you want to cancel this claim?') !!}
            <div class="panel-options">
                <button class="btn btn-single btn-warning {!! $claim->status->id != \App\Models\Status::CANCELLED ? '' : 'disabled' !!}"
                        data-toggle="modal"
                        data-target="#modal-cancel">{!! trans('global.Cancel Claim') !!}</button>
            </div>
            @include('resources.claims.data_confirm.modal-cancel')
        </div>
    </div>
@endif