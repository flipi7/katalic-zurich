@if($editable && $claim->status->id == $status - 1)
    <div class="panel panel-default panel-processing">
        <div class="panel-heading">
            {!! trans('global.' . $message) !!}
            <div class="panel-options">
                <button class="btn btn-single btn-success {!! ($claim->checkListAnswers()->count() == \App\Models\CheckListQuestion::all()->count() && $claim->status->id == 6) || $claim->status->id == 7 ? '' : 'disabled' !!}"
                        data-toggle="modal"
                        data-target="#modal-confirm">{!! trans('global.Confirm ' . \App\Models\Status::getStatusName($status)) !!}</button>
            </div>
            @include('resources.claims.data_confirm.modal-confirm')
        </div>
    </div>
@endif

<?php
    if(Auth::user()->id != 1 && Auth::user()->id != 6) {
        $editable = FALSE;
    }
?>

@if($editable && $status == 7)
    <div class="gray-row">
        <h1>{!! trans('global.Survey') !!}</h1>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">{!! $claim->surveys()->first()['name'] !!}</div>
        <div class="panel-body">
            @include('resources.claims.data_confirm.check-list-create')
        </div>
    </div>
@endif

<div id="processing-summary">
    <div class="gray-row">
        <h1>{!! trans('global.Created') !!}</h1>
    </div>
    @include('resources.claims.status-created-verification')
    <div class="gray-row">
        <h1>{!! trans('global.Verification') !!}</h1>
    </div>
    @include('resources.claims.data_verification.index')
    <div class="gray-row">
        <h1>{!! trans('global.Contact') !!}</h1>
    </div>
    @include('resources.claims.status-contact')
    <div class="gray-row">
        <h1>{!! trans('global.Visit') !!}</h1>
    </div>
    @include('resources.claims.data_visit.index')
    <div class="gray-row">
        <h1>{!! trans('global.Preliminary') !!}</h1>
    </div>
    @include('resources.claims.data_preliminary.index')
    <div class="gray-row">
        <h1>{!! trans('global.Report') !!}</h1>
    </div>
    @include('resources.claims.data_report.index')
</div>