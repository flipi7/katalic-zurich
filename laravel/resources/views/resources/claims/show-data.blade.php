<section class="profile-env">

    <div class="row">
        @include('resources.claims.status-flow', array('claimId' => $claim->id, 'status' => $status, 'latestStatus' => $latestStatus))
    </div>

    <div class="row">

        <div class="col-sm-10">


            <div class="tab-content">


                <div class="tab-pane" id="tab-claim">

                    @if($status == '' || $status == \App\Models\Status::CREATED)
                        @include('resources.claims.status-created-verification')
                    @elseif($status == \App\Models\Status::VERIFICATION)
                        @include('resources.claims.data_verification.index')
                    @elseif($status == \App\Models\Status::CONTACT)
                        @include('resources.claims.status-contact')
                    @elseif($status == \App\Models\Status::VISIT)
                        @include('resources.claims.data_visit.index')
                    @elseif($status == \App\Models\Status::PRELIMINARY)
                        @include('resources.claims.data_preliminary.index')
                    @elseif($status == \App\Models\Status::REPORT)
                        @include('resources.claims.data_report.index')
                    @elseif($status == \App\Models\Status::PROCESSING)
                        @include('resources.claims.data_confirm.index', ['message' => 'Please, verify that all the provided data and reports below are correct before confirming.'])
                    @elseif($status == \App\Models\Status::FINISHED)
                        @include('resources.claims.data_confirm.index', ['message' => 'Please, verify that all the provided data and reports below are correct before confirming.'])
                    @elseif($status == \App\Models\Status::CANCELLED)
                        @include('resources.claims.data_confirm.cancel')
                    @endif

                </div>

                <div class="tab-pane" id="tab-note">
                    <!-- Notes -->
                    @include('resources.claims.notes.index')
                </div>

                <div class="tab-pane" id="tab-docs">
                    <!-- Documents -->
                    @include('resources.claims.documents.index')
                </div>

                <div class="tab-pane" id="tab-timeline">
                    @include('resources.claims.show-timeline')
                </div>

            </div>

        </div>

        <div class="col-sm-2">
            <a href="#tab-claim" data-toggle="tab" class="btn btn-block btn-primary btn-icon btn-icon-standalone btn-icon-standalone-right btn-tab">
                <i class="fa fa-ambulance"></i>
                <span>{!! trans('global.Claim') !!}</span>
            </a>

            <a href="#tab-note" data-toggle="tab" class="btn btn-block btn-primary btn-icon btn-icon-standalone btn-icon-standalone-right btn-tab">
                <i class="fa fa-th-list"></i>
                <span>{!! trans('global.Notes') !!}</span>
            </a>

            <a href="#tab-docs" data-toggle="tab" id="docs-action" class="btn btn-block btn-primary btn-icon btn-icon-standalone btn-icon-standalone-right btn-tab">
                <i class="fa fa-files-o"></i>
                <span>{!! trans('global.Documents') !!}</span>
            </a>
            <a href="#tab-timeline" data-toggle="tab" class="btn btn-block btn-primary btn-icon btn-icon-standalone btn-icon-standalone-right btn-tab">
                <i class="fa fa-clock-o"></i>
                <span>{!! trans('global.Timeline') !!}</span>
            </a>
        </div>

    </div>

</section>
