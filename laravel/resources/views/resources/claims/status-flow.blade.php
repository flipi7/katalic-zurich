<div class="col-sm-12 statuses">
    <?php if (Auth::user()) {
            $userRole = Auth::user()->roles()->first()->pivot->role_id;
          } else {
            $userRole = "public";
          }
    ?>
    <ul>
        <li>
            <div class="status-step">
                <a href="created">
                    <div class="status-icon">
                        <div class="status-icon-bubble created">
                            <i class="fa fa-plus"></i>
                        </div>
                    </div>
                    <div class="status-label">
                        <span>{!! trans('global.Created') !!}</span>
                    </div>
                </a>
            </div>
            @if($status == '' || $status == 1)<div class="active"></div>@endif
        </li>
        <li>
            <div class="status-step
                @if($latestStatus == 0 || $latestStatus == 9)
                    not-available
                @endif">
                <a href="verification">
                    <div class="status-icon">
                        <div class="status-icon-bubble verification">
                            <i class="fa fa-search"></i>
                        </div>
                    </div>
                    <div class="status-label">
                        <span>{!! trans('global.Verification') !!}</span>
                    </div>
                </a>
            </div>
            @if($status == 2)<div class="active"></div>@endif
        </li>
        <li>
            <div class="status-step
                @if($latestStatus < 2 || $latestStatus == 9)
                    not-available
                @endif">
                <a href="contact">
                    <div class="status-icon">
                        <div class="status-icon-bubble contact">
                            <i class="fa fa-phone"></i>
                        </div>
                    </div>
                    <div class="status-label">
                        <span>{!! trans('global.Contact') !!}</span>
                    </div>
                </a>
            </div>
            @if($status == 3)<div class="active"></div>@endif
        </li>
        <li>
            <div class="status-step
                @if($latestStatus < 3 || $latestStatus == 9)
                    not-available
                @endif">
                <a href="visit">
                    <div class="status-icon">
                        <div class="status-icon-bubble visit">
                            <i class="fa fa-sign-in"></i>
                        </div>
                    </div>
                    <div class="status-label">
                        <span>{!! trans('global.Visit') !!}</span>
                    </div>
                </a>
            </div>
            @if($status == 4)<div class="active"></div>@endif
        </li>
        <li>
            <div class="status-step
                @if($latestStatus < 4 || $latestStatus == 9)
                    not-available
                @endif">
                <a href="preliminary">
                    <div class="status-icon">
                        <div class="status-icon-bubble preliminary">
                            <i class="fa fa-mortar-board"></i>
                        </div>
                    </div>
                    <div class="status-label">
                        <span>{!! trans('global.Preliminary') !!}</span>
                    </div>
                </a>
            </div>
            @if($status == 5)<div class="active"></div>@endif
        </li>
        <li>
            <div class="status-step
                @if($latestStatus < 5 || $latestStatus == 9)
                    not-available
                @endif">
                <a href="report">
                    <div class="status-icon">
                        <div class="status-icon-bubble report">
                            <i class="fa fa-file-text-o"></i>
                        </div>
                    </div>
                    <div class="status-label">
                        <span>{!! trans('global.Report') !!}</span>
                    </div>
                </a>
            </div>
            @if($status == 6)<div class="active"></div>@endif
        </li>
        <li>
            <div class="status-step
                @if($latestStatus < 6 || $latestStatus == 9 || !$claim->hasStatus(\App\Models\Status::VERIFICATION))
                    not-available
                @endif">
                <a href="processing">
                    <div class="status-icon">
                        <div class="status-icon-bubble processing">
                            <i class="fa fa-exchange"></i>
                        </div>
                    </div>
                    <div class="status-label">
                        <span>{!! trans('global.Processing') !!}</span>
                    </div>
                </a>
            </div>
            @if($status == 7)<div class="active"></div>@endif
        </li>
        <li>
            <div class="status-step
                @if($latestStatus < 7 || $latestStatus == 9)
                    not-available
                @endif">
                <a href="finished">
                    <div class="status-icon">
                        <div class="status-icon-bubble finished">
                            <i class="fa fa-check-square-o"></i>
                        </div>
                    </div>
                    <div class="status-label">
                        <span>{!! trans('global.Finished') !!}</span>
                    </div>
                </a>
            </div>
            @if($status == 8)<div class="active"></div>@endif
        </li>
        <li>
            <div class="status-step
                @if($claim->hasStatus(\App\Models\Status::VERIFICATION))
                    not-available
                @endif">
                <a href="cancelled">
                    <div class="status-icon">
                        <div class="status-icon-bubble cancelled">
                            <i class="fa fa-close"></i>
                        </div>
                    </div>
                    <div class="status-label">
                        <span>{!! trans('global.Cancelled') !!}</span>
                    </div>
                </a>
            </div>
            @if($status == 9)<div class="active"></div>@endif
        </li>
    </ul>
</div>

@if($userRole == "public")
<script>
 $(".status-step a").attr("href", '#');
</script>
@endif

