<div class="panel panel-default">
    <div class="panel-heading">
        {!! trans('global.Preliminary Report') !!}
    </div>

    <div class="panel-body">
        <!-- Show -->
        @if(empty($claim->dataPreliminary()->first()))
            @include('resources.claims.data_preliminary.create')
        @else
            @include('resources.claims.data_preliminary.edit')
        @endif
    </div>
</div>
