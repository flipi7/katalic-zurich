<form role="form" method="POST" action="{!! $action !!}" class="validate" novalidate>
    {!! csrf_field() !!}
    @if(!empty($claim->dataPreliminary()->first()))
        {!! method_field('PUT') !!}
    @endif
    <div class="row">

        <div class="col-md-6">
            <div class="form-group {!! $errors->first('claim.preliminary.visit_date') ? 'has-error' : '' !!}">
                <label class="control-label" for="about"><strong>{!! trans('global.Visit Date') !!}</strong></label>
                <input class="form-control" name="claim[preliminary][visit_date]" data-validate="required" data-mask="datetime"
                       placeholder="dd/mm/yyyy hh:mm" aria-required="true"
                       aria-invalid="false" value="{!! $claimPreliminary['visit_date'] !!}"
                       @if(!$editable)
                           disabled
                       @endif />
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group {!! $errors->first('claim.preliminary.claim_date') ? 'has-error' : '' !!}">
                <label class="control-label" for="about"><strong>{!! trans('global.Claim Date') !!}</strong></label>
                <input class="form-control" name="claim[preliminary][claim_date]" data-validate="required" data-mask="datetime"
                       placeholder="dd/mm/yyyy hh:mm" aria-required="true"
                       aria-invalid="false" value="{!! $claimPreliminary['claim_date'] !!}"
                       @if(!$editable)
                           disabled
                       @endif />
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group {!! $errors->first('claim.preliminary.validity_start') ? 'has-error' : '' !!}">
                <label class="control-label" for="about"><strong>{!! trans('global.Validity Start') !!}</strong></label>
                <input class="form-control datepicker" name="claim[preliminary][validity_start]" data-validate="required" data-mask="date"
                       data-format="dd-mm-yyyy" placeholder="dd/mm/yyyy" aria-required="true"
                       aria-invalid="false" value="{!! $claimPreliminary['validity_start'] !!}"
                       @if(!$editable)
                           disabled
                       @endif />
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group {!! $errors->first('claim.preliminary.validity_end') ? 'has-error' : '' !!}">
                <label class="control-label" for="about"><strong>{!! trans('global.Validity End') !!}</strong></label>
                <input class="form-control datepicker" name="claim[preliminary][validity_end]" data-validate="required" data-mask="date"
                       data-format="dd-mm-yyyy" placeholder="dd/mm/yyyy" aria-required="true"
                       aria-invalid="false" value="{!! $claimPreliminary['validity_end'] !!}"
                       @if(!$editable)
                           disabled
                       @endif />
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group {!! $errors->first('claim.preliminary.third_party') ? 'has-error' : '' !!}">
                <strong>{!! trans('global.Third party people involved') !!}</strong>

                <p>
                    <label class="radio-inline">
                        <input type="radio"
                               name="claim[preliminary][third_party]"
                               value="yes" {!! $claimPreliminary['third_party'] == 'yes'  ? 'checked' : '' !!}
                               @if(!$editable)
                                   disabled
                               @endif>
                        {!! trans('global.Yes') !!}
                    </label>
                    <label class="radio-inline">
                        <input type="radio"
                               name="claim[preliminary][third_party]"
                               value="no" {!! $claimPreliminary['third_party'] == 'no'  ? 'checked' : '' !!}
                               @if(!$editable)
                                   disabled
                               @endif>
                        {!! trans('global.No') !!}
                    </label>
                </p>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group {!! $errors->first('claim.preliminary.cover') ? 'has-error' : '' !!}">
                <strong>{!! trans('global.Cover') !!}</strong>

                <p>
                    <label class="radio-inline">
                        <input type="radio"
                               name="claim[preliminary][cover]"
                               value="yes" {!! $claimPreliminary['cover'] == 'yes'  ? 'checked' : '' !!}
                               @if(!$editable)
                                   disabled
                               @endif>
                        {!! trans('global.Yes') !!}
                    </label>
                    <label class="radio-inline">
                        <input type="radio"
                               name="claim[preliminary][cover]"
                               value="no" {!! $claimPreliminary['cover'] == 'no'  ? 'checked' : '' !!}
                               @if(!$editable)
                                   disabled
                               @endif>
                        {!! trans('global.No') !!}
                    </label>
                </p>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group {!! $errors->first('claim.preliminary.amount_loss') ? 'has-error' : '' !!}">
                <label><strong>{!! trans('global.Amount Loss (create)') !!}</strong></label>
                <input
                        type="text"
                        class="form-control"
                        name="claim[preliminary][amount_loss]"
                        data-validate="required"
                        value="{!! $claimPreliminary['amount_loss'] !!}"
                        {!! !empty($disabled) ? 'disabled' : '' !!}
                        @if(!$editable)
                            disabled
                        @endif />
            </div>
        </div>

        <div class="col-md-12">
            <div class="form-group {!! $errors->first('claim.preliminary.description') ? 'has-error' : '' !!}">
                <label class="control-label" for="about"><strong>{!! trans('global.Description') !!}</strong></label>
                <textarea id="preliminary-description" rows="3" class="form-control autogrow" name="claim[preliminary][description]" data-validate="required"
                          placeholder="{!! trans('global.Description') !!}"
                          @if(!$editable)
                              disabled
                          @endif >{!! $claimPreliminary['description'] !!}
                </textarea>
            </div>
        </div>

        @if($editable)
        <div class="col-md-12 white-row">
            <button type="submit" class="btn btn-primary btn-single">
                <?php echo trans('global.Save'); ?>

            </button>
        </div>
        @endif

    </div>

    <script src="{!! asset('assets/js/wysihtml5/wysihtml5-angular.js') !!}"></script>
    @if($editable)
    <script>
        $('#preliminary-description').wysihtml5({
            "font-styles": true, //Font styling, e.g. h1, h2, etc. Default true
            "emphasis": true, //Italics, bold, etc. Default true
            "lists": true, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
            "html": false, //Button which allows you to edit the generated HTML. Default false
            "link": false, //Button to insert a link. Default true
            "image": false, //Button to insert an image. Default true,
            "color": false //Button to change color of font
        });
    </script>
    @else
    <script>
        $('#preliminary-description').wysihtml5({
            "font-styles": false, //Font styling, e.g. h1, h2, etc. Default true
            "emphasis": false, //Italics, bold, etc. Default true
            "lists": false, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
            "html": false, //Button which allows you to edit the generated HTML. Default false
            "link": false, //Button to insert a link. Default true
            "image": false, //Button to insert an image. Default true,
            "color": false //Button to change color of font
        });
    </script>
    @endif
</form>