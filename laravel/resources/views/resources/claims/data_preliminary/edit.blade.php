@include('resources.claims.data_preliminary.form', [
    'action' => route('claims.dataPreliminary.update', [$claim, $claim->dataPreliminary]),
    'claimPreliminary' => old('claim.data_preliminary') ? old('claim.data_preliminary') : $claim->dataPreliminary
])
