@include('resources.claims.data_preliminary.form', [
    'action' => route('claims.dataPreliminary.store', [$claim]),
    'claimPreliminary' => [
        'visit_date' => old('claim.data_preliminary.visit_date') ? old('claim.data_preliminary.visit_date') : \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $claim->dataVisits()->orderBy('date', 'DESC')->first()->date)->format('d/m/Y H:i'),
        'claim_date' => old('claim.data_preliminary.claim_date') ? old('claim.data_preliminary.claim_date') : \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $claim->occurred_at)->format('d/m/Y H:i'),
        'validity_start' => old('claim.data_preliminary.validity_start') ? old('claim.data_preliminary.validity_start') : \Carbon\Carbon::createFromFormat('Y-m-d', $claim->dataVerification()->first()->validity_start)->format('d/m/Y'),
        'validity_end' => old('claim.data_preliminary.validity_end') ? old('claim.data_preliminary.validity_end') : \Carbon\Carbon::createFromFormat('Y-m-d', $claim->dataVerification()->first()->validity_end)->format('d/m/Y'),
        'third_party' => old('claim.data_preliminary.third_party', ''),
        'cover' => old('claim.data_preliminary.cover', ''),
        'description' => old('claim.data_preliminary.description', ''),
        'amount_loss' => old('claim.data_preliminary.amount_loss') ? old('claim.data_preliminary.amount_loss') : $claim->amount_loss,
    ]
])
