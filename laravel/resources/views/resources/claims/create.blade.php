@extends('layouts.master')

@section('content')

    <div class="page-title">

        <div class="title-env">
            <h1 class="title">{!! trans('global.Add New Claim') !!}</h1>

            <p class="description">{!! trans('global.Create claim subtitle') !!}</p>
        </div>

        <div class="breadcrumb-env">
            {!! Breadcrumbs::render('claims.create') !!}
        </div>

    </div>

    <form role="form" id="rootwizard" method="post" action="{!! route('claims.store') !!}" class="form-wizard validate" novalidate>
        {!! csrf_field() !!}
        <ul class="tabs">
            <li class="active">
                <a href="#fwv-1" data-toggle="tab">
                    {!! trans('global.Claim basic info') !!}
                    <span>1</span>
                </a>
            </li>
            <li>
                <a href="#fwv-2" data-toggle="tab">
                    {!! trans('global.Contacts') !!}
                    <span>2</span>
                </a>
            </li>
            <li>
                <a href="#fwv-3" data-toggle="tab">
                    {!! trans('global.Address') !!}
                    <span>3</span>
                </a>
            </li>
            <li>
                <a href="#fwv-4" data-toggle="tab">
                    {!! trans('global.Survey of the affected cover') !!}
                    <span>4</span>
                </a>
            </li>
        </ul>

        <div class="progress-indicator">
            <span></span>
        </div>

        <div class="tab-content no-margin">

            <!-- Tabs Content -->
            <div class="tab-pane with-bg active" id="fwv-1">
                @include('resources.claims.create-form')
            </div>

            <div class="tab-pane with-bg" id="fwv-2">])
                @include('resources.claims.contacts.form', [
                        'bagName' => 'claim_contacts',
                        'name' => old('name'),
                        'phone' => old('phone'),
                        'email' => old('email'),
                        'type' => old('type'),
                        'description' => old('description')
                    ])
            </div>

            <div class="tab-pane with-bg" id="fwv-3">
                @include('resources.claims.addresses.form', [
                    'claimAddress' => old('claim.address')
                ])
            </div>

            <div class="tab-pane with-bg" id="fwv-4">
                @include('resources.claims.initial-survey', ['surveys' => $surveys])
            </div>

            <div class="col-md-12 white-row">
                <button type="submit" class="btn btn-primary btn-single">
                    {!! trans('global.Save') !!}
                </button>
            </div>

        </div>

    </form>


@endsection

@section('pageScripts')
    <script src="{!! asset('assets/js/jquery-validate/jquery.validate.min.js') !!}"></script>
    <script src="{!! asset('assets/js/inputmask/jquery.inputmask.bundle.js') !!}"></script>
    <script src="{!! asset('assets/js/formwizard/jquery.bootstrap.wizard.min.js') !!}"></script>
    <script src="{!! asset('assets/js/datepicker/bootstrap-datepicker.js') !!}"></script>
    <script src="{!! asset('assets/js/timepicker/bootstrap-timepicker.min.js') !!}"></script>b
    <script src="{!! asset('assets/js/multiselect/js/jquery.multi-select.js') !!}"></script>
    <script src="{!! asset('assets/js/jquery-ui/jquery-ui.min.js') !!}"></script>
    <script src="{!! asset('assets/js/selectboxit/jquery.selectBoxIt.min.js') !!}"></script>
    <script src="{!! asset('assets/js/app-geolocation.js') !!}"></script>
@endsection