<div class="modal {!! old('modal_old') == 'modal-claims-update-survey' ? 'modal_old' : '' !!}"
     id="modal-claims-update-survey">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" method="POST" action="{!! route('claims.surveys.store', $claim) !!}" class="validate"
                  novalidate>
                {!! csrf_field() !!}

                <input type="hidden" name="modal_old" value="modal-claims-update-survey">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{!! trans('global.Ike Additional Data') !!}</h4>
                </div>

                <div class="modal-body">
                    @include('resources.claims.form-survey')
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">
                        {!! trans('global.Close') !!}
                    </button>
                    <button type="submit" class="btn btn-success">
                        {!! trans('global.Save') !!}
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>