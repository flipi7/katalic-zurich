@extends('resources.claims.contacts.modal', [
    'modalId' => 'modal-claims-contacts-' . $contact->id,
    'action' => route('claims.contacts.update', [$claim, $contact]),
    'section' => 'modal-body-contacts-edit-' . $contact->id
])

@section('modal-body-contacts-edit-' . $contact->id)
    {!! method_field('PUT') !!}
    @include('resources.claims.contacts.form', [
        'bagName' => 'claim_contacts',
        'name' => old('claim.contact.name', $contact->name),
        'phone' => old('claim.contact.phone', $contact->phone),
        'email' => old('claim.contact.email', $contact->email),
        'type' => old('claim.contact.type', $contact->type),
        'description' => old('claim.contact.description', $contact->description)
    ])
@endsection