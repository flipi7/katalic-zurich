<div class="modal {!! $errors->claim_contacts->isEmpty() ? '' : 'js-modal-error' !!}">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" method="POST" action="{!! session('formResponse')['route'] !!}"
                  class="validate" novalidate>
                {!! csrf_field() !!}
                {!! method_field(session('formResponse')['method']) !!}

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{!! trans('global.Create New Contact') !!}</h4>
                </div>

                <div class="modal-body">

                    @include('resources.claims.contacts.form', [
                        'bagName' => 'claim_contacts',
                        'name' => old('name'),
                        'phone' => old('phone'),
                        'email' => old('email'),
                        'type' => old('type'),
                        'description' => old('description')
                    ])

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">
                        {!! trans('global.Close') !!}
                    </button>
                    <button type="submit" class="btn btn-success">
                        {!! trans('global.Create') !!}
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>