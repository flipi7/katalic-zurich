<div class="panel panel-default">
    <div class="panel-heading">
        {!! trans('global.Contacts') !!}
        @if($editable)
            <div class="panel-options">
                <a class="btn btn-single btn-gray" href="javascript:" data-toggle="modal"
                   data-target="#modal-claims-contacts">
                    {!! trans('global.Add New Contact') !!}
                </a>
            </div>
        @endif
    </div>

    <div class="panel-body">
        <!-- Show -->
        @include('resources.claims.contacts.table-list')

         <!-- Modal -->
        @include('resources.claims.contacts.create')
    </div>
</div>