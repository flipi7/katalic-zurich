@extends('resources.claims.contacts.modal', [
    'action' => route('claims.contacts.store', $claim),
    'section' => 'modal-body-contacts-create'
])

@section('modal-body-contacts-create')
    @include('resources.claims.contacts.form', [
        'bagName' => 'claim_contacts',
        'name' => old('claim.contact.name'),
        'phone' => old('claim.contact.phone'),
        'email' => old('claim.contact.email'),
        'type' => old('claim.contact.type'),
        'description' => old('claim.contact.description')
    ])
@endsection