<div class="row">

    <div class="col-md-4">
        <div class="form-group {!! $errors->{$bagName}->first('name') ? 'has-error' : '' !!}">
            <label class="control-label">{!! trans('global.Contact Name') !!}</label>
            <input class="form-control" name="claim[contact][name]" data-validate="required"
                   placeholder="{!! trans('global.Contact Name') !!}"
                   value="{!! $name !!}"
                    />
        </div>
    </div>

    <div class="col-md-4">
        <div class="form-group {!! $errors->{$bagName}->first('phone') ? 'has-error' : '' !!}">
            <label class="control-label">{!! trans('global.Contact Phone') !!}</label>
            <input class="form-control" name="claim[contact][phone]" data-validate="required"
                   placeholder="{!! trans('global.Contact Phone') !!}"
                   value="{!!$phone !!}"
                    />

        </div>
    </div>

    <div class="col-md-4">
        <div class="form-group {!! $errors->{$bagName}->first('email') ? 'has-error' : '' !!}">
            <label class="control-label">{!! trans('global.Contact Email') !!}</label>
            <input class="form-control" name="claim[contact][email]" data-validate="required"
                   placeholder="{!! trans('global.Contact Email') !!}"
                   value="{!! $email !!}"
                    />
        </div>
    </div>

</div>

<div class="row">

    <div class="col-md-4">
        <div class="form-group {!! $errors->{$bagName}->first('type') ? 'has-error' : '' !!}">
            <label class="control-label"
                   for="about">{!! trans('global.Contact Type') !!}</label>
            <select class="form-control" name="claim[contact][type]" data-validate="required">
                <option value="">{!! trans('global.Select Type') !!}</option>
                @foreach($contactTypes as $value => $name)
                    <option value="{!! $value !!}" {!! $value == $type ? 'selected' : '' !!}>{!! trans('global.'. $name) !!}</option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="col-md-8">
        <div class="form-group {!! $errors->{$bagName}->first('description') ? 'has-error' : '' !!}">
            <label class="control-label" for="about">{!! trans('global.Comment') !!}</label>
            <textarea rows="3" class="form-control autogrow" name="claim[contact][description]" data-validate="required"
                      placeholder="{!! trans('global.Comment') !!}">{!! $description !!}</textarea>
        </div>
    </div>

</div>