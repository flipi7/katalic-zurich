<table class="table table-hover members-table middle-align">
    <thead>
    <tr>
        <th>{!! trans('global.Name') !!}</th>
        <th>{!! trans('global.Type') !!}</th>
        <th>{!! trans('global.Phone') !!}</th>
        <th>{!! trans('global.Email') !!}</th>
        @if($editable)
        <th>{!! trans('global.Options') !!}</th>
        @endif
    </tr>
    </thead>
    <tbody>
    @if($claim->contacts->isEmpty())
        <tr>
            <td colspan="5">
                <strong>{!! trans('global.No contacts') !!}.</strong> {!! trans('global.Please create new contact') !!}.
            </td>
        </tr>
    @else
        @foreach($claim->contacts as $contact)
            <tr>
                <td>
                    {!! $contact->name !!}
                </td>
                <td>
                    {!! trans('global.' . $contact->type) !!}
                </td>
                <td>
                    {!! $contact->phone !!}
                </td>
                <td>
                    {!! $contact->email !!}
                </td>
                @if($editable)
                <td>
                    <a class="btn btn-xs btn-default" href="#" data-toggle="modal"
                       data-target="#modal-claims-contacts-{!! $contact->id !!}">
                        <i class="fa fa-pencil"></i>
                        {!! trans('global.Edit') !!}
                    </a>
                    {{--   <a href="{!! route('claims.edit', $claim) !!}" class="btn btn-xs btn-default">
                          <i class="fa fa-remove"></i>
                          {!! trans('global.Delete') !!}
                      </a>--}}
                    @include('resources.claims.contacts.edit')
                </td>
                @endif
            </tr>
        @endforeach
    @endif
    </tbody>
</table>
