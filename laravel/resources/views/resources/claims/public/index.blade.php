<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="description" content="Katalic"/>
    <meta name="author" content=""/>

    <title>Katalic</title>

    <link rel='shortcut icon' type='image/x-icon' href="{!! asset('assets/images/favicon.ico') !!}" />
    <link rel="stylesheet" href="{!! asset('assets/css/fonts/linecons/css/linecons.css') !!}">
    <link rel="stylesheet" href="{!! asset('assets/css/fonts/fontawesome/css/font-awesome.min.css') !!}">
    <link rel="stylesheet" href="{!! asset('assets/css/bootstrap.css') !!}">
    <link rel="stylesheet" href="{!! asset('assets/css/xenon-core.css') !!}">
    <link rel="stylesheet" href="{!! asset('assets/css/xenon-forms.css') !!}">
    <link rel="stylesheet" href="{!! asset('assets/css/xenon-components.css') !!}">
    <link rel="stylesheet" href="{!! asset('assets/css/xenon-skins.css') !!}">
    <link rel="stylesheet" href="{!! asset('assets/js/multiselect/css/multi-select.css') !!}">
    <link rel="stylesheet" href="{!! asset('assets/css/custom.css') !!}">
    <link rel="stylesheet" href="{!! asset('assets/js/wysihtml5/src/bootstrap-wysihtml5.css') !!}">


    <script src="{!! asset('assets/js/jquery-1.11.1.min.js') !!}"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="page-body">
<script>
    $app = {
        messages: {}
    };
</script>

<?php $editable = FALSE; ?>

<div class="page-container">

    <div class="public-bar">
        <div class="logo">
            <img src="{!! asset('assets/images/logo@2x.png') !!}" width="80" alt=""/>
        </div>
        <h1 class="title">{!! trans('global.Claim Information') !!}</h1>
    </div>

    <div class="public-content">
        
        <div class="row">
            <div class="col-md-1">
            </div>
            <div class="col-md-9">
                @include('resources.claims.status-flow', array('claimId' => $claim->id, 'status' => $status, 'latestStatus' => $latestStatus))
            </div>
        </div>

        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-7">
                <div class="tab-content">
                    <div class="tab-pane" id="tab-claim">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">{!! trans('global.Created') !!}</h3>
                                <div class="panel-options">
                                    <a href="#" data-toggle="panel"> <span class="collapse-icon">–</span> <span class="expand-icon">+</span> </a>
                                </div>
                            </div>
                            <div class="panel-body">
                                @include('resources.claims.status-created-verification')
                            </div>
                        </div>

                        @if(!empty($claim->dataContact()->first()))
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">{!! trans('global.Contact') !!}</h3>
                                    <div class="panel-options">
                                        <a href="#" data-toggle="panel"> <span class="collapse-icon">–</span> <span class="expand-icon">+</span> </a>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    @include('resources.claims.data_contact.index')
                                </div>
                            </div>
                        @endif

                        @if(!empty($claim->dataVisits()->first()))
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">{!! trans('global.Visit') !!}</h3>
                                    <div class="panel-options">
                                        <a href="#" data-toggle="panel"> <span class="collapse-icon">–</span> <span class="expand-icon">+</span> </a>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    @include('resources.claims.data_visit.index')
                                </div>
                            </div>
                        @endif
                    </div>

                    <div class="tab-pane" id="tab-note">
                        <!-- Notes -->
                        @include('resources.claims.notes.index', ['public' => TRUE])
                    </div>

                    <div class="tab-pane" id="tab-docs">
                        <!-- Documents -->
                        @include('resources.claims.documents.index', ['public' => TRUE])
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <a href="#tab-claim" data-toggle="tab" class="btn btn-block btn-primary btn-icon btn-icon-standalone btn-icon-standalone-right btn-tab">
                    <i class="fa fa-ambulance"></i>
                    <span>{!! trans('global.Claim') !!}</span>
                </a>

                <a href="#tab-note" data-toggle="tab" class="btn btn-block btn-primary btn-icon btn-icon-standalone btn-icon-standalone-right btn-tab">
                    <i class="fa fa-th-list"></i>
                    <span>{!! trans('global.Notes') !!}</span>
                </a>

                <a href="#tab-docs" data-toggle="tab" class="btn btn-block btn-primary btn-icon btn-icon-standalone btn-icon-standalone-right btn-tab">
                    <i class="fa fa-files-o"></i>
                    <span>{!! trans('global.Documents') !!}</span>
                </a>
            </div>
            <div class="col-md-1"></div>
        </div>
    </div>
</div>

{{--<!-- Page Loading Overlay -->--}}
{{--<div class="page-loading-overlay">--}}
    {{--<div class="loader-2"></div>--}}
{{--</div>--}}

<!-- Include App Messages -->
@include('layouts.messages')

<!-- Bottom Scripts -->
<script src="{!! asset('assets/js/bootstrap.min.js') !!}"></script>
<script src="{!! asset('assets/js/TweenMax.min.js') !!}"></script>
<script src="{!! asset('assets/js/resizeable.js') !!}"></script>
<script src="{!! asset('assets/js/joinable.js') !!}"></script>
<script src="{!! asset('assets/js/xenon-api.js') !!}"></script>
<script src="{!! asset('assets/js/xenon-toggles.js') !!}"></script>

<!-- Imported scripts on this page -->
<script src="{!! asset('assets/js/xenon-widgets.js') !!}"></script>
<script src="{!! asset('assets/js/toastr/toastr.min.js') !!}"></script>
<script src="{!! asset('assets/js/jquery-validate/jquery.validate.min.js') !!}"></script>
<script src="{!! asset('assets/js/inputmask/jquery.inputmask.bundle.js') !!}"></script>
<script src="{!! asset('assets/js/formwizard/jquery.bootstrap.wizard.min.js') !!}"></script>
<script src="{!! asset('assets/js/datepicker/bootstrap-datepicker.js') !!}"></script>
<script src="{!! asset('assets/js/multiselect/js/jquery.multi-select.js') !!}"></script>
<script src="{!! asset('assets/js/jquery-ui/jquery-ui.min.js') !!}"></script>
<script src="{!! asset('assets/js/selectboxit/jquery.selectBoxIt.min.js') !!}"></script>
<script src="{!! asset('assets/js/app-geolocation.js') !!}"></script>

<!-- JavaScripts initializations and stuff -->
<script src="{!! asset('assets/js/xenon-custom.js') !!}"></script>

<!-- JavaScripts App Custom -->
<script src="{!! asset('assets/js/app-custom.js') !!}"></script>
</body>
</html>