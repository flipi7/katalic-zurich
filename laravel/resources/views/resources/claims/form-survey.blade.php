<div class="row">

    <div class="col-md-12">
        <div class="form-group">
            <select id="surveyTemplate" name="claim[survey_template_id]" class="selectboxit"
                    data-validate="required">
                <option value="0">{!! trans('global.Select Survey Template') !!}</option>
                @foreach($claim->surveys as $key => $survey)
                    <option value="{!! $key + 1 !!}" {!! old('claim.survey_template_id') == $survey->id  ? 'selected' : '' !!}>{!! $survey->name !!}</option>
                @endforeach
            </select>

            <ul class="nav nav-tabs hide" id="surveyTab">
                <li><a href="#template_0">test</a></li>
                @foreach($claim->surveys as $survey)
                    <li><a href="#template_{!! $survey->id !!}">{!! $survey->name !!}</a></li>
                @endforeach
            </ul>

        </div>
    </div>

</div>


<div class="row">

    <div class="col-md-12">
        <div class="tab-content">
            <div class="tab-pane" id="template_0"></div>
            @foreach($claim->surveys as $survey)
                <div class="tab-pane"
                     id="template_{!! $survey->id !!}">
                    @foreach($survey->questions as $question)
                        <div class="form-group">
                            <strong>{!! $question->name !!}</strong>

                            <p>
                                <label class="radio-inline">
                                    @if($claim->surveyAnswers->where('pivot.question_id',$question->id)->first())
                                        <input type="radio"
                                               name="claim[surveys][{!! $survey->id !!}][{!! $question->id !!}]"
                                               value="yes" {!! old('claim.survey.'.$survey->id.'.'.$question->id, $claim->surveyAnswers->where('pivot.question_id',$question->id)->first()->pivot->answer) == 'yes'  ? 'checked' : '' !!}>
                                        {!! trans('global.Yes') !!}
                                    @else
                                        <input type="radio"
                                               name="claim[surveys][{!! $survey->id !!}][{!! $question->id !!}]"
                                               value="yes" {!! old('claim.survey.'.$survey->id.'.'.$question->id) == 'yes'  ? 'checked' : '' !!}>
                                        {!! trans('global.Yes') !!}
                                    @endif
                                </label>
                                <label class="radio-inline">
                                    @if($claim->surveyAnswers->where('pivot.question_id',$question->id)->first())
                                        <input type="radio"
                                               name="claim[surveys][{!! $survey->id !!}][{!! $question->id !!}]"
                                               value="no" {!! old('claim.survey.'.$survey->id.'.'.$question->id, $claim->surveyAnswers->where('pivot.question_id',$question->id)->first()->pivot->answer) == 'no'  ? 'checked' : '' !!}>
                                        {!! trans('global.No') !!}
                                    @else
                                        <input type="radio"
                                               name="claim[surveys][{!! $survey->id !!}][{!! $question->id !!}]"
                                               value="no" {!! old('claim.survey.'.$survey->id.'.'.$question->id) == 'no'  ? 'checked' : '' !!}>
                                        {!! trans('global.No') !!}
                                    @endif
                                </label>
                            </p>
                        </div>
                    @endforeach
                </div>
            @endforeach
        </div>
    </div>
</div>


<script>
    $('#surveyTemplate').on('change', function (e) {
        $('#surveyTab li a').eq($(this).val()).tab('show');
    });
</script>
