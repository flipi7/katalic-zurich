<div class="row">

    <div class="col-md-12">
        <div class="form-group">
            <strong>{!! trans('global.Contact') !!}</strong>
            <p>
                <label class="radio-inline">
                    <input type="radio" name="claim[contact][contact]" value="yes" {!! $claimContact['contact'] == 'yes' ? 'checked' : '' !!} {!! !empty($disabled) ? 'disabled' : '' !!}>
                    {!! trans('global.Yes') !!}
                </label>
                <label class="radio-inline">
                    <input type="radio" name="claim[contact][contact]" value="no" {!! $claimContact['contact'] == 'no' ? 'checked' : '' !!} {!! !empty($disabled) ? 'disabled' : '' !!}>
                    {!! trans('global.No') !!}
                </label>
            </p>
        </div>
    </div>

    <div class="col-md-12">
        <div class="form-group {!! $errors->first('claim.contact.visit_date') ? 'has-error' : '' !!}">
            <label class="control-label" for="about">{!! trans('global.Visit Date') !!}</label>
            <input class="form-control" name="claim[contact][visit_date]" data-validate="required" data-mask="datetime"
                   placeholder="dd/mm/yyyy hh:mm" aria-required="true"
                   aria-invalid="false"
                   value="{!! $claimContact['visit_date'] !!}"
                    >
        </div>
    </div>

    <div class="col-md-12">
        <div class="form-group {!! $errors->first('claim.contact.note') ? 'has-error' : '' !!}">
            <label class="control-label" for="street">
                {!! trans('global.Comments') !!}
            </label>
            <textarea class="form-control"
                   name="claim[contact][note]"
                   data-validate="required"
                   placeholder="{!! trans('global.Comments') !!}"
                    {!! !empty($disabled) ? 'disabled' : '' !!}
            >{!! $claimContact['note'] !!}</textarea>
        </div>
    </div>
</div>