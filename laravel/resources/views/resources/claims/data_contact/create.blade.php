@extends('resources.claims.data_contact.modal', [
    'action' => route('claims.dataContact.store', [$claim])
])

@section('modal-contact-body')
    @include('resources.claims.data_contact.form', [
        'claimContact' => old('claim.contact') ? old('claim.contact') : ['contact' => '', 'visit_date' => '', 'note' => '']
    ])
@endsection