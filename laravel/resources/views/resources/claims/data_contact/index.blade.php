<div class="panel panel-default">
    <div class="panel-heading">
        {!! trans('global.Contact') !!}
        @if($editable)
            <div class="panel-options">
                <a class="btn btn-single btn-gray" data-toggle="modal" data-target="#modal-claims-contact">{!! trans('global.Create') !!}</a>
            </div>
        @endif
    </div>

    <div class="panel-body">

        <!-- Show -->
        <table class="table responsive">
            <thead>
            <tr>
                <th>{!! trans('global.Contact') !!}</th>
                <th>{!! trans('global.Visit Date') !!}</th>
                <th>{!! trans('global.Comments') !!}</th>
            </tr>
            </thead>
            <tbody>
            @foreach($claim->dataContact as $statusContact)
                <tr>
                    <td>
                        <i class="{!! $statusContact->contact == 'yes' ? 'fa fa-check text-success' : 'fa fa-close text-danger' !!}"></i>
                        ({!! ucfirst($statusContact->contact) !!})
                    </td>
                    <td>{!! $statusContact->visit_date !!}</td>
                    <td>{!! $statusContact->note !!}</td>
                </tr>
            @endforeach
            </tbody>
        </table>

        <!-- Modal -->
        @include('resources.claims.data_contact.create')
    </div>
</div>