@extends('resources.claims.data_contact.modal', [
    'action' => route('claims.contact.update', [$claim, $claim->statusContact])
])

@section('modal-contact-body')
    {!! method_field('PUT') !!}
    @include('resources.claims.data_contact.form', [
        'claimContact' => old('claim.contact') ? old('claim.contact') : $claim->statusContact->toArray()
    ])
@endsection