@if(!empty($claim->statusContact->id))
    @include('resources.claims.data_contact.form', [
        'disabled' => true,
        'claimContact' => $claim->statusContact->toArray()
    ])
@else
    <div class="alert alert-danger">{!! trans('global.No data provided') !!}</div>
@endif