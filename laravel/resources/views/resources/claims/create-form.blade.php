<div class="row">

    <div class="col-md-6">
        <div class="form-group {!! $errors->first('claim.policy_number') ? 'has-error' : '' !!}">
            <label class="control-label">{!! trans('global.Policy Number') !!}</label>
            <input class="form-control" name="claim[policy_number]" data-validate="required"
                   placeholder="{!! trans('global.Policy Number') !!}"
                   value="{!! old('claim.policy_number') !!}"
            />
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group {!! $errors->first('claim.policy_holder') ? 'has-error' : '' !!}">
            <label class="control-label">{!! trans('global.Policy Holder') !!}</label>
            <input class="form-control" name="claim[policy_holder]" data-validate="required"
                   placeholder="{!! trans('global.Policy Holder') !!}"
                   value="{!! old('claim.policy_holder') !!}"
            />
        </div>
    </div>

</div>

<div class="row">

    <div class="col-md-12">
        <div class="form-group {!! $errors->first('claim.description') ? 'has-error' : '' !!}">
            <label class="control-label" for="about">{!! trans('global.Description') !!}</label>
            <textarea class="form-control autogrow" name="claim[description]"
                      data-validate="required" rows="5"
                      placeholder="{!! trans('global.Description') !!}">{!! old('claim.description') !!}</textarea>
        </div>
    </div>

</div>

<div class="row">

    <div class="col-md-6">
        <div class="form-group {!! $errors->first('claim.amount_loss') ? 'has-error' : '' !!}">
            <label class="control-label">{!! trans('global.Amount Loss (create)') !!}</label>
            <input class="form-control" name="claim[amount_loss]" data-validate="required"
                   placeholder="{!! trans('global.Amount Loss (create)') !!}"
                   value="{!! old('claim.amount_loss') !!}"
            />
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group {!! $errors->first('claim.currency') ? 'has-error' : '' !!}">
            <label class="control-label">{!! trans('global.Currency') !!}</label>
            <select class="form-control" name="claim[currency]" data-validate="required">
                <option value="">{!! trans('global.Select Currency') !!}</option>
                <option value="mxn" {!! old('claim.currency') == 'mxn' ? 'selected' : '' !!}>$ MXN</option>
                <option value="usd" {!! old('claim.currency') == 'usd' ? 'selected' : '' !!}>$ USD</option>
            </select>
        </div>
    </div>

</div>

<div class="row">

    <div class="col-md-6">
        <div class="form-group {!! $errors->first('claim.ike_reference') ? 'has-error' : '' !!}">
            <label class="control-label">{!! trans('global.IKE Reference') !!}</label>
            <input class="form-control" name="claim[ike_reference]" data-validate="required"
                   placeholder="{!! trans('global.IKE Reference') !!}"
                   value="{!! old('claim.ike_reference') !!}"
            />
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group {!! $errors->first('claim.adjuster_id') ? 'has-error' : '' !!}">
            <label class="control-label">{!! trans('global.Adjuster') !!}</label>
            <select class="form-control" name="claim[adjuster_id]" data-validate="required">
                <option value="">{!! trans('global.Select Adjuster') !!}</option>
                @foreach($adjusters as $adjuster)
                    <option value="{!! $adjuster->id !!}" {!! old('claim.adjuster_id') == $adjuster->id  ? 'selected' : '' !!}>{!! $adjuster->name !!}</option>
                @endforeach
            </select>
        </div>
    </div>

</div>

<div class="row">

    <div class="col-md-6">
        <div class="form-group {!! $errors->first('claim.occurred') ? 'has-error' : '' !!}">
            <label class="control-label" for="about">{!! trans('global.Claim date occurrence') !!}</label>
            <input class="form-control" name="claim[occurred]" data-validate="required" data-mask="datetime"
                   placeholder="dd/mm/yyyy hh:mm" aria-required="true" aria-describedby="birthdate-error"
                   aria-invalid="false"
                   value="{!! old('claim.occurred_at', old('claim.occurred_at') ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', old('claim.occurred_at')) : null) !!}"
            >
        </div>
    </div>

</div>



