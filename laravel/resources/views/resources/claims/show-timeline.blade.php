<ul class="cbp_tmtimeline activities-timeline">

    @foreach($claim->activities as $activity)
        <?php $attach = $activity->getAttachment(); ?>

        @if($attach != null)
        <li>
            <time class="cbp_tmtime" datetime="2014-12-06T03:45">
                <span>{!! trans('global.' . ucfirst($activity->activity)) !!}</span>
                <span>{!! $activity->date->diffForHumans() !!}</span>
            </time>
            @if($activity->activity == 'note')
                <div class="cbp_tmicon timeline-bg-gray timeline-claim {!! $activity->activity !!}">
                    <i class="fa-th-list"></i>
                </div>
                <div class="cbp_tmlabel">
                    <h2><span>{!! trans('global.Note') !!} {!! trans('global.added by') !!} <a href="#">@if($attach->user == null){!! trans('global.Insured') !!}@else{!! $activity->user->name !!}@endif</a>:</span></h2>
                    <p>{!! $attach->description !!}</p>
                    @if(!empty($attach->attachmentName))
                        <p>Adjunto: </p><a href="{!! '/static/' . $attach->attachmentHash . '-' . $attach->attachmentName !!}" download target="_blank">{!! $attach->attachmentName !!}</a>
                    @endif
                </div>
            @elseif($activity->activity == 'document')
                <div class="cbp_tmicon timeline-bg-gray timeline-claim {!! $activity->activity !!}">
                    <i class="fa-files-o"></i>
                </div>
                <div class="cbp_tmlabel">
                    <h2><span><a href="#">@if($attach->user == null){!! trans('global.Insured') !!}@else{!! $activity->user->name !!}@endif</a> {!! trans('global.added') !!} {!! trans('global.a document') !!}: {!! $attach->description !!}</span></h2>
                    @if(!empty($attach->attachmentName))
                        <p>Adjunto: </p><a href="{!! '/static/' . $attach->attachmentHash . '-' . $attach->attachmentName !!}" download target="_blank">{!! $attach->attachmentName !!}</a>
                    @endif
                </div>
            @elseif($activity->activity == 'update')
                <div class="cbp_tmicon timeline-bg-gray timeline-claim {!! $activity->activity !!}">
                    <i class="fa-pencil"></i>
                </div>
                <div class="cbp_tmlabel empty">
                    <span><a href="#">{!! $activity->user->name !!}</a> {!! trans('global.updated') !!} {!! trans('global.the '.$attach) !!}.</span>
                </div>
            @elseif($activity->activity == 'add')
                <div class="cbp_tmicon timeline-bg-gray timeline-claim {!! $activity->activity !!}">
                    <i class="fa-plus-square-o"></i>
                </div>
                <div class="cbp_tmlabel empty">
                    <span><a href="#">{!! $activity->user->name !!}</a> {!! trans('global.added') !!} {!! trans('global.a '.$attach) !!}.</span>
                </div>
            @elseif($activity->activity == 'delete')
                <div class="cbp_tmicon timeline-bg-gray timeline-claim {!! $activity->activity !!}">
                    <i class="fa-minus-square-o"></i>
                </div>
                <div class="cbp_tmlabel empty">
                    <span><a href="#">{!! $activity->user->name !!}</a> {!! trans('global.deleted') !!} {!! trans('global.a '.$attach) !!}.</span>
                </div>
            @elseif($activity->activity == 'documentation')
                <div class="cbp_tmicon timeline-bg-gray timeline-claim {!! $activity->activity !!}">
                    <i class="fa-files-o"></i>
                </div>
                <div class="cbp_tmlabel">
                    <span>{!! trans('global.Documentation added by') !!} <a href="#">{!! $activity->user->name !!}</a>.</span>
                </div>
            @elseif($activity->activity == 'created')
                <div class="cbp_tmicon timeline-bg-gray timeline-claim {!! $activity->activity !!}">
                    <i class="fa-plus"></i>
                </div>
                <div class="cbp_tmlabel empty">
                    <span>{!! trans('global.Claim was created by') !!} <a href="#">{!! $activity->user->name !!}</a>.</span>
                </div>
            @elseif($activity->activity == 'verification')
                <div class="cbp_tmicon timeline-bg-gray timeline-claim {!! $activity->activity !!}">
                    <i class="fa-search"></i>
                </div>
                <div class="cbp_tmlabel">
                    <h2><span>{!! trans('global.Claim was verified by') !!} <a href="#">{!! $activity->user->name !!}</a>.</span></h2>
                    <p>{!! trans('global.Zurich Claim Number') !!}: {!! $claim->zurich_claim_number !!}.</p>
                </div>
            @elseif($activity->activity == 'contact')
                <div class="cbp_tmicon timeline-bg-gray timeline-claim {!! $activity->activity !!}">
                    <i class="fa-phone"></i>
                </div>
                <?php $visit_date = explode(" ", \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $attach->visit_date)->format('d/m/Y H:i')); ?>
                @if($attach->contact == 'yes')
                    <div class="cbp_tmlabel">
                        <h2><span><a href="#">{!! $activity->user->name !!}</a> {!! trans('global.has set a visit to') !!} <a href="#">{!! $visit_date[0] !!} {!! trans('global.at') !!} {!! $visit_date[1] !!}</a>.</span></h2>
                        <p>{!! $attach->note !!}</p>
                    </div>
                @else
                    <div class="cbp_tmlabel">
                        <h2><span><a href="#">{!! $activity->user->name !!}</a> {!! trans('global.was not able to make contact on') !!} <a href="#">{!! $visit_date[0] !!} {!! trans('global.at') !!} {!! $visit_date[1] !!}</a>.</span></h2>
                        <p>{!! $attach->note !!}</p>
                    </div>
                @endif
            @elseif($activity->activity == 'visit')
                <div class="cbp_tmicon timeline-bg-gray timeline-claim {!! $activity->activity !!}">
                    <i class="fa-sign-in"></i>
                </div>
                <div class="cbp_tmlabel">
                    <h2><span><a href="#">{!! $activity->user->name !!}</a> {!! trans('global.has visited the claim') !!}:</span></h2>
                    <p>{!! $attach->description !!}</p>
                </div>
            @elseif($activity->activity == 'preliminary')
                <div class="cbp_tmicon timeline-bg-gray timeline-claim {!! $activity->activity !!}">
                    <i class="fa-mortar-board"></i>
                </div>
                <div class="cbp_tmlabel">
                    <h2><span><a href="#">{!! $activity->user->name !!}</a> {!! trans('global.wrote a preliminary report') !!}:</span></h2>
                    {!! $attach->description !!}
                </div>
            @elseif($activity->activity == 'report')
                <div class="cbp_tmicon timeline-bg-gray timeline-claim {!! $activity->activity !!}">
                    <i class="fa-file-text-o"></i>
                </div>
                <div class="cbp_tmlabel">
                    <h2><span><a href="#">{!! $activity->user->name !!}</a> {!! trans('global.wrote a report') !!}.</span></h2>
                    {!! $attach->claim_report !!}
                </div>
            @elseif($activity->activity == 'processing')
                <div class="cbp_tmicon timeline-bg-gray timeline-claim {!! $activity->activity !!}">
                    <i class="fa-exchange"></i>
                </div>
                <div class="cbp_tmlabel empty">
                    <span><a href="#">{!! $activity->user->name !!}</a> {!! trans('global.has started processing the claim') !!}.</span>
                </div>
            @elseif($activity->activity == 'finished')
                <div class="cbp_tmicon timeline-bg-gray timeline-claim {!! $activity->activity !!}">
                    <i class="fa-check-square-o"></i>
                </div>
                <div class="cbp_tmlabel empty">
                    <span><a href="#">{!! $activity->user->name !!}</a> {!! trans('global.has finished the claim') !!}.</span>
                </div>
            @elseif($activity->activity == 'cancelled')
                <div class="cbp_tmicon timeline-bg-gray timeline-claim {!! $activity->activity !!}">
                    <i class="fa-close"></i>
                </div>
                <div class="cbp_tmlabel empty">
                    <span>{!! trans('global.The claim has been cancelled') !!} {!! trans('global.by') !!} <a href="#">{!! $activity->user->name !!}</a>.</span>
                </div>
            @else
                <div class="cbp_tmicon timeline-bg-gray timeline-claim {!! $activity->activity !!}">
                    <i class="fa-user"></i>
                </div>
                <div class="cbp_tmlabel empty">
                    <span>{!! trans('global.Claim changed status to') !!} {!! trans('global.' . ucfirst($activity->activity)) !!} {!! trans('global.by') !!} <a href="#">{!! $activity->user->name !!}</a>.</span>
                </div>
            @endif
        </li>
        @endif
    @endforeach
</ul>
