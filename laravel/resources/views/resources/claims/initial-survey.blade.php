<div class="row">

    <div class="col-md-12">
        <div class="form-group">
            <select id="surveyTemplate" name="claim[survey_template_id]" class="selectboxit"
                    data-validate="required">
                <option value="0">{!! trans('global.Select Survey Template') !!}</option>
                @foreach($surveys as $key => $survey)
                    <option value="{!! $key + 1 !!}" {!! old('claim.survey_template_id') == $survey->id  ? 'selected' : '' !!}>{!! $survey->name !!}</option>
                @endforeach
            </select>

            <ul class="nav nav-tabs hide" id="surveyTab">
                <li><a href="#template_0">test</a></li>
                @foreach($surveys as $survey)
                    <li><a href="#template_{!! $survey->id !!}">{!! $survey->name !!}</a></li>
                @endforeach
            </ul>

        </div>
    </div>

</div>


<div class="row">

    <div class="col-md-12">
        <div class="tab-content">
            <div class="tab-pane" id="template_0"></div>
            @foreach($surveys as $survey)
                <div class="tab-pane"
                     id="template_{!! $survey->id !!}">
                    @foreach($survey->questions as $question)
                        <div class="form-group">
                            <strong>{!! $question->name !!}</strong>

                            <p>
                                <label class="radio-inline">
                                    <input type="radio"
                                           name="claim[surveys][{!! $survey->id !!}][{!! $question->id !!}]"
                                           value="yes" {!! old('claim.survey.'.$survey->id.'.'.$question->id) == 'yes'  ? 'checked' : '' !!}>
                                    {!! trans('global.Yes') !!}
                                </label>
                                <label class="radio-inline">
                                    <input type="radio"
                                           name="claim[surveys][{!! $survey->id !!}][{!! $question->id !!}]"
                                           value="no" {!! old('claim.survey.'.$survey->id.'.'.$question->id) == 'no'  ? 'checked' : '' !!}>
                                    {!! trans('global.No') !!}
                                </label>
                            </p>
                        </div>
                    @endforeach
                </div>
            @endforeach
        </div>
    </div>
</div>


<script>
    $('#surveyTemplate').on('change', function (e) {
        $('#surveyTab li a').eq($(this).val()).tab('show');
    });
</script>