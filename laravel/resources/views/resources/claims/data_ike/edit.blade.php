@extends('resources.claims.data_ike.modal', [
    'action' => route('claims.dataIke.update', [$claim, $claim->dataIke])
])

@section('modal-ike-body')
    {!! method_field('PUT') !!}
    @include('resources.claims.data_ike.form', [
        'ike' => old('claim.ike') ? old('claim.ike') : $claim->dataIke->toArray()
    ])
@endsection