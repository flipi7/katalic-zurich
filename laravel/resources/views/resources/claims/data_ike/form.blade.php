<div class="row">

    <div class="col-md-6">
        <div class="form-group {!! $errors->first('claim.ike.reference') ? 'has-error' : '' !!}">
            <label class="control-label" for="street">
                {!! trans('global.Ike Reference') !!}
            </label>
            <input class="form-control"
                   name="claim[ike][reference]"
                   data-validate="required"
                   placeholder="{!! trans('global.Reference') !!}"
                   value="{!! $ike['reference'] !!}"
                    {!! !empty($disabled) ? 'disabled' : '' !!}
                    />
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group {!! $errors->first('claim.ike.adjuster_id') ? 'has-error' : '' !!}">
            <label>{!! trans('global.Adjuster') !!}</label>
            <select class="form-control" name="claim[ike][adjuster_id]" data-validate="required" {!! !empty($disabled) ? 'disabled' : '' !!}>
                <option value="">{!! trans('global.Select Adjuster') !!}</option>
                @foreach($adjusters as $adjuster)
                    <option value="{!! $adjuster->id !!}" {!! $ike['adjuster_id'] == $adjuster->id  ? 'selected' : '' !!}>{!! $adjuster->name !!}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>