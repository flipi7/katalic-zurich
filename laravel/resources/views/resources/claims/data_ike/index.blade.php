<div class="panel panel-default">
    <div class="panel-heading">
        {!! trans('global.Ike Additional Data') !!}
        @if($editable)
            <div class="panel-options">
                <a class="btn btn-single btn-gray" data-toggle="modal" data-target="#modal-claims-ike">Edit</a>
            </div>
        @endif
    </div>

    <div class="panel-body">

        <!-- Show -->
        @include('resources.claims.data_ike.show')


        <!-- Modal -->
        @if(!empty($claim->dataIke->id))
            @include('resources.claims.data_ike.edit')
        @else
            @include('resources.claims.data_ike.create')
        @endif
    </div>
</div>