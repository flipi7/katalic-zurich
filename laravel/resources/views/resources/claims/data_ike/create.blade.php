@extends('resources.claims.data_ike.modal', [
    'action' => route('claims.dataIke.store', [$claim])
])

@section('modal-ike-body')
    @include('resources.claims.data_ike.form', [
        'ike' => old('claim.ike') ? old('claim.ike') : ['reference' => '', 'adjuster_id' => '']
    ])
@endsection