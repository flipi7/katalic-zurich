<table class="table table-hover members-table middle-align">
    <thead>
    <tr>
        <th></th>
        <th class="hidden-xs hidden-sm"></th>
        <th>{!! trans('global.Name and Role') !!}</th>
        <th class="hidden-xs hidden-sm">{!! trans('global.Email') !!}</th>
        <th>ID</th>
        <th>{!! trans('global.Options') !!}</th>
    </tr>
    </thead>
    <tbody>
    @foreach($users as $user)
        <tr>
            <td class="user-cb">
                <input type="checkbox" class="cbr" name="members-list[]" value="1" checked/>
            </td>
            <td class="user-image hidden-xs hidden-sm">
                <a href="#">
                    <img src="{!! asset('assets/images/user-1.png') !!}" class="img-circle" alt="user-pic"/>
                </a>
            </td>
            <td class="user-name">
                <a href="#" class="name">{!! $user->name !!}</a>
                <span>{!! $user->roles->first()->name !!}</span>
            </td>
            <td class="hidden-xs hidden-sm">
                <span class="email">{!! $user->email !!}</span>
            </td>
            <td class="user-id">
                {!! $user->id !!}
            </td>
            <td class="action-links">
                <a href="{!! route('users.edit', $user) !!}" class="edit">
                    <i class="linecons-pencil"></i>
                    {!! trans('global.Edit') !!}
                </a>

                <a href="javascript:" class="delete" data-toggle="modal" data-target="#modal-delete-user-{!! $user->id !!}">
                    <i class="linecons-trash"></i>
                    {!! trans('global.Delete') !!}
                </a>

                @include('modals.alert-delete', ['modalId' => 'delete-user-' . $user->id])

            </td>
        </tr>
    @endforeach
    </tbody>
</table>

<div class="row">
    <div class="col-sm-offset-6 col-sm-6 text-right text-center-sm">
        {!! $users->render() !!}
    </div>
</div>
