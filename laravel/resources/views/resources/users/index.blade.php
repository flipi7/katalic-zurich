@extends('layouts.master')

@section('content')

    <div class="page-title">

        <div class="title-env">
            <h1 class="title">{!! trans('global.Users') !!}</h1>

            <p class="description">{!! trans('global.Users subtitle') !!}</p>
        </div>

        <div class="breadcrumb-env">
            {!! Breadcrumbs::render('users') !!}
        </div>

    </div>

    <div class="row">

        <div class="col-md-12">

            <ul class="nav nav-tabs">
                <li class="{!! empty($role_id) ? 'active' : '' !!}">
                    <a href="{!! route('users.index') !!}">
                        {!! trans('global.All Users') !!} ({!! $totalUsers !!})
                    </a>
                </li>
                @foreach($roles as $role)
                    <li class="{!! $role_id == $role->id ? 'active' : '' !!}">
                        <a href="{!! route('users.index', ['role_id' => $role->id]) !!}">
                            {!! $role->name !!} ({!! $role->users->count() !!})
                        </a>
                    </li>
                @endforeach
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="all">
                    @include('resources.users.table-list')
                </div>
            </div>

        </div>

    </div>
@endsection