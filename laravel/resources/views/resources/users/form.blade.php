<div class="row">
    <div class="col-sm-3">
        <label class="control-label" for="name">{!! trans('global.Name') !!}</label>
    </div>
    <div class="col-sm-9">
        <input type="text" class="form-control" id="name" name="name" placeholder="{!! trans('global.Name') !!}"
               value="{!! old('name', !empty($user->name) ? $user->name : '') !!}">
    </div>
</div>

<div class="row">
    <div class="col-sm-3">
        <label class="control-label" for="email">{!! trans('global.Email') !!}</label>
    </div>
    <div class="col-sm-9">
        <input type="text" class="form-control" id="email" name="email" placeholder="{!! trans('global.Email') !!}"
               value="{!! old('email', !empty($user->email) ? $user->email : '') !!}">
    </div>
</div>

<div class="row">
    <div class="col-sm-3">
        <label class="control-label" for="password">{!! trans('global.Password') !!}</label>
    </div>
    <div class="col-sm-9">
        <input type="password" class="form-control" id="password" name="password"
               placeholder="{!! trans('global.Password') !!}" value="">
    </div>
</div>

<div class="row">
    <div class="col-sm-3">
        <label class="control-label" for="role_id">{!! trans('global.Role') !!}</label>
    </div>
    <div class="col-sm-9">
        <select class="form-control" name="role_id">
            <option value=""></option>
            @foreach($roles as $role)
                <option value="{!! $role->id !!}" {!! !empty($user) && $role->id == $user->roles->first()->id ? 'selected' : '' !!}>{!! $role->name !!}</option>
            @endforeach
        </select>
    </div>
</div>