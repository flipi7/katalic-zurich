@extends('layouts.master')

@section('content')

    <div class="page-title">

        <div class="title-env">
            <h1 class="title">{!! trans('global.Edit user') !!}</h1>

            <p class="description">{!! trans('global.Edit user subtitle') !!}</p>
        </div>

        <div class="breadcrumb-env">
            {!! Breadcrumbs::render('user.edit', $user) !!}
        </div>

    </div>

    <form role="form" class="form-horizontal" role="form" action="{!! route('users.update', $user) !!}" method="POST">
        {!! csrf_field() !!}
        {!! method_field('PUT') !!}
        <div class="panel panel-headerless">
            <div class="panel-body">

                <div class="member-form-add-header">
                    <div class="row">
                        <div class="col-md-2 col-sm-4 pull-right-sm">

                            <div class="action-buttons">
                                <button type="submit"
                                        class="btn btn-block btn-secondary">{!! trans('global.Save') !!}</button>
                                <button type="reset"
                                        class="btn btn-block btn-gray">{!! trans('global.Reset') !!}</button>
                            </div>

                        </div>
                        <div class="col-md-10 col-sm-8">

                            <div class="user-img">
                                <img src="{!! asset('assets/images/user-4.png') !!}" class="img-circle" alt="user-pic"/>
                            </div>
                            <div class="user-name">
                                <a href="#">{!! $user->name !!}</a>
                                <span>{!! $user->roles->first()->name !!}</span>
                            </div>

                        </div>
                    </div>
                </div>


                <div class="member-form-inputs">
                    @include('resources.users.form')
                </div>

            </div>
        </div>
    </form>

@endsection