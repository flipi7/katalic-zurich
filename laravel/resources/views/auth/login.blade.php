<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="description" content="Katalic"/>
    <meta name="author" content=""/>

    <title>Katalic</title>

    <link rel='shortcut icon' type='image/x-icon' href="{!! asset('assets/images/favicon.ico') !!}" />
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Arimo:400,700,400italic">
    <link rel="stylesheet" href="assets/css/fonts/linecons/css/linecons.css">
    <link rel="stylesheet" href="assets/css/fonts/fontawesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/xenon-core.css">
    <link rel="stylesheet" href="assets/css/xenon-forms.css">
    <link rel="stylesheet" href="assets/css/xenon-components.css">
    <link rel="stylesheet" href="assets/css/xenon-skins.css">
    <link rel="stylesheet" href="assets/css/custom.css">

    <script src="assets/js/jquery-1.11.1.min.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->


</head>
<body class="page-body login-page">

<script>
    $app = {
        messages: {}
    };
</script>

<div class="login-container">

    <div class="row">

        <div class="col-sm-6">

            <script type="text/javascript">
                jQuery(document).ready(function ($) {
                    // Reveal Login form
                    setTimeout(function () {
                        $(".fade-in-effect").addClass('in');
                    }, 1);


                    // Validation and Ajax action
                    $("form#login").validate({
                        rules: {
                            email: {
                                required: true
                            },

                            password: {
                                required: true
                            }
                        },

                        messages: {
                            email: {
                                required: '{!! trans('auth.Please enter your email') !!}'
                            },

                            password: {
                                required: '{!! trans('auth.Please enter your password') !!}'
                            }
                        }
                    });

                    // Set Form focus
                    $("form#login .form-group:has(.form-control):first .form-control").focus();
                });
            </script>

            <!-- Errors container -->
            <div class="errors-container">


            </div>

            <!-- Add class "fade-in-effect" for login form effect -->
            <form method="post" role="form" id="login" class="login-form fade-in-effect"
                  action="{!! route('auth.login') !!}">
                {!! csrf_field() !!}
                <div class="login-header">
                    <a href="/" class="logo">
                        <img src="assets/images/logo@2x.png" alt="" width="80"/>
                        <span>log in</span>
                    </a>

                    <p>{!! trans('auth.login_description') !!}</p>
                </div>


                <div class="form-group">
                    <label class="control-label" for="email">{!! trans('global.Email') !!}</label>
                    <input type="text" class="form-control input-dark" name="email" id="email" autocomplete="off"/>
                </div>

                <div class="form-group">
                    <label class="control-label" for="passwd">{!! trans('global.Password') !!}</label>
                    <input type="password" class="form-control input-dark" name="password" id="password"
                           autocomplete="off"/>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-dark  btn-block text-left">
                        <i class="fa-lock"></i>
                        {!! trans('auth.Log In') !!}
                    </button>
                </div>

            </form>

        </div>

    </div>

</div>

<!-- Include App Messages -->
{{--@include('layouts.messages')--}}

        <!-- Bottom Scripts -->
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/TweenMax.min.js"></script>
<script src="assets/js/resizeable.js"></script>
<script src="assets/js/joinable.js"></script>
<script src="assets/js/xenon-api.js"></script>
<script src="assets/js/xenon-toggles.js"></script>
<script src="assets/js/jquery-validate/jquery.validate.min.js"></script>
<script src="assets/js/toastr/toastr.min.js"></script>


<!-- JavaScripts initializations and stuff -->
<script src="assets/js/xenon-custom.js"></script>

<!-- JavaScripts App Custom -->
<script src="assets/js/app-custom.js"></script>

</body>
</html>