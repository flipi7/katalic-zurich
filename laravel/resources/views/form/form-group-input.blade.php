<div class="form-group {!! $errors->first($fieldName) ? 'has-error' : '' !!}">
    <label class="control-label">{!! $label !!}</label>
    <input class="form-control"
           name="{!! $name !!}"
           data-validate="{!! $validate !!}"
           placeholder="{!! $placeholder !!}"
           value="{!! old($fieldName) !!}"
            />
</div>