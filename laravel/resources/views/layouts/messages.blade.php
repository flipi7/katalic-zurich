@if($errors->getBags())
    <script>
        @foreach($errors->getBags() as $bag)
        $app.messages.errors = {!! json_encode($bag->toArray()) !!}
        @endforeach
    </script>
@endif

@if(session('messages.success'))
    <script>
        $app.messages.success = {!! json_encode(session('messages.success')) !!}
    </script>
@endif