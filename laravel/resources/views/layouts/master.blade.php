<?php
require_once(getcwd() . '/assets/js/iflychat/iflychatsettings.php');
require_once(getcwd() . '/assets/js/iflychat/iflychatuserdetails.php');
require_once(getcwd() . '/assets/js/iflychat/iflychat.php');

global $iflychat_userinfo;
$user = Auth::user();
$iflychat_userinfo = new iFlyChatUserDetails('asasg', '1');
$iflychat_userinfo->setName($user->name);
$iflychat_userinfo->setId((string)$user->id);
$iflychat_userinfo->setIsAdmin(TRUE);//$user->roles->first()->name == 'Super Admin' ? TRUE : FALSE);
$iflychat_userinfo->setAvatarUrl('https://iflychat.com/sites/all/modules/drupalchat/themes/light/images/default_avatar.png');
$iflychat_userinfo->setProfileLink('/user.php?id=1');
$iflychat_userinfo->setRoomRoles(array());
$iflychat_userinfo->setRelationshipSet(FALSE);
$iflychat_userinfo->setAllRoles(array('1'=>'non-admin', '2' => 'admin'));
$_SESSION['iflychat_user'] = $iflychat_userinfo;

$iflychat_settings = new iFlyChatSettings();
$iflychat = new iFlyChat($iflychat_settings->iflychat_settings, $iflychat_userinfo->getUserDetails());

$ifly_html_code = $iflychat->getHtmlCode();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="description" content="Katalic"/>
    <meta name="author" content=""/>

    <title>Katalic</title>

    <link rel='shortcut icon' type='image/x-icon' href="{!! asset('assets/images/favicon.ico') !!}" />
    <link rel="stylesheet" href="{!! asset('assets/css/fonts/linecons/css/linecons.css') !!}">
    <link rel="stylesheet" href="{!! asset('assets/css/fonts/fontawesome/css/font-awesome.min.css') !!}">
    <link rel="stylesheet" href="{!! asset('assets/css/bootstrap.css') !!}">
    <link rel="stylesheet" href="{!! asset('assets/css/xenon-core.css') !!}">
    <link rel="stylesheet" href="{!! asset('assets/css/xenon-forms.css') !!}">
    <link rel="stylesheet" href="{!! asset('assets/css/xenon-components.css') !!}">
    <link rel="stylesheet" href="{!! asset('assets/css/xenon-skins.css') !!}">
    @yield('pageStyles')
    <link rel="stylesheet" href="{!! asset('assets/css/custom.css') !!}">
    <link rel="stylesheet" href="{!! asset('assets/js/wysihtml5/src/bootstrap-wysihtml5.css') !!}">


    <script src="{!! asset('assets/js/jquery-1.11.1.min.js') !!}"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="https://maps.googleapis.com/maps/api/js"></script>

</head>
<body class="page-body">

<script>
    $app = {
        messages: {}
    };
</script>

<div class="settings-pane">

    <a href="#" data-toggle="settings-pane" data-animate="true">
        &times;
    </a>

    <div class="settings-pane-inner">

        <div class="row">

            <div class="col-md-4">

                <div class="user-info">

                    <div class="user-image">
                        <a href="extra-profile.html">
                            <img src="{!! asset('assets/images/user-2.png') !!}" class="img-responsive img-circle"/>
                        </a>
                    </div>

                    <div class="user-details">

                        <h3>
                            <a href="extra-profile.html">{!! 'test' !!}</a>

                            <!-- Available statuses: is-online, is-idle, is-busy and is-offline -->
                            <span class="user-status is-online"></span>
                        </h3>

                        <p class="user-title">Web Developer</p>

                        <div class="user-links">
                            <a href="extra-profile.html" class="btn btn-primary">Edit Profile</a>
                            <a href="extra-profile.html" class="btn btn-success">Upgrade</a>
                        </div>

                    </div>

                </div>

            </div>

            <div class="col-md-8 link-blocks-env">

                <div class="links-block left-sep">
                    <h4>
                        <span>Notifications</span>
                    </h4>

                    <ul class="list-unstyled">
                        <li>
                            <input type="checkbox" class="cbr cbr-primary" checked="checked" id="sp-chk1"/>
                            <label for="sp-chk1">Messages</label>
                        </li>
                        <li>
                            <input type="checkbox" class="cbr cbr-primary" checked="checked" id="sp-chk2"/>
                            <label for="sp-chk2">Events</label>
                        </li>
                        <li>
                            <input type="checkbox" class="cbr cbr-primary" checked="checked" id="sp-chk3"/>
                            <label for="sp-chk3">Updates</label>
                        </li>
                        <li>
                            <input type="checkbox" class="cbr cbr-primary" checked="checked" id="sp-chk4"/>
                            <label for="sp-chk4">Server Uptime</label>
                        </li>
                    </ul>
                </div>

                <div class="links-block left-sep">
                    <h4>
                        <a href="#">
                            <span>Help Desk</span>
                        </a>
                    </h4>

                    <ul class="list-unstyled">
                        <li>
                            <a href="#">
                                <i class="fa-angle-right"></i>
                                Support Center
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa-angle-right"></i>
                                Submit a Ticket
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa-angle-right"></i>
                                Domains Protocol
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa-angle-right"></i>
                                Terms of Service
                            </a>
                        </li>
                    </ul>
                </div>

            </div>

        </div>

    </div>

</div>

<div class="page-container">

    <div class="sidebar-menu toggle-others fixed">

        <div class="sidebar-menu-inner">

            <header class="logo-env">

                <!-- logo -->
                <div class="logo">
                    <a href="/" class="logo-expanded">
                        <img src="{!! asset('assets/images/logo@2x.png') !!}" width="80" alt=""/>
                    </a>

                    <a href="/" class="logo-collapsed">
                        <img src="{!! asset('assets/images/logo-collapsed@2x.png') !!}" width="40" alt=""/>
                    </a>
                </div>

                <div class="mobile-menu-toggle visible-xs">
                    <a href="#" data-toggle="user-info-menu">
                        <i class="fa-bell-o"></i>
                        <span class="badge badge-success">7</span>
                    </a>

                    <a href="#" data-toggle="mobile-menu">
                        <i class="fa-bars"></i>
                    </a>
                </div>

            </header>


            <ul id="main-menu" class="main-menu">
                @if(Auth::user()->roles->first()->id == 1)
                <li class="{!! request()->is('users*') ? 'active opened' : '' !!}">
                    <a href="claims">
                        <i class="fa fa-users"></i>
                        <span class="title">{!! trans('global.Users') !!}</span>
                    </a>
                    <ul>
                        <li class="{!! request()->is('users') ? 'active' : '' !!}">
                            <a href="{!! route('users.index') !!}">
                                <span class="title">{!! trans('global.View Users') !!}</span>
                            </a>
                        </li>
                        <li class="{!! request()->is('users/create') ? 'active' : '' !!}">
                            <a href="{!! route('users.create') !!}">
                                <span class="title">{!! trans('global.Add New User') !!}</span>
                            </a>
                        </li>
                    </ul>
                </li>
                @endif
                <li class="{!! request()->is('claims*') ? 'active opened' : '' !!}">
                    <a href="claims">
                        <i class="fa fa-ambulance"></i>
                        <span class="title">{!! trans('global.Claims') !!}</span>
                    </a>
                    <ul>
                        <li class="{!! request()->is('claims') ? 'active' : '' !!}">
                            <a href="{!! route('claims.index') !!}">
                                <span class="title">{!! trans('global.View Claims') !!}</span>
                            </a>
                        </li>
                        @if(Auth::user()->roles->first()->id == 1 || Auth::user()->roles->first()->id == 2 || Auth::user()->roles->first()->id == 6)
                        <li class="{!! request()->is('claims/create') ? 'active' : '' !!}">
                            <a href="{!! route('claims.create') !!}">
                                <span class="title">{!! trans('global.Add New Claim') !!}</span>
                            </a>
                        </li>
                        @endif
                    </ul>
                </li>
                <li class="{!! request()->is('stats*') ? 'active opened' : '' !!}">
                    <a href="claims">
                        <i class="fa fa-line-chart"></i>
                        <span class="title">{!! trans('global.Statistics') !!}</span>
                    </a>
                    <ul>
                        <li class="{!! request()->is('stats') ? 'active' : '' !!}">
                            <a href="{!! route('stats.index') !!}">
                                <span class="title">{!! trans('global.Claims statistics') !!}</span>
                            </a>
                        </li>
                        @if(Auth::user()->roles->first()->id == 1 || Auth::user()->roles->first()->id == 6)
                            <li class="{!! request()->is('stats/users') ? 'active' : '' !!}">
                                <a href="{!! route('stats.users') !!}">
                                    <span class="title">{!! trans('global.Users statistics') !!}</span>
                                </a>
                            </li>
                            <li class="{!! request()->is('stats/external') ? 'active' : '' !!}">
                            <a href="{!! route('stats.external') !!}">
                                    <span class="title">{!! trans('global.External statistics') !!}</span>
                                </a>
                            </li>
                        @endif
                    </ul>
                </li>

                <li>
                    <a target="_blank" href="http://ayuda.katalic.com">
                        <i class="fa fa-question"></i>
                        <span class="title">{!! trans('global.Help') !!}</span>
                    </a>
                </li>


            </ul>

        </div>

    </div>

    <div class="main-content">

        <nav class="navbar user-info-navbar" role="navigation"><!-- User Info, Notifications and Menu Bar -->

            <!-- Left links for user info navbar -->
            <ul class="user-info-menu left-links list-inline list-unstyled">

                <li class="hidden-sm hidden-xs">
                    <a href="#" data-toggle="sidebar">
                        <i class="fa-bars"></i>
                    </a>
                </li>

                <li class="dropdown hover-line">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa-envelope-o"></i>
                    </a>

                    <ul class="dropdown-menu messages">
                        <li class="top">
                            <p class="small">
                                You have no new messages.
                            </p>
                        </li>

                        <li class="external">
                            <a href="mailbox-main.html">
                                <span>All Messages</span>
                                <i class="fa-link-ext"></i>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="dropdown hover-line">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa-bell-o"></i>
                        {{--<span class="badge badge-purple">7</span>--}}
                    </a>

                    <ul class="dropdown-menu notifications">
                        <li class="top">
                            <p class="small">
                                {{--<a href="#" class="pull-right">Mark all Read</a>--}}
                                You have no new notifications.
                            </p>
                        </li>

                        <li class="external">
                            <a href="#">
                                <span>View all notifications</span>
                                <i class="fa-link-ext"></i>
                            </a>
                        </li>
                    </ul>
                </li>

                <!-- Added in v1.2 -->
                <li class="dropdown hover-line language-switcher">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="{!! asset('assets/images/flags/flag-es.png') !!}" alt="flag-es"/>
                        Espa&ntilde;ol
                    </a>

                    <ul class="dropdown-menu languages">
                        <li>
                            <a href="#">
                                <img src="{!! asset('assets/images/flags/flag-uk.png') !!}" alt="flag-uk"/>
                                English
                            </a>
                        </li>
                        <li class="active">
                            <a href="#">
                                <img src="{!! asset('assets/images/flags/flag-es.png') !!}" alt="flag-es"/>
                                Espa&ntilde;ol
                            </a>
                        </li>
                    </ul>
                </li>

            </ul>


            <!-- Right links for user info navbar -->
            <ul class="user-info-menu right-links list-inline list-unstyled">
                <li class="search-form"><!-- You can add "always-visible" to show make the search input visible -->

                    <form name="userinfo_search_form" method="get" action="extra-search.html">
                        <input type="text" name="s" class="form-control search-field" placeholder="Type to search..."/>

                        <button type="submit" class="btn btn-link">
                            <i class="linecons-search"></i>
                        </button>
                    </form>

                </li>

                <li class="dropdown user-profile">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="{!! asset('assets/images/user-4.png') !!}" alt="user-image" class="img-circle img-inline userpic-32"
                             width="28"/>
							<span>
                                {!! Auth::user()->name !!} {!! Auth::user()->roles->first()->id !!}
                                <i class="fa-angle-down"></i>
							</span>
                    </a>

                    <ul class="dropdown-menu user-profile-menu list-unstyled">
                        <li>
                            <a href="#edit-profile">
                                <i class="fa-edit"></i>
                                New Post
                            </a>
                        </li>
                        <li>
                            <a href="#settings">
                                <i class="fa-wrench"></i>
                                Settings
                            </a>
                        </li>
                        <li>
                            <a href="#profile">
                                <i class="fa-user"></i>
                                Profile
                            </a>
                        </li>
                        <li>
                            <a href="#help">
                                <i class="fa-info"></i>
                                Help
                            </a>
                        </li>
                        <li class="last">
                            <a href="{!! route('auth.logout') !!}">
                                <i class="fa-lock"></i>
                                Logout
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>

        </nav>

        <!-- Page Content -->
        @yield('content')

        <footer class="main-footer sticky footer-type-1">

            <div class="footer-inner">

                <div class="footer-text">
                    &copy; <?php print date("Y"); ?> <strong>Katalic</strong>
                </div>

                <div class="go-up">

                    <a href="#" rel="go-top">
                        <i class="fa-angle-up"></i>
                    </a>

                </div>

            </div>

        </footer>
    </div>

</div>

<!-- Page Loading Overlay -->
<div class="page-loading-overlay">
    <div class="loader-2"></div>
</div>

<!-- Include App Messages -->
@include('layouts.messages')

<!-- Bottom Scripts -->
<script src="{!! asset('assets/js/bootstrap.min.js') !!}"></script>
<script src="{!! asset('assets/js/TweenMax.min.js') !!}"></script>
<script src="{!! asset('assets/js/resizeable.js') !!}"></script>
<script src="{!! asset('assets/js/joinable.js') !!}"></script>
<script src="{!! asset('assets/js/xenon-api.js') !!}"></script>
<script src="{!! asset('assets/js/xenon-toggles.js') !!}"></script>


<!-- Imported scripts on this page -->
<script src="{!! asset('assets/js/xenon-widgets.js') !!}"></script>
<script src="{!! asset('assets/js/devexpress-web-14.1/js/globalize.min.js') !!}"></script>
<script src="{!! asset('assets/js/devexpress-web-14.1/js/dx.chartjs.js') !!}"></script>
<script src="{!! asset('assets/js/toastr/toastr.min.js') !!}"></script>

@yield('pageScripts')

<!-- JavaScripts initializations and stuff -->
<script src="{!! asset('assets/js/xenon-custom.js') !!}"></script>

<!-- JavaScripts App Custom -->
<script src="{!! asset('assets/js/app-custom.js"') !!}"></script>
<script>
    jQuery.extend(jQuery.validator.messages, {
        required: "{!! trans('global.This field is required.') !!}",
        remote: "{!! trans('global.Please fix this field.') !!}",
        email: "{!! trans('global.Please enter a valid email address.') !!}",
        url: "{!! trans('global.Please enter a valid URL.') !!}",
        date: "{!! trans('global.Please enter a valid date.') !!}",
        dateISO: "{!! trans('global.Please enter a valid date (ISO).') !!}",
        number: "{!! trans('global.Please enter a valid number.') !!}",
        digits: "{!! trans('global.Please enter only digits.') !!}",
        creditcard: "{!! trans('global.Please enter a valid credit card number.') !!}",
        equalTo: "{!! trans('global.Please enter the same value again.') !!}",
        accept: "{!! trans('global.Please enter a value with a valid extension.') !!}",
        maxlength: jQuery.validator.format("{!! trans('global.Please enter no more than') !!} {0} {!! trans('global.characters.') !!}"),
        minlength: jQuery.validator.format("{!! trans('global.Please enter at least') !!} {0} {!! trans('global.characters.') !!}"),
        rangelength: jQuery.validator.format("{!! trans('global.Please enter a value between') !!} {0} {!! trans('global.and') !!} {1} {!! trans('global.characters long.') !!}"),
        range: jQuery.validator.format("{!! trans('global.Please enter a value between') !!} {0} {!! trans('global.and') !!} {1}."),
        max: jQuery.validator.format("{!! trans('global.Please enter a value less than or equal to') !!} {0}."),
        min: jQuery.validator.format("{!! trans('global.Please enter a value greater than or equal to') !!} {0}.")
    });
</script>
<!-- iflychat -->
<?php print $ifly_html_code; ?>
<!-- iflychat -->
</body>
</html>