<div class="modal fade" id="modal-{!! $modalId ?: 'id' !!}">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">{!! !empty($title) ? $title : trans('global.Are you sure?') !!}</h4>
            </div>

            <div class="modal-body">
                {!! !empty($description) ? $description : trans('global.Are you sure you want to delete this?') !!}
            </div>

            <div class="modal-footer">
                <form action="{!! route('users.destroy', $user) !!}" method="POST">
                    {!! csrf_field() !!}
                    {!! method_field('DELETE') !!}
                    <button type="button" class="btn btn-white" data-dismiss="modal">
                        {!! trans('global.Close') !!}
                    </button>
                    <button type="submit" class="btn btn-danger">
                        {!! trans('global.Delete') !!}
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>