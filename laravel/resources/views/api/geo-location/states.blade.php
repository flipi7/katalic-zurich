<option value="">{!! trans('global.Select State') !!}</option>
@foreach ($states as $state)
<option value="{{$state->id}}" @if (!empty($selected) && $selected == $state->id) selected="selected" @endif>{{$state->name}}</option>
@endforeach