<option value="">{!! trans('global.Select City') !!}</option>
@foreach ($cities as $city)
<option value="{{$city->id}}" @if (!empty($selected) && $selected == $city->id) selected="selected" @endif>{{$city->name}}</option>
@endforeach