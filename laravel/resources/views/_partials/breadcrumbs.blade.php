@if ($breadcrumbs)
    <ol class="breadcrumb bc-1">
        @foreach ($breadcrumbs as $breadcrumb)
            @if ($breadcrumb->first)
                <li>
                    <a href="{!! $breadcrumb->url !!}">
                        <i class="fa-home"></i>
                        {!! $breadcrumb->title !!}
                    </a>
                </li>
            @elseif (!$breadcrumb->last)
                <li>
                    <a href="{!! $breadcrumb->url !!}">{!! $breadcrumb->title !!}</a>
                </li>
            @else
                <li class="active"><strong>{!! $breadcrumb->title !!}</strong></li>
            @endif
        @endforeach
    </ol>
@endif