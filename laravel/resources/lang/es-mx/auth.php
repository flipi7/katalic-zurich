<?php

return [
    'failed' => 'Estas credenciales no coinciden con nuestros registros.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'login_description' => 'Estimado usuario, inicie sesión para acceder al área de administración!',
    'Log In' => 'Conectar',
    'Please enter your email' => 'Por favor, introduzca su email',
    'Please enter your password' => 'Por favor, introduzca su contraseña'

];
