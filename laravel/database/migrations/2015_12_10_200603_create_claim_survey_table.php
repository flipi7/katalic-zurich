<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClaimSurveyTable extends Migration
{
    public function up()
    {
        Schema::create('claim_survey', function (Blueprint $table) {
            // Fields
            $table->increments('id');
            $table->integer('claim_id')->unsigned();
            $table->integer('survey_id')->unsigned();
            $table->timestamps();

            // Indexes
            $table->foreign('claim_id')->references('id')->on('claims');
            $table->foreign('survey_id')->references('id')->on('surveys');
        });
    }

    public function down()
    {
        Schema::drop('claim_survey');
    }
}
