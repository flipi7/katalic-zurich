<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableClaims extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('claims', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('policy_number');
            $table->string('policy_holder');
            $table->text('description');
            $table->float('amount_loss', 10, 2);
            $table->enum('currency', ['mxn', 'usd']);
            $table->string('zurich_claim_number')->nullable();
            $table->enum('is_cover_correct', ['yes', 'no'])->nullable();
            $table->integer('last_status')->unsigned();
            $table->string('hash');
            $table->timestamp('occurred_at');
            $table->timestamps();
            $table->softDeletes();

            // Indexes
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::drop('claims');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
