<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataPreliminaryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('claim_preliminary', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('visit_date');
            $table->dateTime('claim_date');
            $table->dateTime('validity_start');
            $table->dateTime('validity_end');
            $table->enum('third_party', ['yes', 'no']);
            $table->enum('cover', ['yes', 'no']);
            $table->string('description');
            $table->float('amount_loss', 10, 2);
            $table->integer('claim_id')->unsigned();
            $table->foreign('claim_id')->references('id')->on('claims');
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('claim_preliminary');
    }
}

