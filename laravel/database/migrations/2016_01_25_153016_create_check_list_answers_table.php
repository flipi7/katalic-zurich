<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCheckListAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('check_list_answers', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('answer', ['yes', 'no']);
            $table->integer('claim_id')->unsigned();
            $table->foreign('claim_id')->references('id')->on('claims');
            $table->integer('question_id')->unsigned();
            $table->foreign('question_id')->references('id')->on('check_list_questions');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('check_list_answers');
    }
}
