<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClaimDataIkeTable extends Migration
{
    public function up()
    {
        Schema::create('claim_data_ike', function (Blueprint $table) {
            // Fields
            $table->increments('id');
            $table->integer('claim_id')->unsigned();
            $table->string('reference');
            $table->integer('adjuster_id')->unsigned();
            $table->timestamps();

            // Indexes
            $table->foreign('claim_id')->references('id')->on('claims');
            $table->foreign('adjuster_id')->references('id')->on('users');
        });
    }

    public function down()
    {
        Schema::drop('claim_data_ike');
    }
}
