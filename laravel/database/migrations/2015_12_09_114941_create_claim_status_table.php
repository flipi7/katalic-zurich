<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClaimStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('claim_status', function (Blueprint $table) {
            // Fields
            $table->increments('id');
            $table->integer('claim_id')->unsigned();
            $table->integer('status_id')->unsigned();
            $table->timestamps();

            // Indexes
            $table->foreign('claim_id')->references('id')->on('claims');
            $table->foreign('status_id')->references('id')->on('statuses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('claim_status');
    }
}
