<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClaimDataVerificationTable extends Migration
{
    public function up()
    {
        Schema::create('claim_data_verification', function (Blueprint $table) {
            // Fields
            $table->increments('id');
            $table->integer('claim_id')->unsigned();
            $table->enum('policy_payment', ['yes', 'no']);
            $table->date('validity_start');
            $table->date('validity_end');
            $table->enum('siu', ['yes', 'no']);
            $table->enum('coinsurance', ['yes', 'no']);
            $table->string('coinsurance_company')->nullable();
            $table->integer('coinsurance_percentage')->nullable();
            $table->enum('complaint', ['yes', 'no']);
            $table->integer('analyst_id')->unsigned();
            $table->timestamps();

            // Indexes
            $table->foreign('claim_id')->references('id')->on('claims');
            $table->foreign('analyst_id')->references('id')->on('users');
        });
    }

    public function down()
    {
        Schema::drop('claim_data_verification');
    }
}
