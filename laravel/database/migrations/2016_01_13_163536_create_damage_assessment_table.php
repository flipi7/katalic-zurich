<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDamageAssessmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('damage_assessment', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('cover', ['yes', 'no']);
            $table->float('amount_loss', 10, 2);
            $table->float('deductible', 10, 2);
            $table->string('adjustment_description');
            $table->integer('claim_id')->unsigned();
            $table->foreign('claim_id')->references('id')->on('claims');
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('damage_assessment');
    }
}

