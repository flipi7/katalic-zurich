<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataReportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_report', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('salvage', ['yes', 'no']);
            $table->enum('subrogation', ['yes', 'no']);
            $table->enum('agreement', ['yes', 'no']);
            $table->enum('fraud', ['yes', 'no']);
            $table->enum('insurance', ['under', 'over', 'right']);
            $table->string('claim_report');
            $table->integer('claim_id')->unsigned();
            $table->foreign('claim_id')->references('id')->on('claims');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('data_report');
    }
}

