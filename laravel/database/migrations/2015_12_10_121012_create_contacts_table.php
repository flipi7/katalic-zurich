<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsTable extends Migration
{
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            // Fields
            $table->increments('id');
            $table->integer('claim_id')->unsigned();
            $table->enum('type', ['insured', 'agent', 'injured', 'causative', 'other']);
            $table->string('name');
            $table->string('phone');
            $table->string('email');
            $table->text('description');
            $table->timestamps();
            $table->softDeletes();

            // Indexes
            $table->foreign('claim_id')->references('id')->on('claims');
        });
    }

    public function down()
    {
        Schema::drop('contacts');
    }
}
