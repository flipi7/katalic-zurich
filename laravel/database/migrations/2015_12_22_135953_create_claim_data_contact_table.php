<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClaimDataContactTable extends Migration
{
    public function up()
    {
        Schema::create('claim_data_contact', function (Blueprint $table) {
            // Fields
            $table->increments('id');
            $table->integer('claim_id')->unsigned();
            $table->enum('contact', ['yes', 'no']);
            $table->dateTime('visit_date');
            $table->text('note');
            $table->timestamps();

            // Indexes
            $table->foreign('claim_id')->references('id')->on('claims');
        });
    }

    public function down()
    {
        Schema::drop('claim_data_contact');
    }
}
