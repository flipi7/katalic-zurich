<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Artisan;

class Dev extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $this->command->info('Seeding database with demo data...');

        $this->call(DevUsersTableSeeder::class);
        $this->call(DevSurveysTableSeeder::class);
        $this->call(DevSurveyQuestionsTableSeeder::class);
        $this->call(DevClaimsTableSeeder::class);

        $this->command->info('Seeding database completed.');
        Model::reguard();
    }
}
