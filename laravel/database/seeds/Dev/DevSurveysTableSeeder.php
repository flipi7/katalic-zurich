<?php

use Illuminate\Database\Seeder;

class DevSurveysTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('surveys')->insert([
            ['name' => 'Inundación'],
            ['name' => 'Incendio'],
            ['name' => 'Robo'],
            ['name' => 'Cristales'],
            ['name' => 'Equipo electrónico'],
            ['name' => 'Responsabilidad Civil'],
        ]);
    }
}
