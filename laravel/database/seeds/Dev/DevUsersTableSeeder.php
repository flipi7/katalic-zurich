<?php

use Illuminate\Database\Seeder;

class DevUsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /* $users = \DB::table('users')->where('name', '<>', 'Admin'); */
        /* print_r($users->lists('name')); */

        /* $users = \DB::table('role_user')->where('role_id', '<>', \App\Models\Role::SUPER_ADMIN); */
        /* print_r($users->lists('user_id')); */
        
        $roles_users = array(
            array(
                'name' => 'Antonio Rojo',
                'email' => 'antonio.rojo@katalic.com',
                'role' => \App\Models\Role::CABINA_DANOS
            ),
            array(
                'name' => 'Enrique Espada',
                'email' => 'enrique.espada@katalic.com',
                'role' => \App\Models\Role::MESA_CONTROL
            ),
            array(
                'name' => 'Carlos Sanz',
                'email' => 'carlos.sanz@katalic.com',
                'role' => \App\Models\Role::ANALISTA
            ),
            array(
                'name' => 'Guillermo Montoya',
                'email' => 'guillermo.montoya@katalic.com',
                'role' => \App\Models\Role::AJUSTADOR
            ),
            array(
                'name' => 'Lorenzo Castro',
                'email' => 'lorenzo.castro@katalic.com',
                'role' => \App\Models\Role::COORDINADOR
            ),
            array(
                'name' => 'Juan Palomo',
                'email' => 'juan.palomo@katalic.com',
                'role' => \App\Models\Role::CABINA_DANOS
            ),
            array(
                'name' => 'Sergio Ramos',
                'email' => 'sergio.ramos@katalic.com',
                'role' => \App\Models\Role::MESA_CONTROL
            ),
            array(
                'name' => 'Luis Suárez',
                'email' => 'luis.suarez@katalic.com',
                'role' => \App\Models\Role::ANALISTA
            ),
            array(
                'name' => 'Raúl Cimas',
                'email' => 'raul.cimas@katalic.com',
                'role' => \App\Models\Role::AJUSTADOR
            ),
            array(
                'name' => 'Joaquín Reyes',
                'email' => 'joaquin.reyes@katalic.com',
                'role' => \App\Models\Role::COORDINADOR
            ),
            array(
                'name' => 'José García',
                'email' => 'jose.garcia@katalic.com',
                'role' => \App\Models\Role::CABINA_DANOS
            ),
            array(
                'name' => 'Julián Castillo',
                'email' => 'julian.castillo@katalic.com',
                'role' => \App\Models\Role::MESA_CONTROL
            ),
            array(
                'name' => 'Diego Vázquez',
                'email' => 'diego.vazquez@katalic.com',
                'role' => \App\Models\Role::ANALISTA
            ),
            array(
                'name' => 'Mario Gallego',
                'email' => 'mario.gallego@katalic.com',
                'role' => \App\Models\Role::AJUSTADOR
            ),
            array(
                'name' => 'Ignario Montilla',
                'email' => 'ignacio.montilla@katalic.com',
                'role' => \App\Models\Role::COORDINADOR
            ),
            array(
                'name' => 'María Peinado',
                'email' => 'maria.peinado@katalic.com',
                'role' => \App\Models\Role::CABINA_DANOS
            ),
            array(
                'name' => 'Rosa Quintana',
                'email' => 'rosa.quintana@katalic.com',
                'role' => \App\Models\Role::MESA_CONTROL
            ),
            array(
                'name' => 'Ana López',
                'email' => 'ana.lopez@katalic.com',
                'role' => \App\Models\Role::ANALISTA
            ),
            array(
                'name' => 'Marina Villar',
                'email' => 'marina.villar@katalic.com',
                'role' => \App\Models\Role::AJUSTADOR
            ),
            array(
                'name' => 'Natalia Muñoz',
                'email' => 'natalia.munoz@katalic.com',
                'role' => \App\Models\Role::COORDINADOR
            )
        );

        foreach ($roles_users as $role_user) {
            if (\DB::table('users')->where('email', '=', $role_user['email'])->count() == 0) {
                $userId = \DB::table('users')->insertGetId(
                    ['name' => $role_user['name'], 'email' => $role_user['email'], 'password' => bcrypt('secret')]
                );

                \DB::table('role_user')->insert([
                    'user_id' => $userId,
                    'role_id' => $role_user['role'],
                    'created_by' => \App\Models\Role::SUPER_ADMIN
                ]);
            }
        }
    }
}
