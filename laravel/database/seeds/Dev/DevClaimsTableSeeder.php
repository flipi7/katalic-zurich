<?php

use Illuminate\Database\Seeder;

class DevClaimsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('es_ES');

        $roleAdjuster = \App\Models\Role::find(\App\Models\Role::AJUSTADOR);
        $adjusterIds = $roleAdjuster->users->lists('id');

        $user = \App\Models\User::find(1); // Admin User

        for ($i = 0; $i <= 800; $i++) {

            // Create Claim
            $created_at = $faker->dateTimeBetween('-1 month', $endDate = 'now');
            $claimInfo = [
                'policy_number' => $faker->randomNumber(),
                'policy_holder' => $faker->name,
                'description' => $faker->text,
                'amount_loss' => $faker->randomFloat(),
                'currency' => $faker->randomElement(['mxn', 'usd']),
                'last_status' => 1,
                'occurred_at' => $faker->dateTimeBetween('-1 year', $endDate = 'now'),
                'created_at' => $created_at,
                'updated_at' => $created_at,
            ];
            $claimInfo = array_merge($claimInfo, [
                'hash' => md5(
                    $claimInfo['policy_number'] .
                    $claimInfo['policy_holder'] .
                    $claimInfo['description'] .
                    $claimInfo['occurred_at']->format('Y-m-d H:i:s')
                )
            ]);
            $claim = new \App\Models\Claim($claimInfo);

            $user->claims()->save($claim);

            // Create Data Ike Reference
            $claim->dataIke()->save(new \App\Models\DataIke([
                'reference' => $faker->randomNumber(),
                'adjuster_id' => $faker->randomElement($adjusterIds->all()),
            ]));

            // Create Contact
            $contact = new \App\Models\Contact([
                'name' => $faker->name,
                'phone' => $faker->phoneNumber,
                'email' => $faker->email,
                'description' => $faker->text,
                'type' => $faker->randomElement(\App\Models\Contact::$types)
            ]);
            $claim->contacts()->save($contact);

            // Random states with cities
            $state_id  = rand(2427, 2459); // 2427 - 2459 (Mexico)
            if ($state_id == 2437) {       // 'Estado de Mexico' and 'Mexico' are the same state
                $state_id = 2442;
            }

            $city_id = null;
            if ($state_id == 2427) {
                $city_id = rand(27566, 27579);
            }
            elseif ($state_id == 2430) {
                $city_id = rand(27580, 27599);
            }
            elseif ($state_id == 2431) {
                $city_id = rand(27600, 27700);
            }
            elseif ($state_id == 2432) {
                $city_id = rand(27701, 27742);
            }
            elseif ($state_id == 2433) {
                $city_id = rand(27743, 27774);
            }
            elseif ($state_id == 2434) {
                $city_id = rand(27775, 27789);
            }
            elseif ($state_id == 2436) {
                $city_id = rand(27790, 27816);
            }
            elseif ($state_id == 2438) {
                $city_id = rand(27817, 27898);
            }
            elseif ($state_id == 2439) {
                $city_id = rand(27899, 27976);
            }
            elseif ($state_id == 2440) {
                $city_id = rand(27977, 28038);
            }
            elseif ($state_id == 2441) {
                $city_id = rand(28039, 28173);
            }
            elseif ($state_id == 2442) {
                $city_id = rand(28174, 28421);
            }
            elseif ($state_id == 2443) {
                $city_id = rand(28422, 28541);
            }
            elseif ($state_id == 2444) {
                $city_id = rand(28542, 28603);
            }
            elseif ($state_id == 2445) {
                $city_id = rand(28604, 28636);
            }
            elseif ($state_id == 2447) {
                $city_id = rand(28637, 28756);
            }
            elseif ($state_id == 2448) {
                $city_id = rand(28757, 28925);
            }
            elseif ($state_id == 2449) {
                $city_id = rand(28926, 28953);
            }
            elseif ($state_id == 2452) {
                $city_id = rand(28954, 29006);
            }
            elseif ($state_id == 2453) {
                $city_id = rand(29007, 29054);
            }
            elseif ($state_id == 2454) {
                $city_id = rand(29055, 29101);
            }
            elseif ($state_id == 2455) {
                $city_id = rand(29102, 29137);
            }
            elseif ($state_id == 2456) {
                $city_id = rand(29138, 29195);
            }
            elseif ($state_id == 2457) {
                $city_id = rand(29196, 29386);
            }
            elseif ($state_id == 2458) {
                $city_id = rand(29387, 29459);
            }
            elseif ($state_id == 2459) {
                $city_id = rand(29460, 29504);
            }

            // Create Address
            $address = new \App\Models\Address([
                'address' => implode(', ', array_slice(explode(',', $faker->address), 0, 3)),
                'country_id' => 142, // Mexico
                'state_id' => $state_id,
                'city_id' => $city_id, // Madrid
            ]);
            $claim->address()->save($address);

            // Create Survey
            $claim->surveys()->attach(rand(1,5)); // Robo

            // Create answers
            $answers = [
                [
                    'claim_id' => $claim->id,
                    'question_id' => 13,
                    'answer' => 'no'
                ],
                [
                    'claim_id' => $claim->id,
                    'question_id' => 14,
                    'answer' => 'no'
                ],
                [
                    'claim_id' => $claim->id,
                    'question_id' => 15,
                    'answer' => 'no'
                ],
                [
                    'claim_id' => $claim->id,
                    'question_id' => 16,
                    'answer' => 'no'
                ],
            ];

            \DB::table('survey_answers')->insert($answers);

            // Create initial activity

            $activity = new \App\Models\Activity([
                'activity' => 'created',
                'date' => $created_at,
                'claim_id' => $claim->id,
                'user_id' => 1]);
            $claim->activities()->save($activity);

            // Statuses
            $claim->statuses()->sync([\App\Models\Status::CREATED]);
        }
    }
}
