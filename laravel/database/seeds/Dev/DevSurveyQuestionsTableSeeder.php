<?php

use Illuminate\Database\Seeder;

class DevSurveyQuestionsTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i < 6; $i++) {
            $questions = [
                [ 'name' => '¿Se ha paralizado la operación?', 'template_id' => $i ],
                [ 'name' => '¿Ya se contactó al servicio de bomberos?', 'template_id' => $i ],
                [ 'name' => '¿Requiere un especialista de limpieza?', 'template_id' => $i ],
                [ 'name' => '¿Existe algún tercero afectado?', 'template_id' => $i ]
            ];

            \DB::table('survey_questions')->insert($questions);
        }

        $checkListQuestions = [
            [
                'name' => 'El siniestro ocurre dentro de los 60 días del inicio de vigencia o dentro de los 60 días del término de vigencia, para pólizas nuevas.',
            ],
            [
                'name' => 'El siniestro ocurre dentro de los 30 días del inicio de vigencia o dentro de los 60 días del término de vigencia, para pólizas de renovación.',
            ],
            [
                'name' => 'El siniestro ocurre 30 días despues de una adicion de cobertura a la póliza',
            ],
            [
                'name' => 'Existencia de mas de una póliza que cubren el mismo siniestro',
            ],
            [
                'name' => 'El asegurado no es conciso en cuanto al giro estipulado en su póliza',
            ],
            [
                'name' => 'El siniestro ocurre al final de la temporada de un negocio temporal',
            ],
            [
                'name' => 'El reclamo de la pérdida no va de acuerdo al nivel economico que presenta el asegurado',
            ],
            [
                'name' => 'El asegurado reclama bienes que soporta con facturas que no estan a su nombre?',
            ],
            [
                'name' => 'El asegurado ofreció algún incentivo por un pago rápido',
            ],
            [
                'name' => 'Los bienes asegurado se encuentra notablemente sobre asegurados',
            ],
            [
                'name' => 'El asegurado tiene/presenta siniestros con mucha frecuencia?',
            ],
            [
                'name' => 'Asegurado tarda mas de 5 días en reportar a Zurich',
            ]
        ];

        \DB::table('check_list_questions')->insert($checkListQuestions);
    }
}
