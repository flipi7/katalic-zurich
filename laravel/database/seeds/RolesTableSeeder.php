<?php

use Illuminate\Database\Seeder;
use App\Models\Role;
use App\Models\User;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Check if seeded
        $roles = Role::all();

        if ($roles->isEmpty()) {
            Role::insert([
                ['id' => Role::SUPER_ADMIN, 'name' => 'Super Admin'],
                ['id' => Role::CABINA_DANOS, 'name' => 'Cabina daños'],
                ['id' => Role::MESA_CONTROL, 'name' => 'Mesa de control'],
                ['id' => Role::ANALISTA, 'name' => 'Analista'],
                ['id' => Role::AJUSTADOR, 'name' => 'Ajustador'],
                ['id' => Role::COORDINADOR, 'name' => 'Coordinador']
            ]);

            /* Add default Super Admin */
            // Get First User
            $user = User::first();

            // Get Role
            $role = Role::findOrFail(Role::SUPER_ADMIN);

            // Save Role
            $user->roles()->save($role, ['created_by' => $user->id]);

        }
    }
}
