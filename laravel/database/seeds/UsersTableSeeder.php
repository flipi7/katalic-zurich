<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = \App\Models\User::firstOrNew([
            'email' => 'admin@katalic.com',
        ]);
        if (empty($user->id)) {
            $user->name = 'Admin';
            $user->password = bcrypt('secret');
            $user->save();
        }
    }
}
