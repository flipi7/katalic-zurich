<?php

use Illuminate\Database\Seeder;

class CountriesRegionsCitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Countries
        $countries = \App\Models\Country::first();
        if (empty($countries)) {
            DB::unprepared(file_get_contents(base_path() . "/../sql/geo_countries.sql"));
        }

        // Regions
        $states = \App\Models\State::first();
        if (empty($states)) {
            DB::unprepared(file_get_contents(base_path() . "/../sql/geo_states.sql"));
        }

        // Cities
        $cities = \App\Models\City::first();
        if (empty($cities)) {
            DB::unprepared(file_get_contents(base_path() . "/../sql/geo_cities.sql"));
        }
    }
}
