<?php

use Illuminate\Database\Seeder;
use App\Models\Status;

class StatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Check if seeded
        $claimStatuses = Status::all();

        if ($claimStatuses->isEmpty()) {
            Status::insert([
                ['id' => Status::CREATED, 'name' => 'Created'],
                ['id' => Status::VERIFICATION, 'name' => 'Verification'],
                ['id' => Status::CONTACT, 'name' => 'Contact'],
                ['id' => Status::VISIT, 'name' => 'Visit'],
                ['id' => Status::PRELIMINARY, 'name' => 'Preliminary'],
                ['id' => Status::REPORT, 'name' => 'Report'],
                ['id' => Status::PROCESSING, 'name' => 'Processing'],
                ['id' => Status::FINISHED, 'name' => 'Finished'],
                ['id' => Status::CANCELLED, 'name' => 'Cancelled'],
            ]);
        }
    }
}
