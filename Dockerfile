FROM ubuntu:14.04

RUN apt-get update -qqy \
    && DEBIAN_FRONTEND=noninteractive apt-get install -qqy \
        php5-cli \
        php5-fpm \
        php5-mysql \
        php5-imap \
    && php5enmod imap \
    && rm -rf /var/lib/apt/lists/* /var/cache/apt/*

RUN apt-get update -qqy && apt-get install -qqy \
        curl \
        git \
        zip \
        unzip \
        gnupg \
    && rm -rf /var/lib/apt/lists/* /var/cache/apt/*

## Install Composer
RUN curl --silent --show-error https://getcomposer.org/installer | php \
	&& mv composer.phar /usr/bin/composer

## NodeJS/NPM/BGulp
RUN curl -sL https://deb.nodesource.com/setup_6.x | bash - \
	&& apt-get install -y nodejs \
	&& npm install -g gulp


RUN mkdir /opt/app
WORKDIR /opt/app

COPY  ./ /opt/app/

EXPOSE 8000

CMD /bin/bash -c 'cd /opt/app/laravel && composer install && php artisan serve --host=0.0.0.0 --port=8000'
